
# SSH CA -- Backend

## Requirements
```
sudo apt install libcurl4-openssl-dev build-essential libssl-dev git
sudo apt install nginx fcgiwrap
sudo apt install doxygen doxygen-latex  graphviz
```
- A Redis Server. (Version: >=2.6.12)


## Install

1. Go to /opt/ and clone this repo `git clone --recurse-submodules git@gitlab.gwdg.de:asta/dnd-referat/ssh-ca.git`.
2. Comment out all `location` blocks in your nginx site config and add this line: `include /opt/ssh-ca/site.conf;`
3. `sudo service nginx restart`

Tested on GWDG Cloud Servers Ubuntu 20.04 LTS.

## Building

TODO (make all)

## Configure

TODO

## Code Documentation

The full backend/code documentation can be found [here as HTML](https://asta.pages.gwdg.de/dnd-referat/ssh-ca/) and [here as PDF](https://asta.pages.gwdg.de/dnd-referat/ssh-ca/refman.pdf).

## Coding Style

TODO

## Technical Concepts

TODO

## API Endpoints

TODO
