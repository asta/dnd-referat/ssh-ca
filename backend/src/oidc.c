#include "oidc.h"
#include "user.h"
#include "common.h"
#include "config.h"
#include "jwt.h"

#include "../cJSON/cJSON.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <curl/curl.h>

/**
 * @file
 * @brief Open ID Connect implementation
 *
 * @see oauth.c
 * @see oauthcallback.c
 *
 * @see https://goteleport.com/blog/how-oidc-authentication-works/
 */

/**
 * @brief internal struct used for receiving curl callback data
 */
struct oidc_memory_s {
	char *memory; ///< pointer to the allocated memory, containing the data
	size_t size; ///< size of memory
};

/**
 * @brief Internal function for reading a curl response into a oidc_memory_s struct.
 *
 * @see https://curl.se/libcurl/c/postinmemory.html
 */
static size_t oidc_internal_curl_callback(void *contents, size_t size, size_t nmemb, void *userp) {
	size_t realsize = size * nmemb;
	struct oidc_memory_s *mem = (struct oidc_memory_s *)userp;

	char *ptr = realloc(mem->memory, mem->size + realsize + 1);
	if(!ptr) {
		/* out of memory! */
		return 0;
	}

	mem->memory = ptr;
	memcpy(&(mem->memory[mem->size]), contents, realsize);
	mem->size += realsize;
	mem->memory[mem->size] = 0;

	return realsize;
}

/**
 * @brief Get external subject id from identity provider
 *
 * This function will use the oauth code received by the oauth callback endpoint to get the OpenID Connect id_token from the identity provider specified in the config.
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in] authconfig authentication settings
 * @param[in] code the oauth code oauthcallback received
 *
 * @returns the oidc external subject id of the user that just authenticated at the identity provider
 * @retval NULL an error occurred. errno might be set and errmsg contains an error message
 *
 * The returned string must be freed using free().
 *
 * @see user_get_by_oidc_external_subject_id()
 */
char * oidc_get_external_subject_id_from_identity_provider(char * errmsg, const struct config_auth_s * authconfig, const char * code) {
	bool success = false;
	CURL *curl = NULL;
	CURLcode curlres;
	struct oidc_memory_s chunk;
	chunk.memory = NULL;
	chunk.size = 0;

	char * app_id = NULL;
	char * redirect_uri = NULL;
	char * app_secret = NULL;

	char * postdata = NULL;

	cJSON * resp = NULL;
	char * id_token = NULL;

	cJSON * id_token_payload = NULL;

	char * claim = NULL;

	char * external_subject_id = NULL;

	if (authconfig == NULL || code == NULL) {
		sprintf(errmsg, "%s: all arguments must be set", __func__);
		goto end;
	}
	

	if (strcmp(authconfig->type, "oidc") != 0) { // check type
		sprintf(errmsg, "%s: unknown/unsupported authentication type", __func__);
		goto end;
	}

	if ((app_id = percent_encode(authconfig->app_id)) == NULL) {
		sprintf(errmsg, "%s: failed encoding app_id", __func__);
		goto end;
	}

	if ((app_secret = percent_encode(authconfig->app_secret)) == NULL) {
		sprintf(errmsg, "%s: failed encoding app_secret", __func__);
		goto end;
	}

	if ((redirect_uri = percent_encode(authconfig->redirect_uri)) == NULL) {
		sprintf(errmsg, "%s: failed encoding redirect_uri", __func__);
		goto end;
	}



	// === build postdata
	
	if ((postdata = malloc(sizeof(char) * (1024 + strlen(app_id) + strlen(app_secret) + strlen(code) + strlen(redirect_uri) + 1))) == NULL) {
		sprintf(errmsg, "%s: failed allocating postdata", __func__);
		goto end;
	}
	// client_id=APP_ID&client_secret=APP_SECRET&code=RETURNED_CODE&grant_type=authorization_code&redirect_uri=REDIRECT_URI
	sprintf(postdata, "client_id=%s&client_secret=%s&code=%s&grant_type=authorization_code&redirect_uri=%s", app_id, app_secret, code, redirect_uri);


	// === request
	
	// see https://curl.se/libcurl/c/postinmemory.html

	if ((chunk.memory = malloc(1)) == NULL) {
		sprintf(errmsg, "%s: failed allocating chunk memory", __func__);
		goto end;
	}

	/*
	if (!curl_global_init(CURL_GLOBAL_ALL)) {
		sprintf(errmsg, "%s: failed initializing curl global", __func__);
		goto end;
	}
	*/

	curl = curl_easy_init();
	if (curl == NULL) {
		sprintf(errmsg, "%s: failed initializing curl", __func__);
		goto end;
	}
	curl_easy_setopt(curl, CURLOPT_URL, authconfig->oauth_token_url);
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postdata);
	curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, (long)strlen(postdata));

	/* send all data to this function  */
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, oidc_internal_curl_callback);
	/* we pass our 'chunk' struct to the callback function */
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

	/* some servers don't like requests that are made without a user-agent
	 *        field, so we provide one */
	curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");
	
	/* Perform the request, curlres will get the return code */
	curlres = curl_easy_perform(curl);
	if(curlres != CURLE_OK) {
		sprintf(errmsg, "%s: curl_easy_perform() failed", __func__);
		goto end;
	}

	// === interpret response
	
	//fprintf(stderr,  "[DEBUG] %s: chunk.size: %ld chunk.memory: \"%s\"\n",__func__, chunk.size, chunk.memory);
	if ((resp = cJSON_Parse(chunk.memory)) == NULL) {
		sprintf(errmsg, "%s: response isn't JSON", __func__);
		goto end;
	}

	if (!get_string_from_json(errmsg, resp, "", "id_token", &id_token , false)) {
		goto end;
	} 

	// === verify and parse id_token
	
	if (!jwt_verify_encoding(id_token)) {
		sprintf(errmsg, "%s: id_token is not a valid JWT", __func__);
		goto end;
	}

	if ((id_token_payload = jwt_decode(id_token)) == NULL) {
		sprintf(errmsg, "%s: failed decoding id_token", __func__);
		goto end;
	}
	
	if (jwt_has_expired(id_token_payload)) {
		sprintf(errmsg, "%s: id_token has expired", __func__);
		goto end;
	}

	// === verify payload claims

	if (!get_string_from_json(errmsg, id_token_payload, "id_token", "iss", &claim , false)) {
		goto end;
	}
	if (strcmp(authconfig->oidc_expected_issuer, claim) != 0) {
		sprintf(errmsg, "%s: unexpected issuer", __func__);
		goto end;
	}

	if (!get_string_from_json(errmsg, id_token_payload, "id_token", "aud", &claim , false)) {
		goto end;
	}
	if (strcmp(authconfig->app_id, claim) != 0) {
		sprintf(errmsg, "%s: id_token isn't meant for me", __func__);
		goto end;
	}

	// === get external subject id
	if (!get_string_from_json(errmsg, id_token_payload, "id_token", "sub", &claim , false)) {
		goto end;
	}
	if ((external_subject_id = strdup(claim)) == NULL) {
		sprintf(errmsg, "%s: failed duplicating external_subject_id", __func__);
		goto end;
	}


	success = true;
end:
	cJSON_Delete(id_token_payload);
	cJSON_Delete(resp);
	free(chunk.memory);
	if (curl) {
		curl_easy_cleanup(curl);
	}
	free(postdata);
	free(app_id);
	free(app_secret);
	free(redirect_uri);
	if (!success) {
		free(external_subject_id);
		return NULL;
	}
	return external_subject_id;
}
