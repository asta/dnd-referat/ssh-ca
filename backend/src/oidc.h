#ifndef OIDC_H_
#define OIDC_H_

#include "config.h"


/**
 * @file
 * @brief Header file for oidc.c
 */

char * oidc_get_external_subject_id_from_identity_provider(char * errmsg, const struct config_auth_s * authconfig, const char * code);

#endif
