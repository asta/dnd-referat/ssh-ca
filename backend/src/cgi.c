#include "cgi.h"
#include "common.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>

/**
 * @file
 *
 * @brief Code for working with CGI.
 *
 * @note This file may get replaced in future by the REST API Code.
 */

/**
 * @brief Parses a QUERY_STRING.
 *
 * This is not standard conforming!
 *
 * The QUERY_STRING is a a list of key-value-pairs seperated by '&' characters.
 * Each key-value-pair is intern seperated by an '=' character.
 *
 * @param[in] qrystr the QUERY_STRING
 * 
 * @returns a key-value-pairs list containing the key-value-pairs
 * @retval NULL an error occurred and errno might be set. maybe the QUERY_STRING is invalid?
 *
 * The values can be accessed via cgi_kvp_get().
 *
 * The returned pointer must be freed using cgi_kvp_free().
 */
struct cgi_kvp_s * cgi_parse_query_string(const char * qrystr) {
	bool success = false;
	struct cgi_kvp_s * kvp = NULL;
	char * qrystrdup = NULL;
	char * cur = NULL;
	char * strtokarg = NULL;

	if (qrystr == NULL)
		goto end;

	if ((qrystrdup = strdup(qrystr)) == NULL)
		goto end;

	if ((kvp = cgi_kvp_create()) == NULL)
		goto end;

	success = true;
	strtokarg = qrystrdup;
	while(success && (cur = strtok(strtokarg, "&")) != NULL) {
		success = cgi_kvp_add_delimbased(kvp, cur, '=');
		strtokarg = NULL;
	}

end:
	free(qrystrdup);
	if (success) {
		return kvp;
	} else {
		cgi_kvp_free(kvp);
		return NULL;
	}
}

/**
 * @brief Parses a HTTP_COOKIE string.
 *
 * This is not standard conforming!
 *
 * The HTTP_COOKIE sting is a a list of key-value-pairs seperated by ';' characters.
 * Each key-value-pair is intern seperated by an '=' character.
 * Whitespaces are ignored.
 *
 * @param[in] cokstr the HTTP_COOKIE string.
 * 
 * @returns a key-value-pairs list containing the key-value-pairs
 * @retval NULL an error occurred and errno might be set. maybe the HTTP_COOKIE string is invalid?
 *
 * The values can be accessed via cgi_kvp_get().
 *
 * The returned pointer must be freed using cgi_kvp_free().
 */
struct cgi_kvp_s * cgi_parse_http_cookie(const char * cokstr) {
	bool success = false;
	struct cgi_kvp_s * kvp = NULL;
	char * cokstrdup = NULL;
	char * cur = NULL;
	char * strtokarg = NULL;

	if (cokstr == NULL)
		goto end;

	if ((cokstrdup = strdup(cokstr)) == NULL)
		goto end;

	// remove whitespace
	remove_whitespace(cokstrdup);

	if ((kvp = cgi_kvp_create()) == NULL)
		goto end;

	success = true;
	strtokarg = cokstrdup;
	while(success && (cur = strtok(strtokarg, ";")) != NULL) {
		success = cgi_kvp_add_delimbased(kvp, cur, '=');
		strtokarg = NULL;
	}

end:
	free(cokstrdup);
	if (success) {
		return kvp;
	} else {
		cgi_kvp_free(kvp);
		return NULL;
	}
}


/**
 * @brief Create a key-value-pairs list.
 *
 * @returns a key-value-pairs struct
 * @retval NULL an error occurred and errno is set
 *
 * The returned pointer must be freed using cgi_kvp_free().
 */
struct cgi_kvp_s * cgi_kvp_create(void) {
	struct cgi_kvp_s * res = NULL;

	if ((res = malloc(sizeof(struct cgi_kvp_s))) == NULL)
		return NULL;

	res->key = NULL;
	res->value = NULL;
	res->next = NULL;

	return res;
}


/**
 * @brief Frees a key-value-pairs list.
 *
 * @param[in,out] kvp the key-value-pairs list
 *
 * @see cgi_kvp_create()
 */
void cgi_kvp_free(struct cgi_kvp_s * kvp) {
	if (kvp == NULL)
		return;
	
	cgi_kvp_free(kvp->next);

	free(kvp->key);
	free(kvp->value);
	free(kvp);
}

/**
 * @brief Adds a key-value-pair to a key-value-pairs list.
 *
 * @note Neither key or value can be NULL.
 *
 * @note Duplicate keys can exist and cgi_kvp_get() will only retrieve the first one. Thus you cannot override a value.
 *
 * @param[in,out] kvp the key-value-pairs list
 * @param[in]     key the key
 * @param[in]     value the value
 *
 * @returns whether the pair was successfully added to the list
 * @retval true success
 * @retval false an error occured and errno might be set
 *
 */
bool cgi_kvp_add(struct cgi_kvp_s * kvp, const char * key, const char * value) {
	struct cgi_kvp_s * cur = kvp;
	struct cgi_kvp_s * tmp = NULL;

	if (kvp == NULL || key == NULL || value == NULL)
		return false;

	while (cur->next != NULL) {
		cur = cur->next;
	}

	if (cur->key == NULL) {
		// first item
		tmp = cur;
	} else {
		// items already exist
		tmp = cgi_kvp_create();
		if (tmp == NULL)
			return false;
		cur->next = tmp;
	}

	if (strlen(key) == 0) {
		return false;
	}

	if ((tmp->key = strdup(key)) == NULL) {
		return false;
	}
	if ((tmp->value = strdup(value)) == NULL) {
		return false;
	}

	return true;
}

/**
 * @brief Adds a key-value-pair to a key-value-pairs list but the key-value-pair is represented as a single string seperated by a delimiter.
 *
 * If the delimiter is not or more than once in keyvalue, then 0 is returned.
 *
 * This function is destructive and will replace the delimiter in keyvalue with '\0'.
 *
 * The delimiter cannot be '\0'.
 *
 * @see cgi_kvp_add()
 *
 * @param[in,out] kvp the key-value-pairs list
 * @param[in]     keyvalue the key and value in the same string seperated by the delimiter
 * @param[in]     delim the delimiter
 *
 * @returns whether the pair was successfully added to the list
 * @retval true success
 * @retval false an error occured and errno might be set or the delimiter was not exactly once in the string
 *
 */
bool cgi_kvp_add_delimbased(struct cgi_kvp_s * kvp, char * keyvalue, const char delim) {
	char * loc = NULL; // location of delim in keyvalue

	if (kvp == NULL || keyvalue == NULL || delim == '\0')
		return false;

	if ((loc = strchr(keyvalue, delim)) == NULL) 
		return false; // delim is not in keyvalue

	if (strrchr(keyvalue, delim) != loc) 
		return false; // delim is more than once  in keyvalue

	*loc = '\0';
	
	return cgi_kvp_add(kvp,keyvalue,loc +1);
}

/**
 * @brief Gets a value associated with a given key from a key-value-pairs list.
 *
 * @param[in,out] kvp the key-value-pairs list
 * @param[in]     key the key
 *
 * @returns the value associated with the key
 * @retval NULL no value is associated with the key
 *
 */
char * cgi_kvp_get(struct cgi_kvp_s * kvp, const char * key) {

	if (kvp == NULL || key == NULL || kvp->key == NULL)
		return NULL;
	
	if (strcmp(kvp->key,key) == 0) {
		return kvp->value; // found it
	} else {
		return cgi_kvp_get(kvp->next, key);
	}
}



/**
 * @param[in] method 'p': POST  'g': GET
 *
 */
int cgi_testenv(char * errmsg, char method, const char * post_content_type) {
	char * e;
	switch (method) {
	case 'p':
		if ((e = getenv("REQUEST_METHOD")) == NULL || strcmp(e,"POST") != 0) {
			strcat(errmsg, "Invalid REQUEST_METHOD! Must be \"POST\"!");
			return 400;
		}
		if ((e = getenv("CONTENT_TYPE")) == NULL || post_content_type == NULL || !isprefix(post_content_type, e)) {
			strcat(errmsg, "Invalid CONTENT_TYPE!");
			return 400;
		}
		break;
	case 'g':
		if ((e = getenv("REQUEST_METHOD")) == NULL || strcmp(e,"GET") != 0) {
			strcat(errmsg, "Invalid REQUEST_METHOD! Must be \"GET\"!");
			return 400;
		}
		break;
	default:
		strcat(errmsg, "Unknown needed REQUEST_METHOD!");
		return 500;
	}
	if ((e = getenv("CONTENT_LENGTH")) == NULL || atoi(e) >= CGI_MAXINPUTSIZE) {
		strcat(errmsg, "Invalid CONTENT_LENGTH!");
		return 400;
	}
	/*if ((e = getenv("REQUEST_SCHEME")) == NULL || strcmp(e,"https") != 0) {
		strcat(errmsg, "Invalid REQUEST_SCHEME! Must be \"https\"!");
		return 400;
	}*/

	return 200;
}

void cgi_printstatuscodeasstr(int statuscode) {
	switch (statuscode) {
		case 200: printf("OK"); break;
		case 400: printf("Bad Request"); break;
		case 401: printf("Unauthorized"); break;
		case 403: printf("Forbidden"); break;
		case 404: printf("Not Found"); break;
		case 418: printf("I'm a teapot"); break;
		case 500: printf("Internal Server Error"); break;
		case 501: printf("Not Implemented"); break;
		default:  printf("Unknown"); break;
	}
}

void cgi_printerror(char * errmsg, int statuscode) {
	/*char c;
	while ((c = (char)fgetc(stdin)) != EOF && c != '\0') {
		// empty input
	}*/
	printf("Content-Type: text/html\r\n");
	printf("Status: %d\r\n", statuscode);
	printf("\r\n");
	printf("<h1>%d - ", statuscode);
	cgi_printstatuscodeasstr(statuscode);
	printf("</h1>\r\n");
	printf("Error: <pre>%s</pre>\r\n", errmsg);
	printf("errno: %d <pre>%s</pre>\r\n", errno, strerror(errno));
}

