#ifndef JWT_H_
#define JWT_H_

#include "user.h"

#include "../cJSON/cJSON.h"

#include <stdbool.h>

/**
 * @file
 * @brief Header file for jwt.c
 */

/**
 * @brief JWT Type
 */
#define JWT_HEADER_TYP "JWT"
/**
 * @brief used JWT Algorithm
 */
#define JWT_HEADER_ALG "HS256"

/**
 * @brief JWT Issuer
 */
#define JWT_CLAIM_ISS "SSHCA"


char * jwt_simple_encode(char * errmsg, const user_t * user, unsigned long expin, unsigned long leeway, const char * key);
char * jwt_simple_decode(char * errmsg, const char * jwtstr, const char * key);


cJSON * jwt_gen_payload(char * errmsg, const user_t * user, unsigned long expin, unsigned long leeway);
char * jwt_encode_and_sign(const cJSON * payload, const char * key);

bool jwt_verify_encoding(const char * jwtstr);
bool jwt_verify_signature(const char * jwtstr, const char * key);
cJSON * jwt_decode(const char * jwtstr);
bool jwt_has_expired(const cJSON * payload);

#endif
