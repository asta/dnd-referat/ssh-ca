#include "user.h"
#include "common.h"
#include "db.h"

#include "../cJSON/cJSON.h"

#include <string.h>
#include <stdbool.h>

/**
 * @file
 *
 * @brief User related code
 */

/**
 * @brief Retrieves a user from the database via their oidc external subject id.
 *
 *
 * @see oidc_get_external_subject_id_from_identity_provider()
 *
 * @param[out]    errmsg  buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in,out] db      the database context
 * @param[in]     sub     the oidc external subject id
 *
 * @returns the user
 * @retval NULL an error occurred or user doesn't exist in the database. An error message has been written to errmsg.
 *
 * The returned user object must be freed using user_free().
 */
user_t * user_get_by_oidc_external_subject_id(char * errmsg, db_t * db, const char * sub) {
	user_t * res = NULL;
	cJSON * users = NULL;
	cJSON * user = NULL;
	cJSON * cur = NULL;
	char * cursub = NULL;
	
	// get all users
	if ((users = db_users_get_all(errmsg, db)) == NULL) {
		goto end;
	}
	
	// loop through all users and test if they have the correct external subject id
	cJSON_ArrayForEach(cur, users) {
		if (!get_string_from_json(errmsg, cur, "users[i]", "oidc_external_subject_id", &cursub , false)) {
			goto end;
		}
		if (strcmp(cursub,sub) == 0) {
			// found user
			user = cur;
			break;
		}

	}
	
	if (user == NULL) {
		sprintf(errmsg, "%s: User not found in database.", __func__);
		goto end;
	}

	res = user_parse(errmsg, user);
end:
	cJSON_Delete(users);
	return res;

}

/**
 * @brief Retrieves a user from the database via their userid.
 *
 * @see db_user_get()
 *
 * @param[out]    errmsg        buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in,out] db            the database context
 * @param[in]     userid        the users userid
 *
 * @returns the user
 * @retval NULL an error occurred or user doesn't exist in the database. An error message has been written to errmsg.
 *
 * The returned user object must be freed using user_free().
 */
user_t * user_get(char * errmsg, db_t * db, const char * userid) {
	user_t * res = NULL;
	cJSON * userjson = NULL;
	
	if ((userjson = db_user_get(errmsg, db, userid)) == NULL) {
		goto end;
	}
	
	res = user_parse(errmsg, userjson);
end:
	cJSON_Delete(userjson);
	return res;
}

/**
 * @brief Parses a user from json
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in] userjson  the user as json
 *
 * @returns the parsed user
 * @retval NULL an error occurred and an error message has been written to errmsg
 *
 * The returned user object must be freed using user_free().
 */
user_t * user_parse(char * errmsg, const cJSON * userjson) {
	bool success = false;
	const char * curjsonkey = "user";
	user_t * res = NULL;

	cJSON * dup = NULL;
	cJSON * tmp = NULL;
	cJSON * principal = NULL;
	
	int i = 0;

	if (userjson == NULL) {
		sprintf(errmsg, "%s: All arguments must be set.", __func__);
		goto end;
	}

	if (!cJSON_IsObject(userjson)) {
		sprintf(errmsg, "%s: user is not an object", __func__);
		goto end;
	}

	// alloc
	if ((res = malloc(sizeof(user_t))) == NULL) {
		sprintf(errmsg, "%s: Failed allocating user.", __func__);
		goto end;
	}
	// set everything to 0
	memset(res, 0, sizeof(user_t));

	// duplicate user json
	if ((dup = cJSON_Duplicate(userjson, true)) == NULL) {
		sprintf(errmsg, "%s: Failed duplicating user json.", __func__);
		goto end;
	}
	res->__raw = dup;


	// Read json
	
	if (!get_string_from_json(errmsg, dup, curjsonkey, "id", &res->id, false)) {
		goto end;
	}

	if (!get_string_from_json(errmsg, dup, curjsonkey, "name", &res->name, false)) {
		goto end;
	}

	if (!get_string_from_json(errmsg, dup, curjsonkey, "oidc_external_subject_id", &res->oidc_external_subject_id, false)) {
		goto end;
	}

	if (!get_string_from_json(errmsg, dup, curjsonkey, "state", &res->state, false)) {
		goto end;
	}

	if ((tmp = cJSON_GetObjectItemCaseSensitive(dup, "principals")) == NULL || !cJSON_IsArray(tmp)) {
		sprintf(errmsg, "%s: principals is not an array", __func__);
		goto end;
	}

	res->numprincipals = cJSON_GetArraySize(tmp);

	if ((res->principals = malloc(sizeof(char *) * ( res->numprincipals + 1))) == NULL) {
		sprintf(errmsg, "%s: Failed allocating principals.", __func__);
		goto end;
	}

	i = 0;
	cJSON_ArrayForEach(principal, tmp) {
		if (i +1 > res->numprincipals) {
			sprintf(errmsg, "%s: Too many principals. res->numprincipals: %d i: %d", __func__, res->numprincipals, i);
			goto end;
		}
		if (principal == NULL || !cJSON_IsString(principal) || principal->valuestring == NULL) {
			sprintf(errmsg, "%s: principals isn't a string", __func__);
			goto end;
		}
		res->principals[i] = principal->valuestring;
		i++;

	}
	res->principals[i] = NULL; // NULL-terminate Array

	if (res->numprincipals != i) {
		sprintf(errmsg, "%s: Not enough principals.", __func__);
		goto end;
	}



	success = true;
end:
	if (!success) {
		user_free(res);
		return NULL;
	}
	return res;
}


/**
 * @brief Frees a user.
 *
 * @param[in] user the user
 */
void user_free(user_t * user) {
	if (user == NULL)
		return;
	free(user->principals);
	cJSON_Delete(user->__raw);

	free(user);
}

