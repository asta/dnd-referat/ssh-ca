#include "template_engine.h"
#include "common.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/**
 * @file
 * @brief Template rendering code.
 *
 * ## TEMPLATING LANGUAGE 
 * The data model consits of key-value-pairs.
 * A key in a template is marked by enclosing it in pairs of percent-signs.
 * For example: "Hello %%name%%. I like you."
 * Note: A template is invalid if a key doesn't exist.
 */

/**
 * @brief Gets a value from a tmplt_kvp_s array.
 *
 * @param[in] data a null terminated key-value-pair array
 * @param[in] key the key
 * @param[out] is_safe whether the value is safe. An unsafe string must be encoded/escaped before usage!
 *
 * @returns the value associated with the key
 * @retval NULL key not found
 *
 */
static const char * tmplt_get_value(const struct tmplt_kvp_s * data, const char * key, bool * is_safe) {
	if (data == NULL || key == NULL || is_safe == NULL)
		return NULL;
	size_t i = 0;
	const struct tmplt_kvp_s * cur = NULL;

	while ((cur = &data[i])->key != NULL) {
		if (strcmp(cur->key, key) == 0) {
			*is_safe = cur->is_safe;
			return cur->value;
		}
		i++;
	}
	return NULL;
}

/**
 * @brief Renders a template to a string.
 *
 * This will return a copy of tmplt where all instances of `%%KEY%%`
 * are replaced with the associated value in data.
 * @see TEMPLATING LANGUAGE // TODO reference section
 *
 * A value marked as unsafe will be percent encoded using percent_encode().
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in] tmplt the template as a string
 * @param[in] data a null terminated key-value-pair array
 *
 * @returns the rendered document as a string
 * @retval NULL an error occurred and an error message has been written to errmsg. errno might be set.
 *
 * The returned string must be freed using free().
 */
char * tmplt_render(char * errmsg, const char * tmplt, const struct tmplt_kvp_s * data) {
	bool success = false;
	char * res = NULL;
	size_t reslen = 0;
	const char * cur = tmplt;
	char * key_open = NULL;
	char * key_close = NULL;
	char * key = NULL;
	size_t key_len = 0;
	const char * val = NULL;
	bool is_val_safe = false;
	char * tmp = NULL;


	if (tmplt == NULL || data == NULL) {
		sprintf(errmsg, "%s: all arguments must be set", __func__);
		goto end;
	}

	if ((res = malloc(sizeof(char) * (reslen + 1))) == NULL) {
		sprintf(errmsg, "%s: failed allocating res", __func__);
		goto end;
	}
	res[0] = '\0';
	
	while ((key_open = strstr(cur, TEMPLATE_KEY_OPEN)) != NULL) {
		if ((key_close = strstr(key_open + 1, TEMPLATE_KEY_CLOSE)) == NULL) {
			sprintf(errmsg, "%s: value tag opened but never closed", __func__);
			goto end;
		}
		key_len = key_close - key_open - strlen(TEMPLATE_KEY_CLOSE);

		if ((key = malloc(sizeof(char) * (key_len + 1))) == NULL) {
			sprintf(errmsg, "%s: failed allocating key! key_len: %ld", __func__, key_len);
			goto end;
		}
		strncpy(key, key_open + strlen(TEMPLATE_KEY_OPEN), key_len);
		key[key_len] = '\0';
		
		if (key_open != cur) {
			// append all characters from cur up to key_open to res
			if ((res = str_append(res, cur, &reslen, (size_t) (key_open - cur))) == NULL) {
				sprintf(errmsg, "%s: failed appending cur to res!", __func__);
				goto end;
			}
		}
		
		// get the value of key in data
		if ((val = tmplt_get_value(data, key, &is_val_safe)) == NULL) {
			sprintf(errmsg, "%s: key not in data", __func__);
			goto end;
		}

		
		// append all characters of val to res
		if (is_val_safe) {
			// val is safe and can be used as is
			if ((res = str_append(res, val, &reslen, 0)) == NULL) {
				sprintf(errmsg, "%s: failed appending val to res!", __func__);
				goto end;
			}
		} else {
			// val is unsafe and needs to be encoded/escaped first!
			if ((tmp = percent_encode(val)) == NULL) {
				sprintf(errmsg, "%s: failed encoding val!", __func__);
				goto end;
			}

			if ((res = str_append(res, tmp, &reslen, 0)) == NULL) {
				sprintf(errmsg, "%s: failed appending encoded val to res!", __func__);
				goto end;
			}
			free(tmp);
			tmp = NULL; // prevent double freeing
		}

		free(key);
		key = NULL; // prevent double freeing

		cur = key_close + strlen(TEMPLATE_KEY_CLOSE); // move cur to after key_close
	}

	// no more values in template
	// append the end of the template to res
	if ((res = str_append(res, cur, &reslen, 0)) == NULL) {
		sprintf(errmsg, "%s: failed appending the end of the template to res!", __func__);
		goto end;
	}


	success = true;
end:
	free(key);
	free(tmp);
	if (!success) {
		free(res);
		return NULL;
	}
	return res;
}



/**
 * @brief Renders a template to a string.
 * 
 * Wrapper for tmplt_render().
 * @see tmplt_render()
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in] tmplt_file_path path to the template file
 * @param[in] data a null terminated key-value-pair array
 *
 * @returns the rendered document as a string
 * @retval NULL an error occurred and an error message has been written to errmsg. errno might be set.
 *
 * The returned string must be freed using free().
 */
char * tmplt_render_from_file(char * errmsg, const char * tmplt_file_path, const struct tmplt_kvp_s * data) {
	char * tmplt = filetostr(tmplt_file_path);
	char * res = NULL;
	if (tmplt == NULL) {
		sprintf(errmsg, "%s: failed reading template file", __func__);
		return NULL;
	}
	res = tmplt_render(errmsg, tmplt, data);
	free(tmplt);
	return res;
}
