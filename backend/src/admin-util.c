#include "common.h"
#include "db.h"
#include "oidc.h"
#include "oauth.h"
#include "kredis.h"
#include "jwt.h"
#include "config.h"

#include "../cJSON/cJSON.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <getopt.h>
#include <openssl/sha.h>

/**
 * @file
 *
 * @brief Simple tool administration for administrating the SSH CA
 *
 *
 * @note This tool will be removed as soon as a fully working REST API + Frontend exists.
 * @see https://gitlab.gwdg.de/asta/dnd-referat/ssh-ca/-/issues/14
 */

/**
 * @brief the maximum length of a file path
 */
#define MAXFILEPATHLEN 256

/**
 * @brief the maximum length of a command argument
 */
#define MAXARGLEN 256


/**
 * @brief command argument struct
 */
struct command_arg_s {
	char name[32]; ///< name of the argument
	char desc[128]; ///< description of the argument
	char value[MAXARGLEN + 1]; ///< buffer for storing the value
};


/**
 * @brief command struct
 */
struct command_s {
	char cmd[32]; ///< name of the command
	char desc[128]; ///< description of the command
	int numargs; ///< number of arguments the command expects
	struct command_arg_s args[4]; ///< declaration of arguments
	int (*cmd_func_ptr)(char * errmsg, const char * cmd, const struct config_s * config,db_t * db, struct command_arg_s * args, int numargs); ///< command function
	int res; ///< return value of command function
};

static void printerror(char * errmsg) {
	fprintf(stderr, COLOR_RED"ERROR: "COLOR_PURPLE"%s\n"COLOR_RESET, errmsg);
	fprintf(stderr, "Maybe try the -h option for help?\n");
}

static void printusage(const char * prog, struct command_s * cmds) {
	struct command_s * cur;
	size_t i = 0;
	int j = 0;
	if (cmds == NULL)
		return;
	printf("Usage: %s [OPTIONS] "COLOR_YELLOW"<COMMAND>"COLOR_RESET" "COLOR_CYAN"[COMMANDARGS...]"COLOR_RESET" \n", prog);
	printf("Simple administration tool for SSH CA Server.\n");
	printf("\n");
	printf("OPTIONS:\n");
	printf("    -h          -- Prints this help message.\n");
	printf("    -c <config> -- Use a specific config file.\n");
	printf("\n");
	printf("\n");
	printf("COMMANDS:\n");
	while ((cur = &cmds[i])->cmd_func_ptr != NULL) {
		printf("- "COLOR_YELLOW"%s"COLOR_RESET, cur->cmd);
		for (j = 0; j < cur->numargs; j++) {
			printf(COLOR_CYAN" <%s>"COLOR_RESET, cur->args[j].name);
		}
		printf("\n");
		printf("  Desc: %s\n", cur->desc);
		if (cur->numargs > 0) {
			printf("  Args:\n");
			for (j = 0; j < cur->numargs; j++) {
				printf("  "COLOR_CYAN"%32s"COLOR_RESET" -- %s\n", cur->args[j].name, cur->args[j].desc);
			}
		}
		printf("\n");
		i++;
	}
	printf("\n");
}

static int cmd_list_users(char * errmsg, const char * cmd, const struct config_s * config,db_t * db, struct command_arg_s * args, int numargs) {
	UNUSED(cmd);
	UNUSED(config);
	UNUSED(args);
	UNUSED(numargs);
	int res = 0;
	cJSON * users = NULL;
	char * tmp = NULL;
	if ((users = db_users_get_all(errmsg, db)) == NULL) {
		goto end;
	}
	
	if ((tmp = cJSON_Print(users)) == NULL) {
		sprintf(errmsg, "%s: failed printing users array", __func__);
		goto end;
	}

	printf("===>>> USERS: \n");

	puts(tmp);


	res = 1;
end:
	free(tmp);
	cJSON_Delete(users);
	return res;
}

static int cmd_user_add(char * errmsg, const char * cmd, const struct config_s * config,db_t * db, struct command_arg_s * args, int numargs) {
	UNUSED(cmd);
	UNUSED(config);
	UNUSED(db);
	UNUSED(numargs);
	int res = 0;
	cJSON * user = NULL;
	cJSON * principals = NULL;

	char userid[UUIDLEN +1];
	char * name = args[0].value;
	char * principalsstr = NULL;
	char * oidc_external_subject_id = args[2].value;

	char * curprincipal = NULL;
	char * tmp = NULL;
	cJSON * tmpjson = NULL;

	char * tmpprint = NULL;

	if ((principalsstr = strdup(args[1].value)) == NULL) {
		sprintf(errmsg, "%s: failed duplicating principalstr", __func__);
		goto end;
	}

	// generate userid
	if (!uuid_v4_gen(userid)) {
		sprintf(errmsg, "%s: failed generating userid", __func__);
		goto end;
	}

	if ((user = cJSON_CreateObject()) == NULL) {
		sprintf(errmsg, "%s: failed creating user object", __func__);
		goto end;
	}

	// add simple stuff
	if (cJSON_AddStringToObject(user, "id", userid) == NULL) {
		sprintf(errmsg, "%s: failed adding userid to user", __func__);
		goto end;
	}
	if (cJSON_AddStringToObject(user, "name", name) == NULL) {
		sprintf(errmsg, "%s: failed adding username to user", __func__);
		goto end;
	}
	if (cJSON_AddStringToObject(user, "state", "active") == NULL) {
		sprintf(errmsg, "%s: failed adding state to user", __func__);
		goto end;
	}
	if (cJSON_AddStringToObject(user, "oidc_external_subject_id", oidc_external_subject_id) == NULL) {
		sprintf(errmsg, "%s: failed adding oidc_external_subject_id to user", __func__);
		goto end;
	}

	// add principals
	if ((principals = cJSON_CreateArray()) == NULL) {
		sprintf(errmsg, "%s: failed creating principals array", __func__);
		goto end;
	}
	tmp = principalsstr;
	while( (curprincipal = strtok(tmp, ",")) != NULL ) {
		if ((tmpjson = cJSON_CreateString(curprincipal)) == NULL) {
			sprintf(errmsg, "%s: failed creating principal string", __func__);
			goto end;
		}
		if (!cJSON_AddItemToArray(principals, tmpjson)) {
			sprintf(errmsg, "%s: failed adding principal to principals", __func__);
			goto end;
		}
		tmp = NULL;
	}
	if (!cJSON_AddItemToObject(user, "principals", principals)) {
		sprintf(errmsg, "%s: failed adding principals to user", __func__);
		goto end;
	}
	principals = NULL; // prevent double freeing

	// print user
	if ((tmpprint = cJSON_Print(user)) == NULL) {
		sprintf(errmsg, "%s: failed printing user", __func__);
		goto end;
	
	}

	// store user
	if (!db_user_store(errmsg, db, user)) {
		goto end;
	}


	printf("==>> Successfully added user:\n");
	puts(tmpprint);
	
	res = 1;
end:
	free(tmpprint);
	free(principalsstr);
	cJSON_Delete(principals);
	cJSON_Delete(user);
	return res;
}

static int cmd_user_update_principals(char * errmsg, const char * cmd, const struct config_s * config,db_t * db, struct command_arg_s * args, int numargs) {
	UNUSED(cmd);
	UNUSED(config);
	UNUSED(db);
	UNUSED(numargs);
	int res = 0;
	cJSON * user = NULL;
	cJSON * principals = NULL;

	char * userid = args[0].value;
	char * principalsstr = NULL;

	char * curprincipal = NULL;
	char * tmp = NULL;
	cJSON * tmpjson = NULL;

	char * tmpprint = NULL;

	if ((principalsstr = strdup(args[1].value)) == NULL) {
		sprintf(errmsg, "%s: failed duplicating principalstr", __func__);
		goto end;
	}

	if ((user = db_user_get(errmsg, db, userid)) == NULL) {
		goto end;
	}

	// create principals
	if ((principals = cJSON_CreateArray()) == NULL) {
		sprintf(errmsg, "%s: failed creating principals array", __func__);
		goto end;
	}
	tmp = principalsstr;
	while( (curprincipal = strtok(tmp, ",")) != NULL ) {
		if ((tmpjson = cJSON_CreateString(curprincipal)) == NULL) {
			sprintf(errmsg, "%s: failed creating principal string", __func__);
			goto end;
		}
		if (!cJSON_AddItemToArray(principals, tmpjson)) {
			sprintf(errmsg, "%s: failed adding principal to principals", __func__);
			goto end;
		}
		tmp = NULL;
	}
	// replace principals
	if (!cJSON_ReplaceItemInObjectCaseSensitive(user, "principals", principals)) {
		sprintf(errmsg, "%s: failed replacing principals in user", __func__);
		goto end;
	}
	principals = NULL; // prevent double freeing

	// print user
	if ((tmpprint = cJSON_Print(user)) == NULL) {
		sprintf(errmsg, "%s: failed printing user", __func__);
		goto end;
	
	}

	// store user
	if (!db_user_store(errmsg, db, user)) {
		goto end;
	}


	printf("==>> Successfully updated users principals:\n");
	puts(tmpprint);
	
	res = 1;
end:
	free(tmpprint);
	free(principalsstr);
	cJSON_Delete(principals);
	cJSON_Delete(user);
	return res;
}

static int cmd_user_deactivate(char * errmsg, const char * cmd, const struct config_s * config,db_t * db, struct command_arg_s * args, int numargs) {
	UNUSED(cmd);
	UNUSED(config);
	UNUSED(db);
	UNUSED(numargs);
	int res = 0;
	cJSON * user = NULL;

	char * userid = args[0].value;

	cJSON * tmpjson = NULL;

	char * tmpprint = NULL;

	if ((user = db_user_get(errmsg, db, userid)) == NULL) {
		goto end;
	}

	// create new state string
	if ((tmpjson = cJSON_CreateString("blocked")) == NULL) {
		sprintf(errmsg, "%s: failed creating state string", __func__);
		goto end;
	}
	// replace state string
	if (!cJSON_ReplaceItemInObjectCaseSensitive(user, "state", tmpjson)) {
		sprintf(errmsg, "%s: failed replacing state in user", __func__);
		goto end;
	}

	// print user
	if ((tmpprint = cJSON_Print(user)) == NULL) {
		sprintf(errmsg, "%s: failed printing user", __func__);
		goto end;
	}

	// store user
	if (!db_user_store(errmsg, db, user)) {
		goto end;
	}


	printf("==>> Successfully deactivated user:\n");
	puts(tmpprint);
	
	res = 1;
end:
	free(tmpprint);
	cJSON_Delete(user);
	return res;
}

static int cmd_user_activate(char * errmsg, const char * cmd, const struct config_s * config,db_t * db, struct command_arg_s * args, int numargs) {
	UNUSED(cmd);
	UNUSED(config);
	UNUSED(db);
	UNUSED(numargs);
	int res = 0;
	cJSON * user = NULL;

	char * userid = args[0].value;

	cJSON * tmpjson = NULL;

	char * tmpprint = NULL;

	if ((user = db_user_get(errmsg, db, userid)) == NULL) {
		goto end;
	}

	// create new state string
	if ((tmpjson = cJSON_CreateString("active")) == NULL) {
		sprintf(errmsg, "%s: failed creating state string", __func__);
		goto end;
	}
	// replace state string
	if (!cJSON_ReplaceItemInObjectCaseSensitive(user, "state", tmpjson)) {
		sprintf(errmsg, "%s: failed replacing state in user", __func__);
		goto end;
	}

	// print user
	if ((tmpprint = cJSON_Print(user)) == NULL) {
		sprintf(errmsg, "%s: failed printing user", __func__);
		goto end;
	}

	// store user
	if (!db_user_store(errmsg, db, user)) {
		goto end;
	}


	printf("==>> Successfully deactivated user:\n");
	puts(tmpprint);
	
	res = 1;
end:
	free(tmpprint);
	cJSON_Delete(user);
	return res;
}

/**
 * @brief main function for admin-util
 *
 * @param[in] argc number of arguments from the command line
 * @param[in] argv arguments from the command line
 * @param[in] envp environment variables
 *
 * @returns the exit code of this program.
 * @retval 0 success
 * @retval !0 failure
 */
int main(int argc, char **argv, char **envp) {
	int res = 0;
	char errmsg[ERRMSGMAXSIZE];
	int option;
	struct config_s * config = NULL;
	db_t * db = NULL;

	char configpath[MAXFILEPATHLEN + 1];
	int indextraarg = 0;


	struct command_s * cur = NULL;
	size_t i = 0;
	bool foundcmd = false;
	int expecnumargs = 0;
	struct command_s cmds[] = {
		{"list", "Lists all users.", 0, {{"","",""},{"","",""},{"","",""},{"","",""}},cmd_list_users, 0},
		{"add_user", "Adds a user.", 3, {{"name","Username",""},{"principals","comma-separated list",""},{"oidc_external_subject_id","OpenID Connect Subject ID of the user at Identity provider",""},{"","",""}},cmd_user_add, 0},
		{"update_user_principals", "Overwrites the principals of user.", 2, {{"userid","The users id",""},{"newprincipals","comma-separated list",""},{"","",""},{"","",""}},cmd_user_update_principals, 0},
		{"deactivate_user", "Deactivates a user.", 1, {{"userid","The users id",""},{"","",""},{"","",""},{"","",""}},cmd_user_deactivate, 0},
		{"activate_user", "Activates a user.", 1, {{"userid","The users id",""},{"","",""},{"","",""},{"","",""}},cmd_user_activate, 0},
		{"","",0,{{"","",""},{"","",""},{"","",""},{"","",""}},NULL,0} // TERMINATE ARRAY
	};

	UNUSED(envp);

	errmsg[0] = '\0';

	strcpy(configpath, CONFIGFILEPATH); // default config file path


	// parse arguments
	while ((option = getopt(argc, argv, ":hc:")) != -1) {
		switch (option) {
			case 'h':
				printusage(argv[0], cmds);
				res = 1;
				goto end;
			case 'c':
				if (strlen(optarg) > MAXFILEPATHLEN) {
					sprintf(errmsg, "%s: config path too long", __func__);
					printerror(errmsg);
					goto end;
				}
				strcpy(configpath, optarg);
				break;
			case ':':
				sprintf(errmsg, "%s: option needs a value", __func__);
				printerror(errmsg);
				goto end;
			case '?':
				sprintf(errmsg, "%s: unknown option", __func__);
				printerror(errmsg);
				fprintf(stderr, "option: %c\n", optopt);
				goto end;

		}
	}

	for(; optind < argc; optind++){
		if (indextraarg == 0) {
			// command
			if (strlen(argv[optind]) >= 32) {
				sprintf(errmsg, "%s: command is too long", __func__);
				printerror(errmsg);
				goto end;
			}
			i = 0;
			while (!foundcmd && (cur = &cmds[i])->cmd_func_ptr != NULL ) {
				if (strcmp(cur->cmd,argv[optind]) == 0) {
					foundcmd = true;
					expecnumargs = cur->numargs;
				}
				i++;
			}
			if (!foundcmd) {
				sprintf(errmsg, "%s: command not found", __func__);
				printerror(errmsg);
				goto end;
			
			}
		} else {
			// command argument
			if (strlen(argv[optind]) >= MAXARGLEN) {
				sprintf(errmsg, "%s: command argument is too long", __func__);
				printerror(errmsg);
				goto end;
			}
			if (indextraarg -1 > expecnumargs) {
				sprintf(errmsg, "%s: too many arguments", __func__);
				printerror(errmsg);
				goto end;
			}
			strcpy(cur->args[indextraarg -1].value,argv[optind]);
		
		}
		indextraarg++;
	}
	if (indextraarg <= 0) {
		sprintf(errmsg, "%s: missing command", __func__);
		printerror(errmsg);
		goto end;
	}
	if (indextraarg -1 != expecnumargs) {
		sprintf(errmsg, "%s: not enough arguments", __func__);
		printerror(errmsg);
		goto end;
	}

	// read config
	if ((config = config_parse(errmsg,configpath)) == NULL) {
		printerror(errmsg);
		goto end;
	}

	// connect to database
	if ((db = db_connect(errmsg, &config->db)) == NULL) {
		printerror(errmsg);
		goto end;
		
	}

	// execute cmd
	res = cur->cmd_func_ptr(errmsg, cur->cmd, config,db, cur->args, cur->numargs);
	if (!res) {
		printerror(errmsg);
		goto end;
	}

end:
	fflush(stdout);
	db_close(db);
	config_free(config);
	return !res;
}
