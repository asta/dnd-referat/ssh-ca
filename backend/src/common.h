#ifndef COMMON_H_
#define COMMON_H_
#include "../cJSON/cJSON.h"

#include <stdio.h>
#include <stdbool.h>

/**
 * @file
 * @brief Header file for common.c
 */


/**
 * @brief ANSI Escape Sequence for the color black
 */
#define COLOR_BLACK "\033[0;30m"
/**
 * @brief ANSI Escape Sequence for the color red
 */
#define COLOR_RED "\033[0;31m"
/**
 * @brief ANSI Escape Sequence for the color green
 */
#define COLOR_GREEN "\033[0;32m"
/**
 * @brief ANSI Escape Sequence for the color yellow
 */
#define COLOR_YELLOW "\033[0;33m"
/**
 * @brief ANSI Escape Sequence for the color blue
 */
#define COLOR_BLUE "\033[0;34m"
/**
 * @brief ANSI Escape Sequence for the color purple
 */
#define COLOR_PURPLE "\033[0;35m"
/**
 * @brief ANSI Escape Sequence for the color cyan
 */
#define COLOR_CYAN "\033[0;36m"
/**
 * @brief ANSI Escape Sequence for the color white
 */
#define COLOR_WHITE "\033[0;37m"
/**
 * @brief ANSI Escape Sequence for resetting the color
 */
#define COLOR_RESET "\033[0m"

/**
 * @brief path to the config file
 */
#define CONFIGFILEPATH "/opt/ssh-ca/backend/config.json"
/**
 * @brief size of errmsg buffer
 */
#define ERRMSGMAXSIZE 1024

/**
 * @brief the length of a UUID
 */
#define UUIDLEN 36

/**
 * @brief macro for marking a variable as unused
 */
#define UNUSED(x) (void)(x)

bool uuid_v4_gen(char *buffer);
bool is_valid_uuid_v4(const char *buffer);

bool is_valid_hex(char c);
int hex_to_int(char c);
char int_to_hex(int x);

bool is_percent_encoded(const char * str);
char * percent_decode(const char * str);
char * percent_encode(const char * str);

bool isprefix(const char *pre, const char *str);

char * filetostr(const char * path);

cJSON * filetojson(const char * path);

char * base64_encode(const char * str, size_t len);
char * base64_decode(const char * str);
char * base64url_encode(const char * str, size_t len, bool skippadding);
char * base64url_decode(char * str);

bool hmac_sha256(unsigned char * hash, const char * message, const char *key);

void remove_whitespace(char * s);

bool writestreamtofile(FILE * stream, const char * outpath);

bool get_string_from_json(char * errmsg, const cJSON * json, const char * curjsonkey, const char * stringkey, char ** res , bool can_be_null);
bool get_number_from_json(char * errmsg, const cJSON * json, const char * curjsonkey, const char * numberkey, long * res);
bool get_bool_from_json(char * errmsg, const cJSON * json, const char * curjsonkey, const char * boolkey, bool * res);

char * str_append(char * dest, const char * src, size_t * destlen, size_t srclen);
#endif
