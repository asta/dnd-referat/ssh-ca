#include "config.h"
#include "common.h"
#include "db.h"

#include "../cJSON/cJSON.h"

#include <sys/time.h>
#include <string.h>
#include <stdbool.h>

/**
 * @file
 *
 * @brief Code for parsing the config into a native format.
 *
 * ## config.json Layout
 *
 * @verbatim 
 * {
 * 	"db": { // Database settings
 * 		"type":"redis",
 * 		"ip":<string|ip of redis server|can be null>,
 * 		"port":<number|port of redis server>,
 * 		"socket":<string|path to unix socket file|can be null>,
 * 		"use_unix_socket":<bool|whether to use ip+port or socket for the connection>,
 * 		"timeout": {"seconds":<number|seconds of timeout>, "microseconds": <number|microseconds of timeout>}, // timeout is seconds + microseconds
 * 		"auth": [string|redis auth string NOTE: only pre Redis 6.0 requirepass auth string is supported |can be null>
 * 	},
 * 	"auth": { // Settings for the identity provider
 * 		"type":"oidc", // OpenID Connect
 * 		"oauth_authorize_url":<string|oauth authorize endpoint. example: https://gitlab.gwdg.de/oauth/authorize>,
 * 		"oauth_token_url":<string|oauth token endpoint. example: https://gitlab.gwdg.de/oauth/token>,
 * 		"redirect_uri":<string|url of oauthcallback endpoint. must match the redirect uri configured at the identity provider. example: https://sshca.asta.uni-goettingen.de/cgi/oauthcallback>,
 * 		"scope":<string|space seperated list of oauth scopes. must be equivalent to the scope configured at the identity provierd. must also include `openid`. example: openid>,
 * 		"app_id":<string|the oauth application id>,
 * 		"app_secret":<string|the oauth application secret>,
 * 		"oidc_expected_issuer":<string|the expected oidc issuer. `iss` field in id_token. example: https://gitlab.gwdg.de>
 * 	},
 * 	"jwt": { // JSON Web Token settings of the JWT the user gets by the refreshlogin endpoint after authentication
 * 		"secret":<string|JWT secret. must not be bruteforcable!>,
 * 		"ttl": <number|Time-To-Live of the JWT in seconds>,
 * 		"leeway": <number|leeway of ttl and ttl_refreshtoken>,
 * 		"ttl_refreshtoken": <number|Time-To-Live of the JWT Refresh Token in seconds>
 * 	},
 * 	"ssh-sign": {
 * 		"client_ca_private_key": <string|location of the client-ca file. example: ../../client-ca>,
 * 		"validity": <string|how long a signed key should be valid for. see -V option of ssh-keygen. example: -2h:+12h>
 * 	}
 * }
 * @endverbatim
 */

/**
 * @brief Parses a ssh-sign config json
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in] json the sshsign json from the config 
 * @param[out] conf the sshsign config to be written to
 *
 * @returns whether the function was successful.
 * @retval true success
 * @retval false an error occurred and an error message has been written to errmsg
 */
static bool config_parse_certify(char * errmsg, const cJSON * json, struct config_certify_s * conf) {
	bool res = false;
	const char * curjsonkey = "config.ssh-sign";

	// test if json is object
	if (json == NULL || !cJSON_IsObject(json)) {
		sprintf(errmsg, "%s: json is not an object", __func__);
		goto end;
	}

	// Read json
	if (!get_string_from_json(errmsg, json, curjsonkey, "client_ca_private_key", &conf->client_ca_private_key, false)) {
		goto end;
	}

	if (!get_string_from_json(errmsg, json, curjsonkey, "validity", &conf->validity, false)) {
		goto end;
	}

	res = true;
end:
	return res;

}

/**
 * @brief Parses a jwt config json
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in] json the jwt json from the config 
 * @param[out] conf the jwt config to be written to
 *
 * @returns whether the function was successful.
 * @retval true success
 * @retval false an error occurred and an error message has been written to errmsg
 */
static bool config_parse_jwt(char * errmsg, const cJSON * json, struct config_jwt_s * conf) {
	bool res = false;
	const char * curjsonkey = "config.jwt";

	// test if json is object
	if (json == NULL || !cJSON_IsObject(json)) {
		sprintf(errmsg, "%s: json is not an object", __func__);
		goto end;
	}

	// Read json
	if (!get_string_from_json(errmsg, json, curjsonkey, "secret", &conf->secret, false)) {
		goto end;
	}

	if (!get_number_from_json(errmsg, json, curjsonkey, "ttl", &conf->ttl)) {
		goto end;
	}

	if (!get_number_from_json(errmsg, json, curjsonkey, "leeway", &conf->leeway)) {
		goto end;
	}

	if (!get_number_from_json(errmsg, json, curjsonkey, "ttl_refreshtoken", &conf->ttl_refreshtoken)) {
		goto end;
	}

	res = true;
end:
	return res;


}

/**
 * @brief Parses a auth config json
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in] json the auth json from the config 
 * @param[out] conf the auth config to be written to
 *
 * @returns whether the function was successful.
 * @retval true success
 * @retval false an error occurred and an error message has been written to errmsg
 */
static bool config_parse_auth(char * errmsg, const cJSON * json, struct config_auth_s * conf) {
	bool res = false;
	const char * curjsonkey = "config.auth";

	// test if json is object
	if (json == NULL || !cJSON_IsObject(json)) {
		sprintf(errmsg, "%s: json is not an object", __func__);
		goto end;
	}

	// Read json
	if (!get_string_from_json(errmsg, json, curjsonkey, "type", &conf->type, false)) {
		goto end;
	}

	if (!get_string_from_json(errmsg, json, curjsonkey, "oauth_authorize_url", &conf->oauth_authorize_url, false)) {
		goto end;
	}

	if (!get_string_from_json(errmsg, json, curjsonkey, "app_id", &conf->app_id, false)) {
		goto end;
	}

	if (!get_string_from_json(errmsg, json, curjsonkey, "redirect_uri", &conf->redirect_uri, false)) {
		goto end;
	}

	if (!get_string_from_json(errmsg, json, curjsonkey, "scope", &conf->scope, false)) {
		goto end;
	}

	if (!get_string_from_json(errmsg, json, curjsonkey, "app_secret", &conf->app_secret, false)) {
		goto end;
	}

	if (!get_string_from_json(errmsg, json, curjsonkey, "oauth_token_url", &conf->oauth_token_url, false)) {
		goto end;
	}

	if (!get_string_from_json(errmsg, json, curjsonkey, "oidc_expected_issuer", &conf->oidc_expected_issuer, false)) {
		goto end;
	}

	res = true;
end:
	return res;

}

/**
 * @brief Parses a db config json
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in] json the db json from the config 
 * @param[out] conf the db config to be written to
 *
 * @returns whether the function was successful.
 * @retval true success
 * @retval false an error occurred and an error message has been written to errmsg
 */
static bool config_parse_db(char * errmsg, const cJSON * json, struct config_db_s * conf) {
	bool res = false;
	const char * curjsonkey = "config.db";
	cJSON * tmp = NULL;

	// test if json is object
	if (json == NULL || !cJSON_IsObject(json)) {
		sprintf(errmsg, "%s: json is not an object", __func__);
		goto end;
	}

	// Read json
	if (!get_string_from_json(errmsg, json, curjsonkey, "type", &conf->type, false)) {
		goto end;
	}

	if (!get_string_from_json(errmsg, json, curjsonkey, "ip", &conf->ip, true)) {
		goto end;
	}

	if (!get_string_from_json(errmsg, json, curjsonkey, "socket", &conf->socket, true)) {
		goto end;
	}

	if (!get_number_from_json(errmsg, json, curjsonkey, "port", &conf->port)) {
		goto end;
	}

	if (!get_bool_from_json(errmsg, json, curjsonkey, "use_unix_socket", &conf->use_unix_socket)) {
		goto end;
	}

	if ((tmp = cJSON_GetObjectItemCaseSensitive(json, "timeout")) == NULL || !cJSON_IsObject(tmp)) {
		sprintf(errmsg, "%s: timout is not an object", __func__);
		goto end;
	}

	if (!get_number_from_json(errmsg, tmp, "config.db.timeout", "seconds", &conf->timeout.tv_sec)) {
		goto end;
	}

	if (!get_number_from_json(errmsg, tmp, "config.db.timeout", "microseconds", &conf->timeout.tv_usec)) {
		goto end;
	}

	if (!get_string_from_json(errmsg, json, curjsonkey, "auth", &conf->auth, true)) {
		goto end;
	}

	res = true;
end:
	return res;
}



/**
 * @brief Parses the config
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in] filepath path to the file containing the config
 *
 * @returns the parsed config
 * @retval NULL an error occurred and an error message has been written to errmsg
 *
 * The returned config must be freed using config_free().
 */
struct config_s * config_parse(char * errmsg, const char * filepath) {
	bool success = false;
	struct config_s * res = NULL;

	cJSON * tmp = NULL;
	cJSON * config = NULL;


	// alloc
	if ((res = malloc(sizeof(struct config_s))) == NULL) {
		sprintf(errmsg, "%s: Failed allocating config.", __func__);
		goto end;
	}
	// set everything to 0
	memset(res, 0, sizeof(struct config_s));

	// read config
	if ((config = filetojson(filepath)) == NULL) {
		sprintf(errmsg, "%s: Failed reading config.", __func__);
		goto end;
	}
	res->__raw = config; // store JSON // freed by config_free()

	// parse db config
	if ((tmp = cJSON_GetObjectItemCaseSensitive(config, "db")) == NULL) {
		sprintf(errmsg, "%s: Config doesn't contain db settings.", __func__);
		goto end;
	}
	if (!(success = config_parse_db(errmsg, tmp, &(res->db)))) {
		goto end;
	}


	// parse auth config
	if ((tmp = cJSON_GetObjectItemCaseSensitive(config, "auth")) == NULL) {
		sprintf(errmsg, "%s: Config doesn't contain auth settings.", __func__);
		goto end;
	}
	if (!(success = config_parse_auth(errmsg, tmp, &(res->auth)))) {
		goto end;
	}

	// parse jwt config
	if ((tmp = cJSON_GetObjectItemCaseSensitive(config, "jwt")) == NULL) {
		sprintf(errmsg, "%s: Config doesn't contain jwt settings.", __func__);
		goto end;
	}
	if (!(success = config_parse_jwt(errmsg, tmp, &(res->jwt)))) {
		goto end;
	}

	// parse sshsign config
	if ((tmp = cJSON_GetObjectItemCaseSensitive(config, "ssh-sign")) == NULL) {
		sprintf(errmsg, "%s: Config doesn't contain ssh-sign settings.", __func__);
		goto end;
	}
	if (!(success = config_parse_certify(errmsg, tmp, &(res->certify)))) {
		goto end;
	}


	success = true;
end:
	if (!success) {
		config_free(res);
		return NULL;
	}
	return res;
}


/**
 * @brief Frees a config.
 *
 * @param[in] config the config
 */
void config_free(struct config_s * config) {
	if (config == NULL)
		return;
	cJSON_Delete(config->__raw);

	free(config);

}

