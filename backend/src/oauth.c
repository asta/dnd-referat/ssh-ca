#include "oauth.h"
#include "common.h"
#include "config.h"

#include "../cJSON/cJSON.h"

#include <openssl/sha.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

/**
 * @file
 *
 * @brief Useful code for OAuth Authentication.
 *
 */

/**
 * @brief Tests whether a state is encoded correctly.
 *
 * @param[in]  state see oauth_gen_state()
 *
 * @returns whether the state is encoded correctly
 * @retval true valid encoding; safe to use
 * @retval false invalid encoding; dangerous string!
 *
 */
bool oauth_verify_state_encoding(const char * state) {
	return is_valid_uuid_v4(state);
}

/**
 * @brief Generates a STATE for OAuth.
 * 
 * This is basically just a UUIDv4.
 *
 * @see https://docs.gitlab.com/ee/api/oauth2.html#authorization-code-with-proof-key-for-code-exchange-pkce
 *
 * @returns a new STATE represented by a string
 * @retval NULL an error occurred and errno might be set
 *
 * The returned string must be freed using free().
 */
char * oauth_gen_state(void) {
	char * res = NULL;
	res = malloc(sizeof(char) * (UUIDLEN + 1));
	if (res == NULL)
		return NULL;
	if (!uuid_v4_gen(res)) {
		free(res);
		return NULL;
	}
	return res;
}

/**
 * @brief Generates a CODE_VERIFIER for OAuth.
 * 
 * This is basically just two consecutive UUIDv4s.
 *
 * @see https://docs.gitlab.com/ee/api/oauth2.html#authorization-code-with-proof-key-for-code-exchange-pkce
 *
 * @returns a new CODE_VERIFIER represented by a string
 * @retval NULL an error occurred and errno might be set
 *
 * The returned string must be freed using free().
 */
char * oauth_gen_code_verifier(void) {
	char * res = NULL;
	char * tmp = NULL;
	res = malloc(sizeof(char) * (UUIDLEN * 2 + 1));
	if (res == NULL)
		return NULL;
	if (!uuid_v4_gen(res)) {
		free(res);
		return NULL;
	}
	tmp = &res[strlen(res)]; // tmp points to the '\0' byte of res
	if (!uuid_v4_gen(tmp)) {
		free(res);
		return NULL;
	}
	return res;
}

/**
 * @brief Generates a CODE_CHALLENGE for OAuth.
 * 
 * This is a URL-safe base64-encoded (without padding) string of the SHA256 hash of the CODE_VERIFIER.
 *
 * The returned string is already percent encoded.
 *
 * @see https://docs.gitlab.com/ee/api/oauth2.html#authorization-code-with-proof-key-for-code-exchange-pkce
 *
 * @param[in] code_verifier the CODE_VERIFIER needed for generating the CODE_CHALLENGE
 *
 * @returns the CODE_CHALLENGE represented by a string
 * @retval NULL an error occurred and errno might be set
 *
 * The returned string must be freed using free().
 */
char * oauth_gen_code_challenge(const char * code_verifier) {
	unsigned char hash[SHA256_DIGEST_LENGTH];
	char * tmp = NULL;
	char * res = NULL;

	SHA256((const unsigned char *)code_verifier, strlen(code_verifier), hash);

	tmp = base64url_encode((char *)hash, SHA256_DIGEST_LENGTH, true);
	if (tmp == NULL)
		return NULL;

	res = percent_encode(tmp); // might be NULL
	free(tmp);
	return res;
}

/**
 * @brief Generates an OAuth authorization Url.
 *
 * An example authconfig can be found in config.example.json["auth"].
 *
 * @see https://docs.gitlab.com/ee/api/oauth2.html#authorization-code-flow
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in]  authconfig authentication settings
 * @param[in]  state see oauth_gen_state()
 *
 * @returns A fully usable OAuth authorization Url.
 * @retval NULL an error occurred. errno might be set and errmsg contains an error message. 
 */
char * oauth_gen_url(char * errmsg, const struct config_auth_s * authconfig, const char * state) {
	char * res = NULL;


	char * oauth_authorize_url = NULL;
	char * app_id = NULL;
	char * redirect_uri = NULL;
	char * scope = NULL;
	//char * app_secret = NULL;

	char * redirect_uri_encoded = NULL;

	if (strcmp(authconfig->type, "oidc") != 0) { // check type
		sprintf(errmsg, "%s: unknown/unsupported authentication type", __func__);
		goto end;
	}

	oauth_authorize_url = authconfig->oauth_authorize_url;

	app_id = authconfig->app_id;

	redirect_uri = authconfig->redirect_uri;

	scope = authconfig->scope;

	// percent encode redirect_uri
	if ((redirect_uri_encoded = percent_encode(redirect_uri)) == NULL) {
		sprintf(errmsg, "%s: failed percent encoding redirect_uri", __func__);
		goto end;
	}
	// alloc res
	res = malloc(sizeof(char) * (strlen(oauth_authorize_url) + strlen(app_id) + strlen(redirect_uri) + strlen(scope) + strlen(state) + 128 + 1  ));
	if (res == NULL) {
		sprintf(errmsg, "%s: failed allocating res", __func__);
		goto end;
	}

	sprintf(res, "%s?client_id=%s&redirect_uri=%s&response_type=code&state=%s&scope=%s", oauth_authorize_url, app_id, redirect_uri_encoded, state, scope);

end:
	free(redirect_uri_encoded);
	return res;
}
