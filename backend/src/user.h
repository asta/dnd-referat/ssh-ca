#ifndef USER_H_
#define USER_H_

/**
 * @file
 * @brief Header file for user.c
 */

typedef struct user_s user_t;

#include "db.h"

#include "../cJSON/cJSON.h"

/**
 * @brief user struct
 */
struct user_s {
	cJSON * __raw; ///< the raw json data (internal use only!)
	char * id; ///< the userid
	char * name; ///< the name
	char * oidc_external_subject_id; ///< the users external subject id in the identity provider
	char * state; ///< the state, either "active" or "blocked"
	int numprincipals; ///< the number of principals in principals
	char ** principals; ///< null-terminated list of principal strings
};

user_t * user_get_by_oidc_external_subject_id(char * errmsg, db_t * db, const char * sub);
user_t * user_get(char * errmsg, db_t * db, const char * userid);

user_t * user_parse(char * errmsg, const cJSON * userjson);
void user_free(user_t * user);


#endif
