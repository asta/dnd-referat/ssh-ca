#include "common.h"
#include "cgi.h"
#include "db.h"
#include "oauth.h"
#include "config.h"
#include "user.h"
#include "oidc.h"
#include "template_engine.h"

#include "../cJSON/cJSON.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>


/**
 *
 * @file
 *
 * @brief oauth callback receiver. handles endpoint at /cgi/oauthcallback
 *
 * Request:
 * Expects a GET Request.
 * The QUERY_STRING should contain:
 * - `code` the oauth code
 * - `state` the oauth state
 *
 * Response:
 * Content-Type: `text/html`
 * The Body should contain the rendered version of access_granted.html or access_denied.html.
 */

/**
 * @brief main function for oauthcallback
 *
 * @returns the exit code of this program.
 * @retval 0 success
 * @retval !0 failure
 */
int main(void) {
	char errmsg[ERRMSGMAXSIZE];
	int statuscode = 200;
	struct config_s * config = NULL;
	db_t * db = NULL;
	char * qrystr = NULL;
	struct cgi_kvp_s * qry = NULL;

	const char * qry_oauth_state = NULL;
	const char * qry_code = NULL;
	const char * qry_error = NULL;
	const char * qry_error_description = NULL;

	char * oidc_external_subject_id = NULL;
	
	user_t * user = NULL;

	char jwtrefreshtoken[DB_JWTREFRESHTOKEN_BUFSIZE];

	char * body = NULL;


	errmsg[0] = '\0';
	jwtrefreshtoken[0] = '\0';
		
	if ((statuscode = cgi_testenv(errmsg, 'g', NULL)) != 200) {
		cgi_printerror(errmsg, statuscode);
		goto end;
	}

	// read QUERY_STRING
	if ((qrystr = getenv("QUERY_STRING")) != NULL && strlen(qrystr) > 0 && (qry = cgi_parse_query_string(qrystr)) == NULL) {
		statuscode = 400;
		sprintf(errmsg, "%s: Failed parsing QUERY_STRING.", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}

	// read config
	if ((config = config_parse(errmsg, CONFIGFILEPATH)) == NULL) {
		statuscode = 500;
		cgi_printerror(errmsg, statuscode);
		goto end;
	}

	// connect to database
	if ((db = db_connect(errmsg, &config->db)) == NULL) {
		statuscode = 500;
		cgi_printerror(errmsg, statuscode);
		goto end;
		
	}

	// read query: 'state' 
	if ((qry_oauth_state = cgi_kvp_get(qry, "state")) == NULL) {
		statuscode = 400;
		sprintf(errmsg, "%s: Missing 'state' key.", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}
	// verify state encoding 
	if (!oauth_verify_state_encoding(qry_oauth_state)) {
		statuscode = 400;
		sprintf(errmsg, "%s: state is not encoded correctly.", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}
	// qry_oauth_state is harmless from here on out

	// read query: 'error' 
	if ((qry_error = cgi_kvp_get(qry, "error")) != NULL) {
		// verify qry_error encoding 
		if (!is_percent_encoded(qry_error)) {
			statuscode = 400;
			sprintf(errmsg, "%s: error is not encoded correctly.", __func__);
			cgi_printerror(errmsg, statuscode);
			goto end;
		}
		// qry_error is harmless from here on out
	
		qry_error_description = cgi_kvp_get(qry, "error_description");
		if (qry_error_description == NULL)
			qry_error_description = "(none)";

		const struct tmplt_kvp_s data[] = {
			{"oauth_state",qry_oauth_state,true},
			{"error",qry_error,true},
			{"error_description",qry_error_description,false},
			{NULL,NULL,false} // TERMINATE ARRAY
		};
		if ((body = tmplt_render_from_file(errmsg,TEMPLATES_DIR"/access_denied.html", data)) == NULL) {
			statuscode = 500;
			cgi_printerror(errmsg, statuscode);
			goto end;
		}

		statuscode = 401;
		printf("Content-Type: text/html\r\n");
		printf("Status: %d\r\n", statuscode);
		printf("\r\n");
		fputs(body, stdout);

		goto end;
	}
	

	// read query: 'code' 
	if ((qry_code = cgi_kvp_get(qry, "code")) == NULL) {
		statuscode = 400;
		sprintf(errmsg, "%s: Missing 'code' key.", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}
	// verify code encoding 
	if (!is_percent_encoded(qry_code)) {
		statuscode = 400;
		sprintf(errmsg, "%s: code is not encoded correctly.", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}
	// qry_code is harmless from here on out

	// === oidc stuff

	if ((oidc_external_subject_id = oidc_get_external_subject_id_from_identity_provider(errmsg, &config->auth, qry_code)) == NULL) {
		statuscode = 500;
		cgi_printerror(errmsg, statuscode);
		goto end;
	}


	// get user
	if ((user = user_get_by_oidc_external_subject_id(errmsg, db, oidc_external_subject_id)) == NULL) {
		// user doesn't exist
		statuscode = 403;
		cgi_printerror(errmsg, statuscode);
		goto end;
	}
	
	// verify user
	// oidc_external_subject_id is already verified otherwise the user wouldn't exist
	if (strcmp(user->state, "active") != 0) {
		statuscode = 403;
		sprintf(errmsg, "%s: user isn't activated", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}

	// generate jwtrefreshtoken
	if (!db_jwtrefreshtoken_create(errmsg, db, jwtrefreshtoken, user, config->jwt.ttl_refreshtoken)) {
		statuscode = 500;
		cgi_printerror(errmsg, statuscode);
		goto end;

	}


	const struct tmplt_kvp_s data[] = {
		{NULL,NULL,false} // TERMINATE ARRAY
	};
	if ((body = tmplt_render_from_file(errmsg,TEMPLATES_DIR"/access_granted.html", data)) == NULL) {
		statuscode = 500;
		cgi_printerror(errmsg, statuscode);
		goto end;
	}
	printf("Content-Type: text/html\r\n");
	printf("Status: %d\r\n", statuscode);
	printf("Set-Cookie: jwtrefreshtoken=%s; Secure; HttpOnly; Max-Age=%ld ; SameSite=Strict\r\n", jwtrefreshtoken, config->jwt.ttl_refreshtoken);
	//printf("Set-Cookie: jwtrefreshtoken=%s; Secure; HttpOnly; Max-Age=%ld\r\n", jwtrefreshtoken, config->jwt.ttl_refreshtoken);
	
	/* --------------------------------------------------------------- */
	printf("\r\n");
	/* --------------------------------------------------------------- */
	fputs(body, stdout);

end:
	free(body);
	user_free(user);
	free(oidc_external_subject_id);
	fflush(stdout);
	cgi_kvp_free(qry);
	db_close(db);
	config_free(config);
	return 0;
}
