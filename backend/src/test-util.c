#include "common.h"
#include "db.h"
#include "oauth.h"
#include "kredis.h"
#include "jwt.h"
#include "config.h"
#include "template_engine.h"

#include "../cJSON/cJSON.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <getopt.h>
#include <openssl/sha.h>


/**
 * @file
 *
 * @brief Tool for testing PARTs of this program.
 */

/**
 * @brief the maximum length of a file path
 */
#define MAXFILEPATHLEN 256

/**
 * @brief testcase struct
 */
struct testable_s {
	char name[32]; ///< name of the test
	char desc[128]; ///< description of the test
	int (*test_func_ptr)(char * errmsg, const char * name, const struct config_s * config); ///< test function
	bool marked; ///< true: should be tested false: should not be tested
	int res; ///< return value of test function. 0 indicated failure
	char errmsg[ERRMSGMAXSIZE]; ///< buffer for error message
};
/**
 * @brief struct used for testing encoders
 */
struct encoder_testcase_s {
	char type; ///< '+': str is valid '-': str is invalid '\0': end of array
	char str[256]; ///< testcase string
	char exenc[256 * 3 + 1]; ///< expected encoded of str
};

static void printerror(char * errmsg) {
	fprintf(stderr, "ERROR: %s\n", errmsg);
	fprintf(stderr, "Maybe try the -h option for help?\n");
}

static void printusage(const char * prog, struct testable_s * testables) {
	struct testable_s * cur;
	size_t i = 0;
	if (testables == NULL)
		return;
	printf("Usage: %s [OPTIONS] [PART]...\n", prog);
	printf("Tests one or more PARTs of this program.\n");
	printf("\n");
	printf("OPTIONS:\n");
	printf("    -h          -- Prints this help message.\n");
	printf("    -a          -- Mark all PARTs, that are not listed as extra arguments, for testing\n");
	printf("    -c <config> -- Use a specific config file.\n");
	printf("\n");
	printf("\n");
	printf("Testable PARTs:\n");
	while ((cur = &testables[i])->test_func_ptr != NULL) {
		printf("    - %32s -- %s\n", cur->name, cur->desc);
		i++;
	}
	printf("\n");
	printf("The tests are always tested in the same order. You can only choose\n"
		"    which PARTs will be tested, but not in which order.\n");
	printf("\n");
	printf("Note: This program still needs a valid config.\n");
}

/* ======================== TESTS ======================== */

static int test_uuid_verification(char * errmsg, const char * name, const struct config_s * config) {
	UNUSED(name);
	UNUSED(config);
	char uuidv4[38 + 10]; // + 10 for invalid UUIDv4s

	// valid UUIDv4s
	strcpy(uuidv4, "84b52420-72b3-4c22-9dba-72c1ec9e9f18"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "6da7672f-e3e9-47ec-972c-57226b66d258"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "68ef2681-955a-490f-b2c5-09270eef7a59"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "9ff5615c-7e2a-4e76-a0ee-901d8a22026c"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "0b0b882f-6d8a-4813-8ecf-3695c1fadf3b"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "b91a8624-1f11-4151-937b-5b2553cb1928"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "c39f75af-ee91-4329-a7f1-93c6331656aa"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "f12d89d0-2d7e-4e54-b2a4-92d0b42390b5"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "13a2bd6e-fddd-49e0-8bab-8c78c0eae539"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "2f282595-615d-499a-8da5-f8276e0b90f9"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "e6eab5d4-c84b-406f-84a8-d69da21f5ee9"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "ecaf1a9c-853a-4c26-a811-61801370d84b"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "e479b112-8666-4915-843b-927eeb4bdf1c"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "a6d4aee8-bfaf-47f7-9b36-9b3bdcb623ce"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "5f211f71-4808-4964-a701-6aa57df8f082"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "4a5033e9-0f42-4e34-86dc-057157fd9471"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "9b111f5a-a439-401b-8ed7-7f6a48615a72"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "411bda1c-843a-4ec4-b1b1-69e5bc7dce89"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "94244a03-8796-42df-b53e-a9da2c45bfc5"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "66934cfb-10ae-477f-9d65-6a7e344067fe"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "93eab3c1-7152-4192-9574-9a5e39795323"); if (!is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: valid UUIDv4 was detected as invalid: \"%s\"", __func__, uuidv4); return 0; }

	// invalid UUIDv4s
	strcpy(uuidv4, "84b52420+72b3-4c22-9dba-72c1ec9e9f18"); if (is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: invalid UUIDv4 was detected as valid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "8/b52420-72b3-4c22-9dba-72c1ec9e9f18"); if (is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: invalid UUIDv4 was detected as valid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "84b52420-72b3-4c22-9dba-72c1ec9e9f182"); if (is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: invalid UUIDv4 was detected as valid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "84b52420-72b3-4c22-9dba-72c1ec9e9f1"); if (is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: invalid UUIDv4 was detected as valid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, ""); if (is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: invalid UUIDv4 was detected as valid: \"%s\"", __func__, uuidv4); return 0; }
	strcpy(uuidv4, "84b5242072b34c229dba72c1ec9e9f181234"); if (is_valid_uuid_v4(uuidv4)) { sprintf(errmsg, "%s: invalid UUIDv4 was detected as valid: \"%s\"", __func__, uuidv4); return 0; }

	return 1;
}

static int test_uuid_generation(char * errmsg, const char * name, const struct config_s * config) {
	UNUSED(name);
	UNUSED(config);
	char uuidv4[38];
	int rc;
	int i;
	for (i = 0; i < 128; i++) {
		uuidv4[0] = '\0';
		rc = uuid_v4_gen(uuidv4);
		if(rc == 1) {
			//printf("UUID v4[%ld]: '%s' \n",strlen(uuidv4), uuidv4);
			if (!is_valid_uuid_v4(uuidv4)) {
				sprintf(errmsg, "%s: invalid UUIDv4 generated: \"%s\"", __func__, uuidv4);
				return 0;
			}
		} else {
			sprintf(errmsg, "%s: Not enough random bytes for PRNG", __func__);
			return 0;
		}
	}
	return 1;
}

static int test_percent(char * errmsg, const char * name, const struct config_s * config) {
	UNUSED(name);
	UNUSED(config);
	
	int res = 0;
	char * enc = NULL;
	char * dec = NULL;
	struct encoder_testcase_s testcases[] = {
		// TODO needs way more testcases
		{'-',"Hallo We%lt","Hallo%20We%25lt"},
		{'-',"Hallo Welt%","Hallo%20Welt%25"},
		{'-',"Hallo Welt%2","Hallo%20Welt%252"},
		{'+',"Hallo%20Welt%25","Hallo%2520Welt%2525"},
		{'-',"Hallo Welt","Hallo%20Welt"},
		{'+',"Hallo%20Welt","Hallo%2520Welt"},
		{'\0',"",""} // TERMINATE ARRAY
	};

	struct encoder_testcase_s * cur;
	size_t i = 0;
	while ((cur = &testcases[i])->type != '\0') {
		
		//fprintf(stderr, "[DEBUG] %s: TESTING: type: '%c', str: \"%s\" exenc: \"%s\"\n",__func__,cur->type,cur->str,cur->exenc);

		if (is_percent_encoded(cur->str)) {
			if (cur->type != '+') {
				sprintf(errmsg, "%s: invalid percent encoded string was detected as valid: \"%s\"", __func__, cur->str);
				goto end;
			}
		} else {
			if (cur->type == '+') {
				sprintf(errmsg, "%s: valid percent encoded string was detected as invalid: \"%s\"", __func__, cur->str);
				goto end;
			}
		}
	
		enc = percent_encode(cur->str);
		if (enc == NULL) {
			sprintf(errmsg, "%s: percent_encode returned NULL: \"%s\"", __func__, cur->str);
			goto end;
		}

		if (!is_percent_encoded(enc)) {
			sprintf(errmsg, "%s: encoded string isn't valid. str: \"%s\"", __func__, cur->str);
			goto end;
		}
	
		if (strcmp(enc,cur->exenc) != 0) {
			sprintf(errmsg, "%s: encoded string isn't as expected. str: \"%s\"", __func__, cur->str);
			goto end;
		}
		
		dec = percent_decode(enc);
		if (dec == NULL) {
			sprintf(errmsg, "%s: percent_decode returned NULL: \"%s\"", __func__, cur->str);
			goto end;
		}

		if (strcmp(dec,cur->str) != 0) {
			sprintf(errmsg, "%s: decoded string isn't as expected. str: \"%s\"", __func__, cur->str);
			goto end;
		}

		free(enc);
		enc = NULL;
		free(dec);
		dec = NULL;
		i++;
	}

	res = 1;
end:
	free(enc);
	free(dec);
	return res;

}

static int test_kredis(char * errmsg, const char * name, const struct config_s * config) {
	UNUSED(name);
	UNUSED(config);
	db_t * db = NULL;
	kredis_t * k = NULL;
	char * tmp = NULL;
	int res = 0;


	// connect to database
	if ((db = db_connect(errmsg, &config->db)) == NULL) {
		goto end;
		
	}

	k = db->c;


	/*
	tmp = kredis_get(k, "test:01");
	if (tmp != NULL) {
		printf("tmp[%ld]: \"%s\"\n", strlen(tmp), tmp);
		free(tmp);
	} else
		printf("tmp[-]: nil\n");
	*/


	if (kredis_del(k, "test:01")) {
		sprintf(errmsg, "%s: DEL successful", __func__);
	} else {
		sprintf(errmsg, "%s: DEL failed", __func__);
		goto end;
	}


	tmp = kredis_get(k, "test:01");
	if (tmp != NULL) {
		sprintf(errmsg, "%s: tmp[%ld]: \"%s\"", __func__, strlen(tmp), tmp);
		free(tmp);
	} else
		sprintf(errmsg, "%s: tmp[-]: nil", __func__);

	if (kredis_set(k, "test:01", "   yeet: ' \" { \"hallo\": \"welt\"} EX 123"))
		sprintf(errmsg, "%s: SET successful.", __func__);
	else {
		sprintf(errmsg, "%s: SET FAILED!", __func__);
		goto end;
	}

	tmp = kredis_get(k, "test:01");
	if (tmp != NULL) {
		sprintf(errmsg, "%s: tmp[%ld]: \"%s\"", __func__, strlen(tmp), tmp);
		free(tmp);
	} else {
		sprintf(errmsg, "%s: tmp[-]: nil", __func__);
		goto end;
	}

	if (kredis_set(k, "test:01", "")) {
		sprintf(errmsg, "%s: SET successful.", __func__);
	} else {
		sprintf(errmsg, "%s: SET FAILED!", __func__);
		goto end;
	}

	tmp = kredis_get(k, "test:01");
	if (tmp != NULL) {
		sprintf(errmsg, "%s: tmp[%ld]: \"%s\"", __func__, strlen(tmp), tmp);
		free(tmp);
	} else {
		sprintf(errmsg, "%s: tmp[-]: nil", __func__);
		goto end;
	}

	if (kredis_del(k, "test:01"))
		sprintf(errmsg, "%s: DEL successful.", __func__);
	else {
		sprintf(errmsg, "%s: DEL FAILED!", __func__);
		goto end;
	}

	sprintf(errmsg, "%s: Testing SETS...", __func__);

	if (kredis_del(k, "test:02"))
		sprintf(errmsg, "%s: DEL successful.", __func__);
	else {
		sprintf(errmsg, "%s: DEL FAILED!", __func__);
		goto end;
	}

	char ** elements = NULL;
	size_t elementnum = 0;
	size_t i = 0;

	sprintf(errmsg, "%s: test:02 num elements: %lld", __func__, kredis_set_num(k, "test:02"));
	elements = kredis_set_get(k, "test:02", &elementnum);
	if (elements == NULL) {
		sprintf(errmsg, "%s: test:02 get: returned NULL", __func__);
	} else {
		sprintf(errmsg, "%s: test:02 get: printing %ld elements: ", __func__, elementnum);
		for (i = 0; i < elementnum; i++) {
			sprintf(errmsg, "%s:  - \"%s\"", __func__, elements[i]);
		}
		kredis_set_free(elements, elementnum);
	}

	if (kredis_set_ismember(k, "test:02", "hallu")) {
		sprintf(errmsg, "%s: SISMEMBER true", __func__);
		goto end;
	} else
		sprintf(errmsg, "%s: SISMEMBER false", __func__);

	if (kredis_set_add(k, "test:02", "hallu"))
		sprintf(errmsg, "%s: SADD successful.", __func__);
	else {
		sprintf(errmsg, "%s: SADD FAILED!", __func__);
		goto end;
	}

	if (kredis_set_ismember(k, "test:02", "hallu"))
		sprintf(errmsg, "%s: SISMEMBER true", __func__);
	else {
		sprintf(errmsg, "%s: SISMEMBER false", __func__);
		goto end;
	}

	sprintf(errmsg, "%s: test:02 num elements: %lld", __func__, kredis_set_num(k, "test:02"));
	elements = kredis_set_get(k, "test:02", &elementnum);
	if (elements == NULL) {
		sprintf(errmsg, "%s: test:02 get: returned NULL", __func__);
	} else {
		sprintf(errmsg, "%s: test:02 get: printing %ld elements: ", __func__, elementnum);
		for (i = 0; i < elementnum; i++) {
			sprintf(errmsg, "%s:  - \"%s\"", __func__, elements[i]);
		}
		kredis_set_free(elements, elementnum);
	}

	if (kredis_set_ismember(k, "test:02", "hi")) {
		sprintf(errmsg, "%s: SISMEMBER true", __func__);
		goto end;
	} else
		sprintf(errmsg, "%s: SISMEMBER false", __func__);

	if (kredis_set_add(k, "test:02", "hi"))
		sprintf(errmsg, "%s: SADD successful.", __func__);
	else {
		sprintf(errmsg, "%s: SADD FAILED!", __func__);
		goto end;
	}

	if (kredis_set_ismember(k, "test:02", "hi"))
		sprintf(errmsg, "%s: SISMEMBER true", __func__);
	else {
		sprintf(errmsg, "%s: SISMEMBER false", __func__);
		goto end;
	}

	sprintf(errmsg, "%s: test:02 num elements: %lld", __func__, kredis_set_num(k, "test:02"));
	elements = kredis_set_get(k, "test:02", &elementnum);
	if (elements == NULL) {
		sprintf(errmsg, "%s: test:02 get: returned NULL", __func__);
	} else {
		sprintf(errmsg, "%s: test:02 get: printing %ld elements: ", __func__, elementnum);
		for (i = 0; i < elementnum; i++) {
			sprintf(errmsg, "%s:  - \"%s\"", __func__, elements[i]);
		}
		kredis_set_free(elements, elementnum);
	}

	if (kredis_set_ismember(k, "test:02", "hallu"))
		sprintf(errmsg, "%s: SISMEMBER true", __func__);
	else {
		sprintf(errmsg, "%s: SISMEMBER false", __func__);
		goto end;
	}

	if (kredis_set_add(k, "test:02", "hallu"))
		sprintf(errmsg, "%s: SADD successful.", __func__);
	else {
		sprintf(errmsg, "%s: SADD FAILED!", __func__);
		goto end;
	}

	if (kredis_set_ismember(k, "test:02", "hallu"))
		sprintf(errmsg, "%s: SISMEMBER true", __func__);
	else {
		sprintf(errmsg, "%s: SISMEMBER false", __func__);
		goto end;
	}

	sprintf(errmsg, "%s: test:02 num elements: %lld", __func__, kredis_set_num(k, "test:02"));
	elements = kredis_set_get(k, "test:02", &elementnum);
	if (elements == NULL) {
		sprintf(errmsg, "%s: test:02 get: returned NULL", __func__);
	} else {
		sprintf(errmsg, "%s: test:02 get: printing %ld elements: ", __func__, elementnum);
		for (i = 0; i < elementnum; i++) {
			sprintf(errmsg, "%s:  - \"%s\"", __func__, elements[i]);
		}
		kredis_set_free(elements, elementnum);
	}

	if (kredis_set_ismember(k, "test:02", "")) {
		sprintf(errmsg, "%s: SISMEMBER true", __func__);
		goto end;
	} else
		sprintf(errmsg, "%s: SISMEMBER false", __func__);

	if (kredis_set_add(k, "test:02", ""))
		sprintf(errmsg, "%s: SADD successful.", __func__);
	else {
		sprintf(errmsg, "%s: SADD FAILED!", __func__);
		goto end;
	}

	sprintf(errmsg, "%s: test:02 num elements: %lld", __func__, kredis_set_num(k, "test:02"));
	elements = kredis_set_get(k, "test:02", &elementnum);
	if (elements == NULL) {
		sprintf(errmsg, "%s: test:02 get: returned NULL", __func__);
	} else {
		sprintf(errmsg, "%s: test:02 get: printing %ld elements: ", __func__, elementnum);
		for (i = 0; i < elementnum; i++) {
			sprintf(errmsg, "%s:  - \"%s\"", __func__, elements[i]);
		}
		kredis_set_free(elements, elementnum);
	}

	if (kredis_set_ismember(k, "test:02", ""))
		sprintf(errmsg, "%s: SISMEMBER true", __func__);
	else {
		sprintf(errmsg, "%s: SISMEMBER false", __func__);
		goto end;
	}

	if (kredis_set_rem(k, "test:02", ""))
		sprintf(errmsg, "%s: SREM successful.", __func__);
	else {
		sprintf(errmsg, "%s: SREM FAILED!", __func__);
		goto end;
	}
	sprintf(errmsg, "%s: test:02 num elements: %lld", __func__, kredis_set_num(k, "test:02"));
	elements = kredis_set_get(k, "test:02", &elementnum);
	if (elements == NULL) {
		sprintf(errmsg, "%s: test:02 get: returned NULL", __func__);
	} else {
		sprintf(errmsg, "%s: test:02 get: printing %ld elements: ", __func__, elementnum);
		for (i = 0; i < elementnum; i++) {
			sprintf(errmsg, "%s:  - \"%s\"", __func__, elements[i]);
		}
		kredis_set_free(elements, elementnum);
	}

	if (kredis_set_ismember(k, "test:02", "")) {
		sprintf(errmsg, "%s: SISMEMBER true", __func__);
		goto end;
	} else
		sprintf(errmsg, "%s: SISMEMBER false", __func__);

	if (kredis_set_rem(k, "test:02", "yeet"))
		sprintf(errmsg, "%s: SREM successful.", __func__);
	else {
		sprintf(errmsg, "%s: SREM FAILED!", __func__);
		goto end;
	}
	sprintf(errmsg, "%s: test:02 num elements: %lld", __func__, kredis_set_num(k, "test:02"));
	elements = kredis_set_get(k, "test:02", &elementnum);
	if (elements == NULL) {
		sprintf(errmsg, "%s: test:02 get: returned NULL", __func__);
	} else {
		sprintf(errmsg, "%s: test:02 get: printing %ld elements: ", __func__, elementnum);
		for (i = 0; i < elementnum; i++) {
			sprintf(errmsg, "%s:  - \"%s\"", __func__, elements[i]);
		}
		kredis_set_free(elements, elementnum);
	}

	if (kredis_set_rem(k, "test:02", "hallu"))
		sprintf(errmsg, "%s: SREM successful.", __func__);
	else {
		sprintf(errmsg, "%s: SREM FAILED!", __func__);
		goto end;
	}
	sprintf(errmsg, "%s: test:02 num elements: %lld", __func__, kredis_set_num(k, "test:02"));
	elements = kredis_set_get(k, "test:02", &elementnum);
	if (elements == NULL) {
		sprintf(errmsg, "%s: test:02 get: returned NULL", __func__);
	} else {
		sprintf(errmsg, "%s: test:02 get: printing %ld elements: ", __func__, elementnum);
		for (i = 0; i < elementnum; i++) {
			sprintf(errmsg, "%s:  - \"%s\"", __func__, elements[i]);
		}
		kredis_set_free(elements, elementnum);
	}

	if (kredis_set_rem(k, "test:02", "hi"))
		sprintf(errmsg, "%s: SREM successful.", __func__);
	else {
		sprintf(errmsg, "%s: SREM FAILED!", __func__);
		goto end;
	}

	if (kredis_set_rem(k, "test:03", "yeet"))
		sprintf(errmsg, "%s: SREM successful.", __func__);
	else {
		sprintf(errmsg, "%s: SREM FAILED!", __func__);
		goto end;
	}
	sprintf(errmsg, "%s: test:03 num elements: %lld", __func__, kredis_set_num(k, "test:03"));
	elements = kredis_set_get(k, "test:03", &elementnum);
	if (elements == NULL) {
		sprintf(errmsg, "%s: test:03 get: returned NULL", __func__);
	} else {
		sprintf(errmsg, "%s: test:03 get: printing %ld elements: ", __func__, elementnum);
		for (i = 0; i < elementnum; i++) {
			sprintf(errmsg, "%s:  - \"%s\"", __func__, elements[i]);
		}
		kredis_set_free(elements, elementnum);
	}

	if (kredis_set_ismember(k, "test:03", "asdasd")) {
		sprintf(errmsg, "%s: SISMEMBER true", __func__);
		goto end;
	} else
		sprintf(errmsg, "%s: SISMEMBER false", __func__);

	
	if (kredis_del(k, "test:04"))
		sprintf(errmsg, "%s: DEL successful.", __func__);
	else {
		sprintf(errmsg, "%s: DEL FAILED!", __func__);
		goto end;
	}

	if (kredis_hash_exists(k, "test:04", "yeet")) {
		sprintf(errmsg, "%s: HEXISTS true", __func__);
		goto end;
	} else {
		sprintf(errmsg, "%s: HEXISTS false", __func__);
	}

	tmp = kredis_hash_get(k, "test:04", "yeet");
	if (tmp != NULL) {
		sprintf(errmsg, "%s: tmp[%ld]: \"%s\"", __func__, strlen(tmp), tmp);
		free(tmp);
		goto end;
	} else {
		sprintf(errmsg, "%s: tmp[-]: nil", __func__);
	}

	if (kredis_hash_set(k, "test:04", "yeet", "no")) {
		sprintf(errmsg, "%s: HSET successful.", __func__);
	} else {
		sprintf(errmsg, "%s: HSET FAILED!", __func__);
		goto end;
	}

	if (kredis_hash_exists(k, "test:04", "yeet")) {
		sprintf(errmsg, "%s: HEXISTS true", __func__);
	} else {
		sprintf(errmsg, "%s: HEXISTS false", __func__);
		goto end;
	}

	tmp = kredis_hash_get(k, "test:04", "yeet");
	if (tmp != NULL) {
		sprintf(errmsg, "%s: tmp[%ld]: \"%s\"", __func__, strlen(tmp), tmp);
		free(tmp);
	} else {
		sprintf(errmsg, "%s: tmp[-]: nil", __func__);
		goto end;
	}

	if (kredis_hash_set(k, "test:04", "yeet", "yes")) {
		sprintf(errmsg, "%s: HSET successful.", __func__);
	} else {
		sprintf(errmsg, "%s: HSET FAILED!", __func__);
		goto end;
	}

	if (kredis_hash_exists(k, "test:04", "yeet")) {
		sprintf(errmsg, "%s: HEXISTS true", __func__);
	} else {
		sprintf(errmsg, "%s: HEXISTS false", __func__);
		goto end;
	}

	tmp = kredis_hash_get(k, "test:04", "yeet");
	if (tmp != NULL) {
		sprintf(errmsg, "%s: tmp[%ld]: \"%s\"", __func__, strlen(tmp), tmp);
		free(tmp);
	} else {
		sprintf(errmsg, "%s: tmp[-]: nil", __func__);
		goto end;
	}
	
	if (kredis_del(k, "test:04"))
		sprintf(errmsg, "%s: DEL successful.", __func__);
	else {
		sprintf(errmsg, "%s: DEL FAILED!", __func__);
		goto end;
	}

	if (kredis_hash_exists(k, "test:04", "yeet")) {
		sprintf(errmsg, "%s: HEXISTS true", __func__);
		goto end;
	} else {
		sprintf(errmsg, "%s: HEXISTS false", __func__);
	}

	tmp = kredis_hash_get(k, "test:04", "yeet");
	if (tmp != NULL) {
		sprintf(errmsg, "%s: tmp[%ld]: \"%s\"", __func__, strlen(tmp), tmp);
		free(tmp);
		goto end;
	} else {
		sprintf(errmsg, "%s: tmp[-]: nil", __func__);
	}

	// TODO test kredis_set_with_ex()

	//kredis_free(k);
	sprintf(errmsg, "%s: All tests for kredis passed!", __func__);

	res = 1;
end:
	db_close(db);
	return res;
}


static int test_base64url(char * errmsg, const char * name, const struct config_s * config) {
	int res = 0;
	char * enc = NULL;
	char * dec = NULL;
	struct encoder_testcase_s testcases[] = {
		{'-',"Hallo Welt, ich bin Jake! Yeet!","SGFsbG8gV2VsdCwgaWNoIGJpbiBKYWtlISBZZWV0IQ=="},
		{'-',"(%/)& DS)A  /D=(\"MÖSDLA (=\"U","KCUvKSYgRFMpQSAgL0Q9KCJNw5ZTRExBICg9IlU="},
		//{'+',"",""}, // returns NULL
		{'-',"a","YQ=="},
		{'-',"as?}","YXM_fQ=="},
		{'-',"as?}>sasd>>>>sda","YXM_fT5zYXNkPj4-PnNkYQ=="},
		{'-',"123467898","MTIzNDY3ODk4"},
		{'-',"any carnal pleasure.","YW55IGNhcm5hbCBwbGVhc3VyZS4="},
		{'-',"any carnal pleasure","YW55IGNhcm5hbCBwbGVhc3VyZQ=="},
		{'-',"any carnal pleasur","YW55IGNhcm5hbCBwbGVhc3Vy"},
		{'-',"any carnal pleasu","YW55IGNhcm5hbCBwbGVhc3U="},
		{'-',"any carnal pleas","YW55IGNhcm5hbCBwbGVhcw=="},
		{'-',"pleasure.","cGxlYXN1cmUu"},
		{'-',"leasure.","bGVhc3VyZS4="},
		{'-',"easure.","ZWFzdXJlLg=="},
		{'-',"asure.","YXN1cmUu"},
		{'-',"sure.","c3VyZS4="},
		{'\0',"",""} // TERMINATE ARRAY
	};

	struct encoder_testcase_s * cur;
	size_t i = 0;

	UNUSED(name);
	UNUSED(config);

	while ((cur = &testcases[i])->type != '\0') {
		enc = base64url_encode(cur->str, strlen(cur->str), false);
		if (enc == NULL) {
			sprintf(errmsg, "%s: base64url_encode returned NULL: str: \"%s\"", __func__, cur->str);
			goto end;
		}

		if (strcmp(enc, cur->exenc) != 0) {
			sprintf(errmsg, "%s: encoded string isn't as expected. str: \"%s\"", __func__, cur->str);
			goto end;
		}

		dec = base64url_decode(enc);
		if (dec == NULL) {
			sprintf(errmsg, "%s: base64url_decode returned NULL: str: \"%s\"", __func__, cur->str);
			goto end;
		}

		if (strcmp(dec,cur->str) != 0) {
			sprintf(errmsg, "%s: decoded string isn't as expected. str: \"%s\"", __func__, cur->str);
			goto end;
		}

		free(enc);
		enc = NULL;
		free(dec);
		dec = NULL;
		i++;
	}

	res = 1;
end:
	free(enc);
	free(dec);
	return res;
}

static int test_oauth_code_challenge(char * errmsg, const char * name, const struct config_s * config) {
	/*
	 * ```ruby
	 * require 'digest'
	 * require 'base64'
	 *
	 * puts Base64.urlsafe_encode64(Digest::SHA256.digest("5e201d32-3729-4c3f-9e42-cd2ae16f604533c1ea47-4f17-4248-840d-c7457101f256"))
	 * ```
	 */
	int res = 0;
	char * challenge = NULL;
	struct encoder_testcase_s testcases[] = {
		{'-',"Hallo Welt","LS2hlgWjTgN9voIXP5ipkqUwpf3VPa2IL1cNS6IE7zA"},
		{'-',"5e201d32-3729-4c3f-9e42-cd2ae16f604533c1ea47-4f17-4248-840d-c7457101f256","W4z8T8C0dwPRhPval-9X1cqJW0hMlOctYPJWwBoq6VA"},
		{'\0',"",""} // TERMINATE ARRAY
	};

	struct encoder_testcase_s * cur;
	size_t i = 0;

	UNUSED(name);
	UNUSED(config);

	while ((cur = &testcases[i])->type != '\0') {
		challenge = oauth_gen_code_challenge(cur->str);
		if (challenge == NULL) {
			sprintf(errmsg, "%s: oauth_gen_code_challenge returned NULL: str: \"%s\"", __func__, cur->str);
			goto end;
		}

		if (strcmp(challenge, cur->exenc) != 0) {
			fprintf(stderr, "[DEBUG] %s: str: \"%s\" expected: \"%s\" got: \"%s\"\n", __func__, cur->str, cur->exenc, challenge);
			sprintf(errmsg, "%s: challenge isn't as expected. str: \"%s\"", __func__, cur->str);
			goto end;
		}

		free(challenge);
		challenge = NULL;
		i++;
	}

	res = 1;
end:
	free(challenge);
	return res;
}

static int test_hmac_sha256(char * errmsg, const char * name, const struct config_s * config) {
	int res = 0;
	struct hmac_testcase_s {
		char type; // '+': str is valid '-': str is invalid '\0': end of array
		char msg[256]; // message
		char key[256]; // key
		char hash[256]; // expected hash (base64url encoded)
	} testcases[] = {
		{'+',"The quick brown fox jumps over the lazy dog","key","97yD9DBThCSxMpjmqm+xQ+9NWaFJRhdZl0edvC0aPNg="},
		{'-',"Th1 quick brown fox jumps over the lazy dog","key","97yD9DBThCSxMpjmqm+xQ+9NWaFJRhdZl0edvC0aPNg="},
		{'+',"Hallo Welt","b6d0364e-d8bf-425c-86e4-69950aa5ac53f555a973-51ae-4c9d-984c-71b7122499f3","AqTexrdsTibiqAhnr0f1EQFf2dbklD8d8Z9RjVN6V1U="},
		{'\0',"","",""} // TERMINATE ARRAY
	};
	struct hmac_testcase_s * cur = NULL;
	size_t i = 0;
	unsigned char curhash[SHA256_DIGEST_LENGTH];
	char * curhashenc = NULL;

	UNUSED(name);
	UNUSED(config);

	while ((cur = &testcases[i])->type != '\0') {
		if (!hmac_sha256(curhash, cur->msg, cur->key)) {
			sprintf(errmsg, "%s: hmac_sha256 failed: msg: \"%s\"", __func__, cur->msg);
			goto end;
		}

		curhashenc = base64_encode((char *)curhash, SHA256_DIGEST_LENGTH);
		if (curhashenc == NULL) {
			sprintf(errmsg, "%s: base64 encoding of hash failed: msg: \"%s\"", __func__, cur->msg);
			goto end;
		}

		if (strcmp(curhashenc,cur->hash) != 0) {
			if (cur->type == '+') {
				sprintf(errmsg, "%s: valid hash isn't as expected. msg: \"%s\" got hash: \"%s\"", __func__, cur->msg, curhashenc);
				goto end;
			}
		} else {
			if (cur->type != '+') {
				sprintf(errmsg, "%s: invalid hash isn't as expected. msg: \"%s\" got hash: \"%s\"", __func__, cur->msg, curhashenc);
				goto end;
			}
		
		}

		free(curhashenc);
		curhashenc = NULL;

		i++;
	}

	res = 1;
end:
	free(curhashenc);
	return res;
}

static int test_jwt(char * errmsg, const char * name, const struct config_s * config) {
	int res = 0;
	const char * payloadraw = "{\"sub\": \"1234567890\",\"name\": \"John Doe\",\"iat\": 1516239022}";
	const char * key = "your-256-bit-secret";
	const char * expenc = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
	cJSON * payload = NULL;
	cJSON * decpayload = NULL;
	char * enc = NULL;

	UNUSED(name);
	UNUSED(config);

	if ((payload = cJSON_Parse(payloadraw)) == NULL) {
		sprintf(errmsg, "%s: failed parsing payload", __func__);
		goto end;
	}

	if ((enc = jwt_encode_and_sign(payload, key)) == NULL) {
		sprintf(errmsg, "%s: failed encoding or signing", __func__);
		goto end;
	}

	if (strcmp(enc,expenc) != 0) {
		//fprintf(stderr, "[DEBUG] %s: enc: \"%s\"\n      expenc: \"%s\"\n",__func__,enc,expenc);
		sprintf(errmsg, "%s: enc differs from expenc", __func__);
		goto end;
	}

	if (!jwt_verify_encoding(enc)) {
		sprintf(errmsg, "%s: failed verifying encoding", __func__);
		goto end;
	}

	if (!jwt_verify_signature(enc, key)) {
		sprintf(errmsg, "%s: failed verifying signature", __func__);
		goto end;
	}


	if ((decpayload = jwt_decode(enc)) == NULL) {
		sprintf(errmsg, "%s: failed decoding", __func__);
		goto end;
	}

	if (!cJSON_Compare(payload, decpayload, 1)) {
		sprintf(errmsg, "%s: payloads don't match", __func__);
		goto end;
	}

	if (jwt_verify_encoding("hallo.welt")) {
		sprintf(errmsg, "%s: succeeded verifying encoding but it should've failed", __func__);
		goto end;
	}
	if (jwt_verify_signature("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_bdQssw5c", key)) {
		sprintf(errmsg, "%s: succeeded verifying signature but it should've failed", __func__);
		goto end;
	}

	res = 1;
end:
	cJSON_Delete(decpayload);
	free(enc);
	cJSON_Delete(payload);
	return res;
}

static int test_tmplt_engine(char * errmsg, const char * name, const struct config_s * config) {
	int res = 0;
	char * tmp = NULL;
	const struct tmplt_kvp_s data[] = {
		{"foo","bar",true},
		{"420","Yeet",true},
		{"name","Jake",false},
		{"name2","Rüdiger",false},
		{"Hello","World",true},
		{NULL,NULL,false} // TERMINATE ARRAY
	};
	
	struct encoder_testcase_s testcases[] = {
		{'+',"",""},
		{'+',"String without key.","String without key."},
		{'+',"String with single % sign.","String with single % sign."},
		{'+',"Hello %%name%%. I like you.","Hello Jake. I like you."},
		{'+',"Hello %%name2%%. I like you.","Hello R%c3%bcdiger. I like you."},
		{'+',"a%%foo%%","abar"},
		{'+',"%%foo%%b%%name%%c","barbJakec"},
		{'+',"%%foo%%b%%name2%%c","barbR%c3%bcdigerc"},
		{'+',"%%foo%%b","barb"},
		{'+',"a%%foo%%b","abarb"},
		{'+',"%%foo%%","bar"},
		{'+',"%%name%%","Jake"},
		{'+',"%%name2%%","R%c3%bcdiger"},
		{'+',"Hello: %%Hello%% 420: %%420%% name: %%name%% name2: %%name2%% foo: %%foo%%","Hello: World 420: Yeet name: Jake name2: R%c3%bcdiger foo: bar"},
		{'-',"Hello %%dontexist%%.",""},
		{'-',"Hello %%name%%. I %%inavlidkey% you.",""},
		{'\0',"",""} // TERMINATE ARRAY
	};

	struct encoder_testcase_s * cur;
	size_t i = 0;

	UNUSED(name);
	UNUSED(config);

	while ((cur = &testcases[i])->type != '\0') {
		tmp = tmplt_render(errmsg, cur->str, data);
		if (cur->type == '+') {
			// valid template
			if (tmp == NULL) {
				strcat(errmsg, "str: \"");
				strcat(errmsg, cur->str);
				strcat(errmsg, "\"");
				goto end;
			}

			if (strcmp(tmp, cur->exenc) != 0) {
				fprintf(stderr, "[DEBUG] %s: str: \"%s\" expected: \"%s\" got: \"%s\"\n", __func__, cur->str, cur->exenc, tmp);
				sprintf(errmsg, "%s: rendered string isn't as expected. str: \"%s\"", __func__, cur->str);
				goto end;
			}
		} else {
			// invalid template
			if (tmp != NULL) {
				sprintf(errmsg, "%s: invalid template wasn't rejected. str: \"%s\"", __func__, cur->str);
				goto end;
			}
		}

		free(tmp);
		tmp = NULL;
		i++;
	}

	res = 1;
end:
	free(tmp);
	return res;
}

/**
 * @brief main function for test-util
 *
 * @param[in] argc number of arguments from the command line
 * @param[in] argv arguments from the command line
 * @param[in] envp environment variables
 *
 * @returns the exit code of this program.
 * @retval 0 success
 * @retval !0 failure
 */
int main(int argc, char **argv, char **envp) {
	int res = 1;
	char errmsg[ERRMSGMAXSIZE];
	int option;
	struct config_s * config = NULL;
	//cJSON * authsettings = NULL;

	char configpath[MAXFILEPATHLEN + 1];

	int testall = 0;
	int found = 0;
	int count = 0;
	int expectedcount = 0;
	int numpassed = 0;
	int numfailed = 0;
	int numskipped = 0;

	struct testable_s * cur;
	size_t i = 0;
	
	struct testable_s testables[] = { // worked through from top to bottom
		{"uuidv4_verification","Tests UUIDv4 verification.",test_uuid_verification,false,0,""},
		{"uuidv4_generation","Tests UUIDv4 generation.",test_uuid_generation,false,0,""},
		{"percent","Tests percent encoding and decoding + verification.",test_percent,false,0,""},
		{"kredis","Tests kredis. Requires a redis database.",test_kredis,false,0,""},
		{"base64url","Tests base64url encoding and decoding.",test_base64url,false,0,""},
		{"oauth_code_challenge","Tests the oauth code_challenge generator.",test_oauth_code_challenge,false,0,""},
		{"hmac_sha256","Tests the hmac_sha256 calculator.",test_hmac_sha256,false,0,""},
		{"jwt","Tests JWT encoding, signing & decoding.",test_jwt,false,0,""},
		{"template_engine","Tests the template engine..",test_tmplt_engine,false,0,""},
		{"","",NULL,false,0,""} // TERMINATE ARRAY
	};

	UNUSED(envp);

	errmsg[0] = '\0';

	strcpy(configpath, CONFIGFILEPATH); // default config file path


	// parse arguments
	while ((option = getopt(argc, argv, ":ahc:")) != -1) {
		switch (option) {
			case 'a':
				testall = 1;
				break;
			case 'h':
				printusage(argv[0], testables);
				res = 1;
				goto end;
			case 'c':
				if (strlen(optarg) > MAXFILEPATHLEN) {
					sprintf(errmsg, "%s: config path too long", __func__);
					printerror(errmsg);
					goto end;
				}
				strcpy(configpath, optarg);
				break;
			case ':':
				sprintf(errmsg, "%s: option needs a value", __func__);
				printerror(errmsg);
				goto end;
			case '?':
				sprintf(errmsg, "%s: unknown option", __func__);
				printerror(errmsg);
				fprintf(stderr, "option: %c\n", optopt);
				goto end;

		}
	}

	// mark parts to be tested
	if (testall) {
		i = 0;
		while ((cur = &testables[i])->test_func_ptr != NULL) {
			cur->marked = true;
			expectedcount++;
			i++;
		}
	}
	for(; optind < argc; optind++){
		found = 0;
		i = 0;
		while (!found && (cur = &testables[i])->test_func_ptr != NULL) {
			if (strcmp(argv[optind],cur->name) == 0) {
				found = 1;
				if (testall) {
					cur->marked = false;
					expectedcount--;
				} else {
					cur->marked = true;
					expectedcount++;
				}
			}
			i++;
		}
		if (!found) {
			sprintf(errmsg, "%s: PART not found!", __func__);
			printerror(errmsg);
			fprintf(stderr, "PART: \"%s\"\n", argv[optind]);
			goto end;
		}
	}

	if (expectedcount == 0) {
		sprintf(errmsg, "%s: no PART marked for testing.", __func__);
		printerror(errmsg);
		goto end;
		
	}


	// read config
	if ((config = config_parse(errmsg, configpath)) == NULL) {
		printerror(errmsg);
		goto end;
	}

	// execute tests
	i = 0;
	while ((cur = &testables[i])->test_func_ptr != NULL) {
		if (cur->marked) {
			count++;
			fprintf(stdout, COLOR_YELLOW"===>>> Testing[%d/%d]: %s <<<===\n"COLOR_RESET,count,expectedcount,cur->name );
			cur->res = cur->test_func_ptr(cur->errmsg, cur->name, config);
			if (cur->res) {
				numpassed++;
				fprintf(stdout, COLOR_GREEN"===>>> Passed! <<<===\n"COLOR_RESET);
			} else {
				numfailed++;
				fprintf(stdout, COLOR_RED"===>>> Failed! <<<===\n"COLOR_RESET);
				fprintf(stdout, COLOR_RED"Error Message: "COLOR_RESET"\"%s\"\n"COLOR_RESET, cur->errmsg);
			}
		} else {
			fprintf(stdout, COLOR_CYAN"===>>> Skipping test: %s <<<===\n"COLOR_RESET,cur->name );
			numskipped++;
		}
		i++;
	}

	// print summary
	fprintf(stdout, COLOR_YELLOW"\n===>>> Test Summary: <<<===\n"COLOR_RESET);
	fprintf(stdout, COLOR_YELLOW"Skipped: "COLOR_CYAN"%d"COLOR_RESET"\n", numskipped);
	fprintf(stdout, COLOR_YELLOW"Passed: "COLOR_GREEN"%d"COLOR_RESET"\n", numpassed);
	fprintf(stdout, COLOR_YELLOW"Failed: "COLOR_RED"%d"COLOR_RESET"\n", numfailed);
	fprintf(stdout, COLOR_YELLOW"Total(Passed + Failed): "COLOR_RESET"%d\n", numfailed+numpassed);
	fprintf(stdout, "\n"COLOR_RESET);


	res = numfailed;
end:
	fflush(stdout);
	config_free(config);
	return res;
}
