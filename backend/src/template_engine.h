#ifndef TEMPLATE_ENGINE_H_
#define TEMPLATE_ENGINE_H_

#include <stdbool.h>
#include <stdio.h>


/**
 * @file
 * @brief Header file for template_engine.c
 */

/**
 * @brief Path to the directory containing the templates.
 */
#define TEMPLATES_DIR "../../frontend/templates"

/**
 * @brief Indicator for the beginning of a key.
 */
#define TEMPLATE_KEY_OPEN "%%"
/**
 * @brief Indicator for the ending of a key.
 */
#define TEMPLATE_KEY_CLOSE "%%"


/**
 * @brief Template Key-Value-Pair Struct.
 *
 * @note An Array of tmplt_kvp_s should be terminated by an element with key set to NULL.
 */
struct tmplt_kvp_s {
	const char * key; ///< the key
	const char * value; ///< the value that will replace %%key%%
	bool is_safe; ///< whether the string is safe and can be used as is, or the string is unsafe and needs to be encoded
};


char * tmplt_render(char * errmsg, const char * tmplt, const struct tmplt_kvp_s * data);
char * tmplt_render_from_file(char * errmsg, const char * tmplt_file_path, const struct tmplt_kvp_s * data);

#endif
