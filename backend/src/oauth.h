#ifndef OAUTH_H_
#define OAUTH_H_

#include "config.h"

#include "../cJSON/cJSON.h"

#include <stdbool.h>

/**
 * @file
 * @brief Header file for oauth.c
 */

bool oauth_verify_state_encoding(const char * state);
char * oauth_gen_state(void);
char * oauth_gen_code_verifier(void);
char * oauth_gen_code_challenge(const char * code_verifier);
char * oauth_gen_url(char * errmsg, const struct config_auth_s * authconfig, const char * state);

#endif
