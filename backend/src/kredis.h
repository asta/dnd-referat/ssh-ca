#ifndef KREDIS_H_
#define KREDIS_H_

#include <stdlib.h>
#include <stdbool.h>
#include <sys/time.h>

/**
 * @file
 * @brief Header file for kredis.c
 */


/**
 * @brief Extra prefix for every key
 */
#define KREDIS_PREFIX "sshca:"

/**
 * @brief The krefis context struct
 * @see kredis_t
 */
struct kredis_s;

/**
 * @brief A kredis context
 */
typedef struct kredis_s kredis_t;

kredis_t * kredis_connect(const char * ip, int port, bool is_unix_socket, struct timeval timeout,  const char * auth);
void kredis_free(kredis_t * k);

bool kredis_ping(kredis_t * k); // PING
int kredis_error(kredis_t * k);
const char * kredis_error_string(kredis_t * k);

char * kredis_get(kredis_t * k, const char * key);                               // GET
bool   kredis_set(kredis_t * k, const char * key, const char * value);           // SET
bool   kredis_set_with_ex(kredis_t * k, const char * key, const char * value, long ex); // SET with EX set
bool   kredis_del(kredis_t * k, const char * key);                               // DEL

bool   kredis_set_add(kredis_t * k, const char * key, const char * member);      // SADD
long long kredis_set_num(kredis_t * k, const char * key);                        // SCARD
bool   kredis_set_ismember(kredis_t * k, const char * key, const char * member); // SISMEMBER
char** kredis_set_get(kredis_t * k, const char * key, size_t * nummembers);      // SMEMBERS
bool   kredis_set_rem(kredis_t * k, const char * key, const char * member);      // SREM
void   kredis_set_free(char ** elements, size_t nummemebers);


bool   kredis_hash_set(kredis_t * k, const char * key, const char * field, const char * value); // HSET
bool   kredis_hash_exists(kredis_t * k, const char * key, const char * field);                  // HEXISTS
char * kredis_hash_get(kredis_t * k, const char * key, const char * field);                     // HGET

#endif
