#include "common.h"
#include "cgi.h"
#include "oauth.h"
#include "config.h"

#include "../cJSON/cJSON.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>


/**
 *
 * @file
 *
 * @brief oauth url generator. handles endpoint at /cgi/requestoauthurl
 *
 * Request:
 * Expects a GET Request.
 * The QUERY_STRING may contain a `as` field specifying the Content-Type of the response. (Supported are `json`,`redirect` and `plain`. Default: `json`)
 *
 * Response as json (if successful):
 * Content-Type: `application/json`
 * The Body should contain a JSON Object with an `oauthurl` string field.
 *
 * Response as plain (if successful):
 * Content-Type: `text/plain`
 * The Body should contain `oauthurl`.
 *
 *
 * Response as redirect (if successful):
 * Content-Type: `text/html`
 * Status: 302
 * Location: `oauthurl`
 */

/**
 * @brief main function for requestoauthurl
 *
 * @returns the exit code of this program.
 * @retval 0 success
 * @retval !0 failure
 */
int main(void) {
	char errmsg[ERRMSGMAXSIZE];
	int statuscode = 200;
	struct config_s* config = NULL;
	char * qrystr = NULL;
	struct cgi_kvp_s * qry = NULL;
	const char * qry_as = NULL;

	char * oauth_state = NULL;

	char * resurl = NULL;



	errmsg[0] = '\0';

	if ((statuscode = cgi_testenv(errmsg, 'g', NULL)) != 200) {
		cgi_printerror(errmsg, statuscode);
		goto end;
	}

	// read QUERY_STRING
	if ((qrystr = getenv("QUERY_STRING")) != NULL && strlen(qrystr) > 0 && (qry = cgi_parse_query_string(qrystr)) == NULL) {
		statuscode = 400;
		sprintf(errmsg, "%s: Failed parsing QUERY_STRING.", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}

	// read config
	if ((config = config_parse(errmsg, CONFIGFILEPATH)) == NULL) {
		statuscode = 500;
		cgi_printerror(errmsg, statuscode);
		goto end;
	}


	// generate oauth state
	if ((oauth_state = oauth_gen_state()) == NULL) {
		statuscode = 500;
		sprintf(errmsg, "%s: Failed generating oauth state.", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}

	// generate oauth url
	if ((resurl = oauth_gen_url(errmsg, &config->auth, oauth_state)) == NULL) {
		statuscode = 500;
		cgi_printerror(errmsg, statuscode);
		goto end;
	}
	
	// read query: 'as' 
	if ((qry_as = cgi_kvp_get(qry, "as")) == NULL)
		qry_as = "json"; // default


	if (strcmp(qry_as, "json") == 0) {
		//printf("Content-Type: text/html\r\n");
		printf("Content-Type: application/json\r\n");
		printf("Status: %d\r\n", statuscode);

		/* --------------------------------------------------------------- */
		printf("\r\n");
		/* --------------------------------------------------------------- */

		printf("{\r\n");
		printf("\"oauthurl\":\"%s\"\r\n", resurl);
		printf("}\r\n");
	} else if (strcmp(qry_as, "plain") == 0) {
		printf("Content-Type: text/plain\r\n");
		printf("Status: %d\r\n", statuscode);

		/* --------------------------------------------------------------- */
		printf("\r\n");
		/* --------------------------------------------------------------- */
		printf("%s", resurl);
	} else if (strcmp(qry_as, "redirect") == 0) {
		statuscode = 302;
		printf("Content-Type: text/html\r\n");
		printf("Status: %d\r\n", statuscode);
		printf("Location: %s\r\n", resurl);

		/* --------------------------------------------------------------- */
		printf("\r\n");
		/* --------------------------------------------------------------- */
		printf("<a href=\"%s\">%s</a>", resurl, resurl);
	} else {
		statuscode = 400;
		sprintf(errmsg, "%s: Unsupported 'as' query value", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	
	}

end:
	fflush(stdout);
	cgi_kvp_free(qry);
	free(resurl);
	free(oauth_state);
	config_free(config);
	return 0;
}
