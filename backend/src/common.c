#include "common.h"

#include <openssl/rand.h>
#include <openssl/sha.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

#include "../cJSON/cJSON.h"

#include "../libb64/include/b64/cencode.h"
#include "../libb64/include/b64/cdecode.h"

/**
 * @file
 *
 * @brief A bunch of useful/common code.
 *
 */


/** @brief Generate a Version 4 UUID according to RFC-4122
 *
 * Uses the openssl RAND_bytes function to generate a
 * Version 4 UUID.
 *
 * @param[in,out] buffer A buffer that is at least (UUIDLEN + 1) bytes long.
 * @retval true success
 * @retval false failure
 *
 * @note Code from https://gist.github.com/kvelakur/9069c9896577c3040030
 */
bool uuid_v4_gen(char *buffer)
{
	union
	{
		struct
		{
			uint32_t time_low;
			uint16_t time_mid;
			uint16_t time_hi_and_version;
			uint8_t  clk_seq_hi_res;
			uint8_t  clk_seq_low;
			uint8_t  node[6];
		} s;
		uint8_t __rnd[16];
	} uuid;

	bool rc = (RAND_bytes(uuid.__rnd, sizeof(uuid)) == 1);

	/* Refer Section 4.2 of RFC-4122 */
	/* https://tools.ietf.org/html/rfc4122#section-4.2 */
	uuid.s.clk_seq_hi_res = (uint8_t) ((uuid.s.clk_seq_hi_res & 0x3F) | 0x80);
	uuid.s.time_hi_and_version = (uint16_t) ((uuid.s.time_hi_and_version & 0x0FFF) | 0x4000);
	/* 8 + 1 + 4 + 1 + 4 + 1+ 2 +2 + 1 + 12 */
	buffer[0] = '\0';
	sprintf(buffer, "%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x",
			uuid.s.time_low, uuid.s.time_mid, uuid.s.time_hi_and_version,
			uuid.s.clk_seq_hi_res, uuid.s.clk_seq_low,
			uuid.s.node[0], uuid.s.node[1], uuid.s.node[2],
			uuid.s.node[3], uuid.s.node[4], uuid.s.node[5]);

	return rc;
}


/**
 * @brief Tests whether a given string is a valid UUIDv4.
 *
 * A UUIDv4 looks like this: 034d9d19-0182-4fd8-aa18-21ffd18bf956
 *
 * @see uuid_v4_gen()
 *
 * @param[in] buffer the string to be tested
 * 
 * @returns whether buffer is a valid UUIDv4.
 * @retval false not a valid UUIDv4
 * @retval true a valid UUIDv4
 */
bool is_valid_uuid_v4(const char *buffer) {
	int i;
	int counter = 0;

	if (buffer == NULL || strlen(buffer) != UUIDLEN)
		return false;

/* 034d9d19-0182-4fd8-aa18-21ffd18bf956	 */
	for (i = 0; i < 8; i++) {
		if (!is_valid_hex(buffer[counter++]))
			return false;
	}
	if (buffer[counter++] != '-')
			return false;
	for (i = 0; i < 4; i++) {
		if (!is_valid_hex(buffer[counter++]))
			return false;
	}
	if (buffer[counter++] != '-')
			return false;
	for (i = 0; i < 4; i++) {
		if (!is_valid_hex(buffer[counter++]))
			return false;
	}
	if (buffer[counter++] != '-')
			return false;
	for (i = 0; i < 4; i++) {
		if (!is_valid_hex(buffer[counter++]))
			return false;
	}
	if (buffer[counter++] != '-')
			return false;
	for (i = 0; i < 12; i++) {
		if (!is_valid_hex(buffer[counter++]))
			return false;
	}
	return true;
}

/**
 * @brief Tests whether a character is a valid hexadecimal character.
 *
 * Valid hexadecimal characters are: 0-9 a-f A-F
 *
 * @param[in] c the character to be tested
 *
 * @returns whether c is a valid hex character.
 * @retval false not a valid UUIDv4
 * @retval true a valid UUIDv4
 */
bool is_valid_hex(char c) {
	return (c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F');
}


/**
 * @brief Converts a hexadecimal character to a number.
 *
 * Basically '0'-'9' are mapped to 0-9 and 'a'-'f' or 'A'-'F' are mapped to 10-15.
 *
 * A non-hexadecimal character is just treated as '0'.
 *
 * @param[in] c the character
 *
 * @returns c as a number
 *
 *
 */
int hex_to_int(char c) {
	if (c >= '0' && c <= '9') {
		return c - '0';
	}
	c = (char)tolower(c);
	if (c >= 'a' && c <= 'f') {
		return c - 'a' + 10;
	}
	return 0;
}

/**
 * @brief Converts a number to an uppercase hexadecimal character.
 *
 * Basically 0-9 are mapped to '0'-'9' and 10-15 are mapped to 'A'-'F'.
 *
 * A number larger than 15 or smaller than 0 is just treated as 0.
 *
 * @param[in] x the number
 *
 * @returns x as an uppercase hexadecimal character
 *
 */
char int_to_hex(int x) {
	if (x >= 0 && x <= 9)
		return '0' + (char)x;
	if (x >= 10 && x <= 15)
		return 'a' + (char)x - (char)10;
	return '0';
}

/**
 * @brief Tests whether a given string is correctly percent encoded.
 *
 * Conforming to [RFC 3986 §2.1](https://datatracker.ietf.org/doc/html/rfc3986#section-2.1).
 *
 * @param[in] str the string to be tested
 * 
 * @returns whether str is correctly percent encoded.
 */
bool is_percent_encoded(const char * str) {
	size_t len;
	int mode = 0; /* 0: expecting unreserved char or percent // 1: expecting first hex  // 2: expecting second hex */
	size_t i;
	char c;

	if (str == NULL)
		return false;

	len = strlen(str);

	for (i = 0; i < len; i++) {
		c = str[i];
		switch (mode) {
		case 0:
			if (c == '%') {
				mode = 1;
				break;
			} else if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '-' || c == '.' || c == '_' || c == '~') {
				break;
			} else {
				return false;
			}
		case 1:
			if (is_valid_hex(c)) {
				mode = 2;
				break;
			} else {
				return false;
			}
		case 2:
			if (is_valid_hex(c)) {
				mode = 0;
				break;
			} else {
				return false;
			}
		default:
			fprintf(stderr, "WARN: is_percent_encoded: unknown mode!!\n");
			return false;
		}
	}

	return (mode == 0);
}

/**
 * @brief Decodes a percent encoded string.
 *
 * This function expects str to be conforming to [RFC 3986 §2.1](https://datatracker.ietf.org/doc/html/rfc3986#section-2.1).
 * You can test if str is valid using is_percent_encoded().
 *
 * @note The sequence "%00" may cause undefined behaviour as it will just null-terminate the string at that point.
 *
 * @param[in] str the percent encoded string to be decoded
 *
 * @return the decoded string
 * @retval NULL an error occurred and errno might be set
 *
 * The returned string must be freed using free().
 */
char * percent_decode(const char * str) {
	char * res;
	int r = 0;
	int w = 0;
	char c;
	char buf[3];

	if (str == NULL)
		return NULL;

	res = malloc(sizeof(char) * (strlen(str) +1));
	if (res == NULL)
		return NULL;

	buf[2] = '\0';
	while ((c = str[r]) != '\0') {
		if (c == '%') {
			buf[0] = str[r+1];	
			buf[1] = str[r+2];	
			res[w] = (char)(strtol(buf,NULL,16));
			r+=2;
		} else {
			res[w] = str[r];
		}
		w++;
		r++;
	}
	res[w] = '\0';
	return res;
}

/**
 * @brief Percent encodes a string.
 *
 * Conforming to [RFC 3986 §2.1](https://datatracker.ietf.org/doc/html/rfc3986#section-2.1).
 *
 * @note This function cannot handle '\0' characters as those are used for indicating the end of a string in C.
 *
 * @param[in] str the string to be encoded
 *
 * @return the encoded string
 * @retval NULL an error occurred and errno might be set
 *
 * The returned string must be freed using free().
 */
char * percent_encode(const char * str) {
	size_t ressize;
	char * res;
	int r = 0;
	int w = 0;
	char c;

	if (str == NULL)
		return NULL;

	ressize = strlen(str) + 1;
	res = malloc(sizeof(char) * ressize);
	if (res == NULL)
		return NULL;

	while ((c = str[r]) != '\0') {
		if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '-' || c == '.' || c == '_' || c == '~') {
			res[w] = str[r];
		} else {
			ressize += 2;
			res = realloc(res, ressize);
			if (res == NULL)
				return NULL;
			res[w]   = '%';
			res[w+1] = int_to_hex((unsigned char)(((unsigned char)(((unsigned char)c) & 0xf0)) >> 4) );
			res[w+2] = int_to_hex((unsigned char)(((unsigned char)(((unsigned char)c) & 0x0f))));
			/* printf("percent_encode: c: '%c' (int): %d (hex): %X\n", c, c, c); */
			w+=2;
		}
		w++;
		r++;
	}
	res[w] = '\0';
	return res;

}

/**
 * @brief Tests whether str starts with pre.
 *
 * @param[in] pre the expected prefix string
 * @param[in] str the string to be tested
 *
 * @returns whether str starts with pre.
 */
bool isprefix(const char *pre, const char *str) {
	return strncmp(pre, str, strlen(pre)) == 0;
}


/**
 * @brief Reads a file into a string.
 *
 * Reads the file at path and converts it into a null-terminated string.
 *
 * @note This function is not suitable for binary files that may contain '\0' characters.
 *
 * @returns The files contents as a string.
 * @retval NULL an error occurred and errno might be set.
 *
 * The returned string must be freed using free().
 */
char * filetostr(const char * path) {
	char * buffer = NULL;
	size_t length;
	FILE * f = fopen (path, "rb");

	if (f) {
		fseek (f, 0, SEEK_END);
		length = (size_t)ftell(f);
		fseek (f, 0, SEEK_SET);
		buffer = malloc (sizeof(char) * (length +1));
		if (buffer) {
			buffer[length] = '\0';
			if (fread(buffer, 1, length, f)==0) {
				//fprintf(stderr, "Warn: fread might have failed\n");
				if (!feof(f) || ferror(f)) {
					free(buffer);
					buffer = NULL;
				}
			
			}
		}
		fclose(f);
	}

	return buffer;
}

/**
 * @brief Reads a JSON file.
 *
 * Reads the file at path and parses it into a cJSON object.
 *
 * @returns The files contents as a cJSON object.
 * @retval NULL an error occurred and errno might be set.
 *
 * The returned cJSON object must be freed using cJSON_Delete().
 */
cJSON * filetojson(const char * path) {
	char * buffer = NULL;
	cJSON * json = NULL;
	if ((buffer = filetostr(path)) == NULL) {
		return NULL;
	}
	json = cJSON_Parse(buffer); // might be NULL on failure
	free(buffer);
	return json;
}

/**
 * @brief base64 encodes a string
 *
 * Uses libb64 to encode a string.
 *
 * @param[in] str  the input string to be encoded
 * @param[in] len  the length of the string (if str isn't raw binary this should be strlen(str))
 * 
 * @returns the encoded string
 * @retval NULL an error occurred and errno might be set
 *
 * The returned string must be freed using free().
 */
char * base64_encode(const char * str, size_t len) {
	char * res = NULL;
	char * c = NULL;
	int cnt = 0;
	size_t reslen;
	//size_t len;
	
	base64_encodestate s;

	if (str == NULL)
		return NULL;
	
	
	/* initialise the encoder state */
	base64_init_encodestate(&s);

	s.chars_per_line = 0; // no linebreaks

	//len = strlen(str);
	reslen = base64_encode_length(len, &s);
	if (reslen == 0)
		return NULL;

	res = malloc(sizeof(char) * (reslen + 1 + 16)); // +16 because I don't really trust libb64
	if (res == NULL)
		return NULL;

	/* keep track of our encoded position */
	c = res;

	/*---------- START ENCODING ----------*/
	/* gather data from the input and send it to the output */
	cnt = base64_encode_block(str, len, c, &s);
	c += cnt;
	/* since we have encoded the entire input string, we know that 
	   there is no more input data; finalise the encoding */
	cnt = base64_encode_blockend(c, &s);
	c += cnt;
	/*---------- STOP ENCODING  ----------*/
	
	/* we want to print the encoded data, so null-terminate it: */
	*c = '\0';

	return res;
}

/**
 * @brief decodes a base64 encoded string
 *
 * Uses libb64 to decode a string.
 *
 * @param[in] str  the encoded input string to be decoded
 * 
 * @returns the decoded string
 * @retval NULL an error occurred and errno might be set
 *
 * The returned string must be freed using free().
 */
char * base64_decode(const char * str) {
	char * res = NULL;
	char * c = NULL;
	int cnt = 0;
	size_t reslen;
	size_t len;
	
	base64_decodestate s;

	if (str == NULL)
		return NULL;
	
	
	/* initialise the decoder state */
	base64_init_decodestate(&s);

	len = strlen(str);
	reslen = base64_decode_maxlength(len);
	if (reslen == 0)
		return NULL;

	res = malloc(sizeof(char) * (reslen + 1 + 16)); // +16 because I don't really trust libb64
	if (res == NULL)
		return NULL;

	/* keep track of our decoded position */
	c = res;

	/*---------- START DECODING ----------*/
	/* initialise the decoder state */
	base64_init_decodestate(&s);
	/* decode the input data */
	cnt = base64_decode_block(str, len, c, &s);
	c += cnt;
	/* note: there is no base64_decode_blockend! */
	/*---------- STOP DECODING  ----------*/
	
	/* we want to print the decoded data, so null-terminate it: */
	*c = '\0';

	return res;

}

/**
 * @brief base64url encodes a string
 *
 * Conforming to [RFC 4648 §5](https://datatracker.ietf.org/doc/html/rfc4648#section-5).
 *
 * @note You might wish to use percent_encode() on the result of this function before passing it over into a url, as padding is still using '=' characters.
 * Or you just disable padding with skippadding.
 *
 * @param[in] str  the input string to be encoded
 * @param[in] len  the length of the string (if str isn't raw binary this should be strlen(str))
 * @param[in] skippadding whether the padding at the end should be skipped
 * 
 * @returns the encoded string
 * @retval NULL an error occurred and errno might be set
 *
 * The returned string must be freed using free().
 */
char * base64url_encode(const char * str, size_t len, bool skippadding) {
	char * res = base64_encode(str, len); // base64_encode also checks if str == NULL
	char c;
	size_t i = 0;

	if (res == NULL)
		return NULL;

	while ((c = res[i]) != '\0') {
		if (c == '+')
			res[i] = '-';
		else if (c == '/')
			res[i] = '_';
		else if (skippadding && c == '=')
			res[i] = '\0';
		i++;
	}
	return res;
}

/**
 * @brief decodes a base64url encoded string
 *
 * Conforming to [RFC 4648 §5](https://datatracker.ietf.org/doc/html/rfc4648#section-5).
 *
 * This function will change str! This is because you will probably use this function with the result of a percent_decode() call.
 *
 * @note This function expects the padding to be using '=' characters.
 *
 * @param[in,out] str  the encoded input string to be decoded
 * 
 * @returns the decoded string
 * @retval NULL an error occurred and errno might be set
 *
 * The returned string must be freed using free().
 */
char * base64url_decode(char * str) {
	char c;
	size_t i = 0;

	if (str == NULL)
		return NULL;

	while ((c = str[i]) != '\0') {
		if (c == '-')
			str[i] = '+';
		else if (c == '_')
			str[i] = '/';
		i++;
	}

	return base64_decode(str);
}





/**
 * @brief Calculates the HMAC-SHA256 of a given message with a given key.
 *
 * @see https://en.wikipedia.org/wiki/HMAC
 *
 * @param[out] hash    buffer for the resulting hash; Must be at least SHA256_DIGEST_LENGTH bytes long.
 * @param[in]  message the message
 * @param[in]  key     the key
 *
 * @returns whether the function was successful.
 * @retval true success
 * @retval false an error occurred and errno might be set
 */
bool hmac_sha256(unsigned char * hash, const char * message, const char *key) {
	const size_t blocksize = 64; // 512bits = 64bytes * 8 // sha256 specific
	const size_t outputsize = SHA256_DIGEST_LENGTH; // 256bits = 32bytes * 8 // sha256 specific
	unsigned char * (*hash_func_ptr)(const unsigned char *d, size_t n, unsigned char *md) = SHA256;

	bool res = false;
	unsigned char fin_key[blocksize];
	unsigned char o_key_pad[blocksize];
	unsigned char i_key_pad[blocksize];
	size_t i = 0;
	size_t messagelen = 0;

	unsigned char * tmp = NULL;
	unsigned char tmp2[blocksize + outputsize];
	unsigned char tmp_hash[outputsize];


	if (hash == NULL || message == NULL || key == NULL)
		goto end;

	messagelen = strlen(message);

	// set everything to 0
	memset(fin_key, 0, blocksize);
	memset(o_key_pad, 0, blocksize);
	memset(i_key_pad, 0, blocksize);

	// generate fin_key
	if (strlen(key) > blocksize)
		hash_func_ptr((const unsigned char *)key, strlen(key), fin_key);
	else
		memcpy(fin_key,key,strlen(key));


	// generate padding
	for (i = 0; i < blocksize; i++) {
		o_key_pad[i] = fin_key[i] ^ 0x5c;
		i_key_pad[i] = fin_key[i] ^ 0x36;
	}


	if ((tmp = malloc(sizeof (char) * (blocksize + messagelen))) == NULL) {
		goto end;
	}
	memcpy(tmp, i_key_pad, blocksize);
	memcpy(tmp + blocksize, message, messagelen);
	// tmp_hash = hash(i_key_pad + message)
	hash_func_ptr(tmp, blocksize + messagelen, tmp_hash);

	// hash(o_key_pad + hash(i_key_pad + message))
	memcpy(tmp2, o_key_pad, blocksize);
	memcpy(tmp2 + blocksize, tmp_hash, outputsize);
	hash_func_ptr(tmp2, blocksize + outputsize, hash);

	res = true; // success
end:
	free(tmp);
	// set everything back to 0 for security reasons
	memset(fin_key, 0, outputsize);
	memset(o_key_pad, 0, blocksize);
	memset(i_key_pad, 0, blocksize);
	return res;
}


/**
 * @brief Removes all whitespace characters from a string.
 *
 * @param[in,out] s the string
 *
 */
void remove_whitespace(char * s) {
	const char* d = s;
	if (s == NULL || *s == '\0')
		return;
	do {
		while (*d != '\0' && isspace(*d)) {
			d++;
		}
		*s = *d;
		if (*s == '\0')
			return;
		s++;
		d++;
	} while (*s != '\0');
}



/**
 * @brief Writes a stream to the file located at outpath.
 *
 * The stream is read until EOF is reached.
 *
 * @param[in] stream the stream to be read from
 * @param[in] outpath path to the file were the output should be written to
 *
 * @return whether the function was successful
 * @retval true success
 * @retval false An error occurred and errno might be set
 *
 */
bool writestreamtofile(FILE * stream, const char * outpath) {
	bool res = false;
	FILE * out = NULL;
	int c = '\0';

	if (stream == NULL || outpath == NULL) {
		goto end;
	}

	if ((out = fopen(outpath, "w")) == NULL) {
		goto end;
	}

	// this is slow, I know
	while (!feof(stream)) {
		c = fgetc(stream);
		if (fputc(c, out) == EOF) {
			goto end;
		}
	}

	res = true;
end:
	fclose(out);
	return res;
}





/**
 * @brief Tries to retrieve the value of stringkey from json as a string.
 * 
 * Note: res will only contain the pointer to the string inside the json object and thus will be freed by calling cJSON_Delete on json.
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in] json the json object that is supposed to contain stringkey 
 * @param[in] curjsonkey the name of json (used for error messages)
 * @param[in] stringkey the name of the key
 * @param[out] res pointer to the pointer where the pointer of the retrieved value should be stored
 * @param[in] can_be_null whether the value is allowed to be null
 *
 * @returns whether the function was successful.
 * @retval true success. a pointer to the value has been stored in res
 * @retval false failed retrieving the string and an error message has been written to errmsg
 */
bool get_string_from_json(char * errmsg, const cJSON * json, const char * curjsonkey, const char * stringkey, char ** res , bool can_be_null) {
	bool success = false;
	cJSON * tmp = NULL;
	
	if (errmsg == NULL || curjsonkey == NULL || json == NULL || stringkey == NULL || res == NULL) {
		sprintf(errmsg, "%s: all arguments must be set", __func__);
		goto end;
	}

	*res = NULL;

	if ((tmp = cJSON_GetObjectItemCaseSensitive(json, stringkey)) == NULL) {
		sprintf(errmsg, "%s: Failed parsing json: %s.%s is not defined", __func__, curjsonkey, stringkey);
		goto end;
	}
	if ((!cJSON_IsString(tmp) || tmp->valuestring == NULL) && (!can_be_null && !cJSON_IsNull(tmp)) ) {
		sprintf(errmsg, "%s: Failed parsing json: %s.%s is not a string%s", __func__, curjsonkey, stringkey, (can_be_null ? " and is not null" : ""));
		goto end;
	}
	if (can_be_null && cJSON_IsNull(tmp)) {
		*res = NULL;
	} else {
		*res = tmp->valuestring;
	}

	success = true;
end:
	return success;
}


/**
 * @brief Tries to retrieve the value of numberkey from json as a long.
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in] json the json object that is supposed to contain numberkey 
 * @param[in] curjsonkey the name of json (used for error messages)
 * @param[in] numberkey the name of the key
 * @param[out] res where the retrieved value should be stored
 *
 * @returns whether the function was successful.
 * @retval true success. a pointer to the value has been stored in res
 * @retval false failed retrieving the number and an error message has been written to errmsg
 */
bool get_number_from_json(char * errmsg, const cJSON * json, const char * curjsonkey, const char * numberkey, long * res) {
	bool success = false;
	cJSON * tmp = NULL;
	
	if (errmsg == NULL || curjsonkey == NULL || json == NULL || numberkey == NULL || res == NULL) {
		sprintf(errmsg, "%s: all arguments must be set", __func__);
		goto end;
	}

	if ((tmp = cJSON_GetObjectItemCaseSensitive(json, numberkey)) == NULL) {
		sprintf(errmsg, "%s: Failed parsing json: %s.%s is not defined", __func__, curjsonkey, numberkey);
		goto end;
	}
	if (!cJSON_IsNumber(tmp)) {
		sprintf(errmsg, "%s: Failed parsing json: %s.%s is not a number", __func__, curjsonkey, numberkey);
		goto end;
	}
	*res = (long) tmp->valuedouble;

	success = true;
end:
	return success;
}

/**
 * @brief Tries to retrieve the value of boolkey from json as an int.
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in] json the json object that is supposed to contain boolkey 
 * @param[in] curjsonkey the name of json (used for error messages)
 * @param[in] boolkey the name of the key
 * @param[out] res where the retrieved value should be stored
 *
 * @returns whether the function was successful.
 * @retval true success. a pointer to the value has been stored in res
 * @retval false failed retrieving the bool and an error message has been written to errmsg
 */
bool get_bool_from_json(char * errmsg, const cJSON * json, const char * curjsonkey, const char * boolkey, bool * res) {
	bool success = false;
	cJSON * tmp = NULL;
	
	if (errmsg == NULL || curjsonkey == NULL || json == NULL || boolkey == NULL || res == NULL) {
		sprintf(errmsg, "%s: all arguments must be set", __func__);
		goto end;
	}

	if ((tmp = cJSON_GetObjectItemCaseSensitive(json, boolkey)) == NULL) {
		sprintf(errmsg, "%s: Failed parsing json: %s.%s is not defined", __func__, curjsonkey, boolkey);
		goto end;
	}
	if (!cJSON_IsBool(tmp)) {
		sprintf(errmsg, "%s: Failed parsing json: %s.%s is not a bool", __func__, curjsonkey, boolkey);
		goto end;
	}
	*res = cJSON_IsTrue(tmp);

	success = true;
end:
	return success;
}


/**
 * @brief Resizes the dest string and appends the src string to the dest string.
 *
 * Uses realloc() to resize dest to fit dest, src and the terminating null
 * byte, then it appends srclen characters of the src string to the dest string, overwriting the
 * terminating null byte at the end of dest, and then adds a terminating null
 * byte.
 *
 * @note dest and src must not be NULL.
 *
 * @param[in,out] dest the destination string
 * @param[in]     src  the source string
 * @param[in,out] destlen pointer where the current length of dest is stored. This function also updates this value. Excluding the terminating null byte. Ignored if NULL.
 * @param[in]     srclen the length of src. This function will use strlen(src), if srclen == 0.
 *
 * @returns the new location of dest.
 * @retval NULL an error occurred and errno might be set.
 *
 * @note dest is freed by this function if an error occurres.
 */
char * str_append(char * dest, const char * src, size_t * destlen, size_t srclen) {
	bool success = false;
	size_t olddestlen = 0;
	
	if (dest == NULL || src == NULL) {
		goto end;
	}

	olddestlen = (destlen != NULL) ? *destlen : strlen(dest);
	//fprintf(stderr, "%s: [DEBUG] dest: \"%s\" src: \"%s\" srclen: %ld\n", __func__, dest,src,srclen);
	if (srclen == 0) {
		srclen = strlen(src);
	}

	if ((dest = realloc(dest, sizeof(char) * (olddestlen + srclen + 1))) == NULL) {
		goto end;
	}

	strncat(&dest[olddestlen],src, srclen);

	if (destlen != NULL) {
		*destlen = olddestlen + srclen;
	}

	success = true;
end:
	if (!success) {
		free(dest);
		return NULL;
	}
	return dest;
}
