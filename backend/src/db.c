#include "db.h"
#include "common.h"
#include "cgi.h"
#include "kredis.h"
#include "config.h"
#include "user.h"

#include "../cJSON/cJSON.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <sys/time.h>

/**
 * @file
 * 
 * @brief Code for interacting with the database on a 'higher'-level.
 *
 * ## Database Layout
 *
 * |type|expire|key|value|comment|
 * |----|------|---|-----|-------|
 * |string|10min|jwtrefreshtoken:<jwtrefreshtoken>|`<userid>`|don't delete manually. let it expire|
 * |string|never|DB_PREFIX_USER<userid>|see the JSON  Syntax at db_user_store() | |
 * |set|never|DB_PREFIX_USERS|`<userids>`| |
 *
 * All keys have the extra prefix `sshca:`.
 */

/**
 * @brief Connects to a database.
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in]  dbconfig config for connecting to the database 
 *
 * @returns On success a database context is returned and the connection is established. Otherwise NULL is returned, errno might be set and errmsg contains an error message. 
 *
 * The returned pointer must be freed using db_close().
 */
db_t * db_connect(char * errmsg, const struct config_db_s * dbconfig) {
	bool success = false;
	db_t * res = NULL;
	char * iposocket = NULL;

	if (dbconfig == NULL) {
		sprintf(errmsg, "%s: all arguments must be set", __func__);
		goto end;
	}

	// Read dbsettings
	if (strcmp(dbconfig->type, "redis") != 0) { // check type
		sprintf(errmsg, "%s: unknown/unsupported database type", __func__);
		goto end;
	}

	if (dbconfig->use_unix_socket) {
		if (dbconfig->socket == NULL) {
			sprintf(errmsg, "%s: socket must be set", __func__);
			goto end;
		}
		iposocket = dbconfig->socket;
	} else {
		if (dbconfig->ip == NULL) {
			sprintf(errmsg, "%s: ip must be set", __func__);
			goto end;
		}
		iposocket = dbconfig->ip;
	}



	// alloc res
	res = malloc(sizeof(db_t));
	if (res == NULL) {
		sprintf(errmsg, "%s: failed allocating res", __func__);
		goto end;
	}
	res->isconnected = false;

	// connect to the database
	res->c = kredis_connect(iposocket,dbconfig->port,dbconfig->use_unix_socket,dbconfig->timeout,dbconfig->auth);
	if (res->c == NULL) {
		sprintf(errmsg, "%s: failed connecting to the database", __func__);
		goto end;
	}


	// successfully connected
	success = true;
	res->isconnected = true;
end:
	if (!success) {
		db_close(res);
		res = NULL;
	}
	return res;
}

/**
 * @brief Disconnects and frees a database context.
 *
 * @param[in,out] db the database context
 */
void db_close(db_t * db) {
	if (db == NULL)
		return;
	kredis_free(db->c);
	free(db);
}



/**
 * @brief Stores a user in the database.
 *
 * The data is stored as JSON and can later be retrieved using db_user_get().
 *
 * The JSON syntax is as follows:
 * ```json
 * {
 * 	"id":"<userid>",
 * 	"name":"<username>",
 * 	"oidc_external_subject_id":"<external subject id>",
 * 	"state":"<active|blocked>",
 * 	"principals":[
 * 		"<principal>",
 * 		"..."
 * 	]
 * }
 * ```
 *
 * @param[out]    errmsg        buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in,out] db            the database context
 * @param[in]     user          the user
 *
 * @returns whether the function was successful.
 * @retval true success
 * @retval false an error occurred and an error message has been written to errmsg
 */
bool db_user_store(char * errmsg, db_t * db, cJSON * user) {
	bool res = false;
	char * key = NULL;
	char * userid = NULL;
	char * valuestr = NULL;
	cJSON * tmp = NULL;

	if (errmsg == NULL || db == NULL || user == NULL) {
		sprintf(errmsg, "%s: all arguments must be set", __func__);
		goto end;
	}

	if ((tmp = cJSON_GetObjectItemCaseSensitive(user, "id")) == NULL) {
		sprintf(errmsg, "%s: id is not defined", __func__);
		goto end;
	}
	if (!cJSON_IsString(tmp) || tmp->valuestring == NULL) {
		sprintf(errmsg, "%s: id is not a string", __func__);
		goto end;
	}
	userid = tmp->valuestring;
	// build key
	if ((key = malloc(sizeof(char) * (strlen(DB_PREFIX_USER) + strlen(userid) + 1))) == NULL) {
		sprintf(errmsg, "%s: failed allocating key", __func__);
		goto end;
	}
	key[0] = '\0';
	strcat(key, DB_PREFIX_USER);
	strcat(key, userid);

	// render user to valuestr
	if ((valuestr = cJSON_PrintUnformatted(user)) == NULL) {
		sprintf(errmsg, "%s: failed rendering user", __func__);
		goto end;
	}

	// write to the database
	res = kredis_set(db->c, key, valuestr);
	if (!res || kredis_error(db->c)) {
		sprintf(errmsg, "%s: failed writing to the database: %s", __func__, kredis_error_string(db->c));
		goto end;
	}

	free(key); // key and valuestr will be reused later
	free(valuestr);
	key = NULL; // prevent double freeing
	valuestr = NULL;

	// === add user in "users" set
	
	// write to the database
	res = kredis_set_add(db->c, DB_PREFIX_USERS, userid);
	if (!res || kredis_error(db->c)) {
		sprintf(errmsg, "%s: failed writing to the database: %s", __func__, kredis_error_string(db->c));
		goto end;
	}
end:
	free(key);
	free(valuestr);
	return res;


}


/**
 * @brief Retrieves a user from the database via their userid.
 *
 * @see db_user_store()
 *
 * @see user_get()
 *
 * @param[out]    errmsg        buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in,out] db            the database context
 * @param[in]     userid        the users userid
 *
 * @returns the retrieved data as JSON
 * @retval NULL an error occurred or user doesn't exist in the database. An error message has been written to errmsg.
 *
 * The returned cJSON object must be freed using cJSON_Delete().
 */
cJSON * db_user_get(char * errmsg, db_t * db, const char * userid) {
	cJSON * res = NULL;
	char * key = NULL;
	char * valuestr = NULL;

	if (errmsg == NULL || db == NULL || userid == NULL) {
		sprintf(errmsg, "%s: all arguments must be set", __func__);
		goto end;
	}

	// build key
	if ((key = malloc(sizeof(char) * (strlen(DB_PREFIX_USER) + strlen(userid) + 1))) == NULL) {
		sprintf(errmsg, "%s: failed allocating key", __func__);
		goto end;
	}
	key[0] = '\0';
	strcat(key, DB_PREFIX_USER);
	strcat(key, userid);

	// get data from database
	valuestr = kredis_get(db->c, key);
	if (valuestr == NULL || kredis_error(db->c)) {
		sprintf(errmsg, "%s: failed reading from the database: %s", __func__, kredis_error_string(db->c));
		goto end;
	}

	// parse valuestr
	if ((res = cJSON_Parse(valuestr)) == NULL) {
		sprintf(errmsg, "%s: failed parsing valuestr", __func__);
		goto end;
	}

end:
	free(valuestr);
	free(key);
	return res;

}

/**
 * @brief Retrieves all users as an array from the database.
 *
 * @param[out]    errmsg         buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in,out] db             the database context
 *
 * @returns a cJSON Array containing all the user objects
 * @retval NULL an error occurred. An error message has been written to errmsg.
 *
 * The returned array must be freed using cJSON_Delete().
 */
cJSON * db_users_get_all(char * errmsg, db_t * db) {
	bool success = false;
	cJSON * res = NULL;
	cJSON * cur = NULL;
	size_t numusers = 0;
	char ** userids = NULL;
	size_t i = 0;

	if (errmsg == NULL || db == NULL) {
		sprintf(errmsg, "%s: all arguments must be set", __func__);
		goto end;
	}

	userids = kredis_set_get(db->c, DB_PREFIX_USERS, &numusers);
	if (userids == NULL || kredis_error(db->c)) {
		sprintf(errmsg, "%s: failed reading from database", __func__);
		goto end;
	}

	if ((res = cJSON_CreateArray()) == NULL) {
		sprintf(errmsg, "%s: failed creating array", __func__);
		goto end;
	}
	
	for (i = 0; i < numusers; i++) {
		if ((cur = db_user_get(errmsg, db, userids[i])) == NULL) {
			goto end;
		}
		if (!cJSON_AddItemToArray(res, cur)) {
			sprintf(errmsg, "%s: failed adding user to array", __func__);
			goto end;
		}
	}

	success = true;
end:
	kredis_set_free(userids, numusers);
	if (!success) {
		cJSON_Delete(res);
		return NULL;
	}
	return res;
}



/**
 * @brief Generates a new JWT refresh token and writes it into the database.
 *
 * JWT refresh tokens are used for authorizing JWT generation.
 *
 * A token is always associated with a user.
 * Thus when using a token you can determine for whom the JWT must be generated.
 *
 * A token can only ever be used once.
 *
 * A JWT refresh token itself is just a UUIDv4 string.
 *
 * The database entry will expire after ttl seconds.
 *
 * @see db_jwtrefreshtoken_refresh()
 *
 * @param[out]    errmsg        buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in,out] db            the database context
 * @param[out]    token         buffer for storing the JWT refresh token. size: DB_JWTREFRESHTOKEN_BUFSIZE
 * @param[in]     user          the user
 * @param[in]     ttl           how long the token should be valid in seconds
 *
 * @returns whether the function was successful.
 * @retval true success
 * @retval false an error occurred and an error message has been written to errmsg
 */
bool db_jwtrefreshtoken_create(char * errmsg, db_t * db, char * token, const user_t * user, long ttl) {
	bool res = false;
	char * key = NULL;

	if (errmsg == NULL || db == NULL || token == NULL || user == NULL) {
		sprintf(errmsg, "%s: all arguments must be set", __func__);
		goto end;
	}


	// generate token
	if (!uuid_v4_gen(token)) {
		sprintf(errmsg, "%s: failed generating token", __func__);
		goto end;
	}
	
	// build key
	if ((key = malloc(sizeof(char) * (strlen(DB_PREFIX_JWTREFRESHTOKEN) + strlen(token) + 1))) == NULL) {
		sprintf(errmsg, "%s: failed allocating key", __func__);
		goto end;
	}
	key[0] = '\0';
	sprintf(key, "%s%s", DB_PREFIX_JWTREFRESHTOKEN, token);

	// write to the database
	res = kredis_set_with_ex(db->c, key, user->id, ttl);
	if (!res || kredis_error(db->c)) {
		sprintf(errmsg, "%s: failed writing to the database: %s", __func__, kredis_error_string(db->c));
		goto end;
	}


end:
	free(key);
	return res;
}

/**
 * @brief Uses and regenerates a JWT refresh token and updates the database accordingly.
 *
 * This will invalidate oldtoken.
 * newtoken should be sent back to the user.
 *
 * @see db_jwtrefreshtoken_create()
 *
 * @param[out]    errmsg        buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in,out] db            the database context
 * @param[out]    oldtoken      buffer containing the old JWT refresh token about to be used. size: DB_JWTREFRESHTOKEN_BUFSIZE
 * @param[in]     newtoken      buffer for storing the new JWT refresh token. size: DB_JWTREFRESHTOKEN_BUFSIZE
 * @param[in]     ttl           how long the token should be valid in seconds
 *
 * @note oldtoken and newtoken may overlap.
 *
 * @returns the user associated with the token
 * @retval NULL an error occurred or oldtoken doesn't exist in the database. An error message has been written to errmsg.
 *
 * The returned user object must be freed using user_free().
 */
user_t * db_jwtrefreshtoken_refresh(char * errmsg, db_t * db, char * oldtoken, char * newtoken, long ttl) {
	bool success = false;
	user_t * user = NULL;
	char * userid = NULL;
	char * key = NULL;

	if (errmsg == NULL || db == NULL || oldtoken == NULL || newtoken == NULL) {
		sprintf(errmsg, "%s: all arguments must be set", __func__);
		goto end;
	}

	// build key
	if ((key = malloc(sizeof(char) * (strlen(DB_PREFIX_JWTREFRESHTOKEN) + strlen(oldtoken) + 1))) == NULL) {
		sprintf(errmsg, "%s: failed allocating key", __func__);
		goto end;
	}
	key[0] = '\0';
	sprintf(key, "%s%s", DB_PREFIX_JWTREFRESHTOKEN, oldtoken);

	// get userid via oldtoken from database
	userid = kredis_get(db->c, key);
	if (userid == NULL || kredis_error(db->c)) {
		sprintf(errmsg, "%s: failed reading from the database: %s", __func__, kredis_error_string(db->c));
		goto end;
	}

	// NOTE: potential race condition

	// delete oldtoken from database
	if (!kredis_del(db->c, key) || kredis_error(db->c)) {
		sprintf(errmsg, "%s: failed deleting from the database: %s", __func__, kredis_error_string(db->c));
		goto end;
	}

	// get user from database
	if ((user = user_get(errmsg, db, userid)) == NULL) {
		// errmsg already set
		goto end;
	}

	// create new token
	success = db_jwtrefreshtoken_create(errmsg, db, newtoken, user, ttl);
	// if failed -> errmsg already set
end:
	free(userid);
	free(key);
	if (!success) {
		user_free(user);
		return NULL;
	}
	return user;

}



