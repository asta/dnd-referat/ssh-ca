#include "kredis.h"
#include "../hiredis/hiredis.h"

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/time.h>


/**
 * @file
 *
 * @brief Simple ABI for hiredis
 *
 * @see https://github.com/redis/hiredis
 *
 * @note The redis server must be at least version 2.6.12.
 *
 */


/**
 * @internal
 *
 * A kredis context struct
 */
struct kredis_s {
	redisContext * rc; ///< the redisContext from hiredis
};

/**
 * @internal
 *
 * Duplicates a string. Exactly the same behaviour as the strdup() function defined in string.h.
 */
static char * kredis_strdup(const char * s) {
	size_t len;
	char * res;
	if (s == NULL)
		return NULL;

	len = strlen(s);
	res = malloc(sizeof(char) * (len + 1));
	if (res == NULL)
		return NULL;
	res[0] = '\0';
	strcat(res, s);
	return res;
}

/**
 * @brief Disconnects and frees a kredis_t object.
 *
 * @param[in,out] k the kredis context
 */
void kredis_free(kredis_t * k) {
	if (k == NULL)
		return;
	if (k->rc != NULL)
		redisFree(k->rc);

	free(k);
}

/**
 * @brief Connects to a redis server.
 * 
 * @param[in] ip             The IP Address of the server or the path to a unix socket. If NULL set by default to 127.0.0.1.
 * @param[in] port           The port of the redis server. Note: The default redis port is 6379.
 * @param[in] is_unix_socket Whether ip is a path to a unix socket. port is ignored if set.
 * @param[in] timeout        Sets the connection timeout.
 * @param[in] auth           The redis server password specified in the server config by requirepass. Ignored if equal to NULL.
 *
 * @returns On success a kredis context is returned. Otherwise NULL is returned and errno might be set.
 *
 * The returned kredis context must be freed using kredis_free().
 *
 * @note Only the old authentication method prior to Redis 6.0 is supported.
 */
kredis_t * kredis_connect(const char * ip, int port, bool is_unix_socket, struct timeval timeout,  const char * auth) {
	struct kredis_s * res = NULL;
	redisReply *reply = NULL;
	redisOptions options = {0};

	if (ip == NULL)
		ip = "127.0.0.1";
	
	if ((res = malloc(sizeof(struct kredis_s))) == NULL) {
		goto error_close;
	}

	if (is_unix_socket) {
		REDIS_OPTIONS_SET_UNIX(&options, ip);
	} else {
		REDIS_OPTIONS_SET_TCP(&options, ip, port);
	}

	options.connect_timeout = &timeout;

	res->rc = redisConnectWithOptions(&options);
	//res->rc = redisConnect(ip, port);

	if (res->rc == NULL || res->rc->err) {
		/*if (c) {
		printf("Error: %s\n", c->errstr);
		// handle error
		} else {
		printf("Can't allocate redis context\n");
		}*/
		goto error_close;
	}

	if (auth != NULL) {
		reply = redisCommand(res->rc, "AUTH %s", auth);
		//printf("DEBUG: AUTH reply: type: %d err: %d\n", reply->type, res->rc->err);
		if (reply == NULL || res->rc->err)
			goto error_close;
		switch (reply->type) {
			case REDIS_REPLY_STATUS:
				//printf("DEBUG: REDIS_REPLY_STATUS: str[%ld]: \"%s\"\n", reply->len, reply->str);
				break;
			/*case REDIS_REPLY_ERROR:
				//printf("DEBUG: REDIS_REPLY_ERROR: str[%ld]: \"%s\"\n", reply->len, reply->str);
				goto error_close;*/
			default:
				//printf("DEBUG: unknown/unhandled reply type: %d\n", reply->type);
				goto error_close;

		}
		freeReplyObject(reply);
		reply = NULL;
	}
	
	// test connection
	if (!kredis_ping(res))
		goto error_close;

	return res;

error_close:
	if (reply != NULL)
		freeReplyObject(reply);
	kredis_free(res);
	return NULL;
}

/**
 * @brief Ping the redis server.
 *
 * @param[in,out] k the kredis context
 *
 * @returns whether the ping was successful.
 * @retval false failed
 * @retval true success
 *
 * @note Check for errors using kredis_error() on failure.
 *
 */
bool kredis_ping(kredis_t * k) {
	redisReply *reply = NULL;
	if (k == NULL)
		return false;
	reply = redisCommand(k->rc, "PING");
	//printf("DEBUG: PING reply: type: %d err: %d\n", reply->type, k->rc->err);
	if (reply == NULL)
		return false;
	if (k->rc->err) {
		freeReplyObject(reply);
		return false;
	}
	if (reply->type == REDIS_REPLY_STATUS) {
		freeReplyObject(reply);
		return true;
	} else {
		freeReplyObject(reply);
		return false;
	}
}

/**
 * @brief Gets the current error code as an integer.
 *
 * In case an error actually happend, you can use kredis_error_string() to get the error message as a string.
 *
 * @param[in] k the kredis context
 *
 * @returns the error code as an integer
 * @retval 0 indicates no error
 *
 */
int kredis_error(kredis_t * k) {
	if (k == NULL || k->rc == NULL)
		return 1; // error
	return k->rc->err;
}

/**
 * @brief Gets the current error code as a string.
 *
 * This function cannot be used for checking whether an error actually occurred, use kredis_error() instead.
 *
 * An empty string is returned if no error happend.
 *
 * @param[in] k the kredis context
 *
 * @returns the error message 
 * @retval NULL invalid argument or invalid redisContext
 *
 * @note The returned string MUST NOT be freed.
 *
 */
const char * kredis_error_string(kredis_t * k) {
	if (k == NULL || k->rc == NULL)
		return NULL;
	if (k->rc->err == 0)
		return "";
	return k->rc->errstr;
}

/**
 * @brief Native Redis GET Command
 *
 * Get the value of key.
 *
 * The returned string must be freed using free().
 *
 * @see https://redis.io/commands/get
 *
 * @param[in,out] k the kredis context
 * @param[in]     key the key
 *
 * @returns the value of key
 * @retval NULL key doesn't exists or isn't a string
 *
 * @note Check for errors using kredis_error() after calling this function.
 */
char * kredis_get(kredis_t * k, const char * key) {
	if (key == NULL)
		return NULL;
	redisReply *reply = NULL;
	reply = redisCommand(k->rc, "GET "KREDIS_PREFIX"%s", key);
	//printf("DEBUG: PING reply: type: %d err: %d\n", reply->type, k->rc->err);
	if (reply == NULL)
		return NULL;
	if (k->rc->err) {
		freeReplyObject(reply);
		return NULL;
	}
	if (reply->type != REDIS_REPLY_STRING || reply->str == NULL) {
		// note: NIL might be returned
		//printf("DEBUG: kredis_get: unknown type: key: '"KREDIS_PREFIX"%s' type: %d\n", key, reply->type);
		freeReplyObject(reply);
		return NULL;
	}
	/*if (reply->type == REDIS_REPLY_NIL || reply->type == REDIS_REPLY_ERROR) {
		freeReplyObject(reply);
		return NULL;
	}*/
	char * res = kredis_strdup(reply->str); // no need for failure check here as the return value NULL might also indicate an error
	freeReplyObject(reply);
	return res;
}

/**
 * @brief Native Redis SET Command
 *
 * Set key to hold the string value.
 * If key already holds a value, it is overwritten, regardless of its type.
 * Any previous time to live associated with the key is discarded on successful SET operation.
 *
 * @see https://redis.io/commands/set
 *
 * @param[in,out] k the kredis context
 * @param[in]     key the key
 * @param[in]     value the value
 *
 * @returns whether the command was successful.
 * @retval false failed
 * @retval true success
 *
 * @note Check for errors using kredis_error() after calling this function.
 */
bool kredis_set(kredis_t * k, const char * key, const char * value) {
	if (value == NULL || key == NULL)
		return false;
	redisReply *reply = NULL;
	reply = redisCommand(k->rc, "SET "KREDIS_PREFIX"%s %s", key, value);
	//printf("DEBUG: PING reply: type: %d err: %d\n", reply->type, k->rc->err);
	if (reply == NULL) {
		//printf("DEBUG: SET: reply == NULL\n");
		return false;
	}
	if (k->rc->err) {
		//printf("DEBUG: SET: rc->err: %d\n", k->rc->err);
		freeReplyObject(reply);
		return false;
	}
	if (reply->type != REDIS_REPLY_STATUS) {
		//printf("DEBUG: kredis_set: unknown type: key: '"KREDIS_PREFIX"%s' type: %d\n", key, reply->type);
		freeReplyObject(reply);
		return false;
	}
	/*if (reply->type == REDIS_REPLY_NIL || reply->type == REDIS_REPLY_ERROR) {
		freeReplyObject(reply);
		return NULL;
	}*/
	freeReplyObject(reply);
	return true;

}

/**
 * @brief Native Redis SET Command with EX set
 *
 * Set key to hold the string value.
 * If key already holds a value, it is overwritten, regardless of its type.
 * Any previous time to live associated with the key is discarded on successful SET operation.
 *
 * @see https://redis.io/commands/set
 *
 * @param[in,out] k the kredis context
 * @param[in]     key the key
 * @param[in]     value the value
 * @param[in]     ex expire time, in seconds
 *
 * @returns whether the command was successful.
 * @retval false failed
 * @retval true success
 *
 * @note Check for errors using kredis_error() after calling this function.
 */
bool kredis_set_with_ex(kredis_t * k, const char * key, const char * value, long ex) {
	if (value == NULL || key == NULL)
		return false;
	redisReply *reply = NULL;

	char buf[32];
	buf[0] = '\0';
	sprintf(buf, "%ld", ex);

	reply = redisCommand(k->rc, "SET "KREDIS_PREFIX"%s %s EX %s", key, value, buf);
	//printf("DEBUG: PING reply: type: %d err: %d\n", reply->type, k->rc->err);
	if (reply == NULL) {
		//printf("DEBUG: SET: reply == NULL\n");
		return false;
	}
	if (k->rc->err) {
		//printf("DEBUG: SET: rc->err: %d\n", k->rc->err);
		freeReplyObject(reply);
		return false;
	}
	if (reply->type != REDIS_REPLY_STATUS) {
		//printf("DEBUG: kredis_set: unknown type: key: '"KREDIS_PREFIX"%s' type: %d\n", key, reply->type);
		freeReplyObject(reply);
		return false;
	}
	/*if (reply->type == REDIS_REPLY_NIL || reply->type == REDIS_REPLY_ERROR) {
		freeReplyObject(reply);
		return NULL;
	}*/
	freeReplyObject(reply);
	return true;

}

/**
 * @brief Native Redis DEL Command
 *
 * Removes the specified keys. A key is ignored if it does not exist.
 *
 * @see https://redis.io/commands/del
 *
 * @param[in,out] k the kredis context
 * @param[in]     key the key
 *
 * @returns whether the command was successful.
 * @retval false failed
 * @retval true success
 *
 * @note Check for errors using kredis_error() after calling this function.
 */
bool kredis_del(kredis_t * k, const char * key) {
	if (key == NULL)
		return false;
	redisReply *reply = NULL;
	reply = redisCommand(k->rc, "DEL "KREDIS_PREFIX"%s", key);
	if (reply == NULL)
		return false;
	if (k->rc->err) {
		freeReplyObject(reply);
		return false;
	}
	if (reply->type != REDIS_REPLY_INTEGER) {
		//printf("DEBUG: kredis_del: unknown type: key: '"KREDIS_PREFIX"%s' type: %d\n", key, reply->type);
		freeReplyObject(reply);
		return false;
	}
	freeReplyObject(reply);
	return true;
}



/**
 * @brief Native Redis SADD Command
 * 
 * Add the specified member to the set stored at key.
 * Specified members that are already a member of this set are ignored.
 * If key does not exist, a new set is created before adding the specified member.
 *
 * An error occures when the value stored at key is not a set.
 *
 * @see https://redis.io/commands/sadd
 *
 * @param[in,out] k the kredis context
 * @param[in]     key the key
 * @param[in]     member the member to be added
 *
 * @returns whether the command was successful.
 * @retval false failed
 * @retval true success
 *
 * @note Check for errors using kredis_error() after calling this function.
 */
bool kredis_set_add(kredis_t * k, const char * key, const char * member) {
	if (key == NULL || member == NULL)
		return false;
	redisReply *reply = NULL;
	reply = redisCommand(k->rc, "SADD "KREDIS_PREFIX"%s %s", key, member);
	if (reply == NULL) {
		return false;
	}
	if (k->rc->err) {
		freeReplyObject(reply);
		return false;
	}
	if (reply->type != REDIS_REPLY_INTEGER) {
		//printf("DEBUG: kredis_set_add: unknown type: key: '"KREDIS_PREFIX"%s' type: %d\n", key, reply->type);
		freeReplyObject(reply);
		return false;
	}
	freeReplyObject(reply);
	return true;
}


/**
 * @brief Native Redis SCARD Command
 * 
 * Returns the set cardinality (number of elements) of the set stored at key.
 *
 * @see https://redis.io/commands/scard
 *
 * @param[in,out] k the kredis context
 * @param[in]     key the key
 *
 * @returns the set cardinality
 * @retval 0 failed or there are no elements in the set
 *
 * @note Check for errors using kredis_error() after calling this function.
 */
long long kredis_set_num(kredis_t * k, const char * key) {
	if (key == NULL)
		return 0;
	redisReply *reply = NULL;
	reply = redisCommand(k->rc, "SCARD "KREDIS_PREFIX"%s", key);
	if (reply == NULL) {
		return 0;
	}
	if (k->rc->err) {
		freeReplyObject(reply);
		return 0;
	}
	if (reply->type != REDIS_REPLY_INTEGER) {
		//printf("DEBUG: kredis_set_num: unknown type: key: '"KREDIS_PREFIX"%s' type: %d\n", key, reply->type);
		freeReplyObject(reply);
		return 0;
	}
	long long res = reply->integer;
	freeReplyObject(reply);
	return res;

}

/**
 * @brief Native Redis SISMEMBER Command
 * 
 * Returns if member is a member of the set stored at key.
 *
 * @see https://redis.io/commands/sismember
 *
 * @param[in,out] k the kredis context
 * @param[in]     key the key
 * @param[in]     member the member
 *
 * @returns whether member is a member of the set
 * @retval true the element is a member of the set
 * @retval false the element is not a member of the set, key does not exist, or other failure
 *
 * @note Check for errors using kredis_error() after calling this function.
 */
bool kredis_set_ismember(kredis_t * k, const char * key, const char * member) {
	if (key == NULL || member == NULL)
		return false;
	redisReply *reply = NULL;
	reply = redisCommand(k->rc, "SISMEMBER "KREDIS_PREFIX"%s %s", key, member);
	if (reply == NULL) {
		return false;
	}
	if (k->rc->err) {
		freeReplyObject(reply);
		return false;
	}
	if (reply->type != REDIS_REPLY_INTEGER) {
		//printf("DEBUG: kredis_set_ismember: unknown type: key: '"KREDIS_PREFIX"%s' type: %d\n", key, reply->type);
		freeReplyObject(reply);
		return false;
	}
	bool res = (bool)reply->integer;
	freeReplyObject(reply);
	return res;
}

/**
 * @brief Native Redis SREM Command
 * 
 * Remove the specified member from the set stored at key.
 * Specified members that are not a member of this set are ignored.
 * If key does not exist, it is treated as an empty set.
 *
 * An error occures when the value stored at key is not a set.
 *
 * @see https://redis.io/commands/srem
 *
 * @param[in,out] k the kredis context
 * @param[in]     key the key
 * @param[in]     member the member to be removed
 *
 * @returns whether the command was successful.
 * @retval false failed
 * @retval true success
 *
 * @note Check for errors using kredis_error() after calling this function.
 */
bool kredis_set_rem(kredis_t * k, const char * key, const char * member) {
	if (key == NULL || member == NULL)
		return false;
	redisReply *reply = NULL;
	reply = redisCommand(k->rc, "SREM "KREDIS_PREFIX"%s %s", key, member);
	if (reply == NULL) {
		return false;
	}
	if (k->rc->err) {
		freeReplyObject(reply);
		return false;
	}
	if (reply->type != REDIS_REPLY_INTEGER) {
		//printf("DEBUG: kredis_set_rem: unknown type: key: '"KREDIS_PREFIX"%s' type: %d\n", key, reply->type);
		freeReplyObject(reply);
		return false;
	}
	freeReplyObject(reply);
	return true;
}

/**
 * @brief Native Redis SMEMBERS Command
 * 
 * Returns all the members of the set value stored at key.
 *
 * The returned array must be freed using kredis_set_free().
 *
 * @see https://redis.io/commands/smembers
 *
 * @param[in,out] k the kredis context
 * @param[in]     key the key
 * @param[out]    nummembers the number of elements that were read and thus the size of the returned array
 *
 * @returns an array of strings
 * @retval NULL something failed, errno might be set
 *
 * @note Check for errors using kredis_error() after calling this function.
 */
char** kredis_set_get(kredis_t * k, const char * key, size_t * nummembers) {
	if (key == NULL || nummembers == NULL)
		return NULL;
	*nummembers = 0;
	redisReply *reply = NULL;
	reply = redisCommand(k->rc, "SMEMBERS "KREDIS_PREFIX"%s", key);
	if (reply == NULL) {
		return NULL;
	}
	if (k->rc->err) {
		freeReplyObject(reply);
		return NULL;
	}
	if (reply->type != REDIS_REPLY_ARRAY) {
		//printf("DEBUG: kredis_set_get: unknown type: key: '"KREDIS_PREFIX"%s' type: %d\n", key, reply->type);
		freeReplyObject(reply);
		return NULL;
	}

	if (reply->elements == 0) {
		//printf("DEBUG: kredis_set_get: read 0 elements\n");
		freeReplyObject(reply);
		return NULL;
	}


	char ** res = malloc(sizeof(char *) * reply->elements);
	if (res == NULL) {
		freeReplyObject(reply);
		return NULL;
	}

	size_t i;
	for (i = 0; i < reply->elements; i++) {
		res[i] = NULL;
	}

	redisReply *cur;
	for (i = 0; i < reply->elements; i++) {
		cur = reply->element[i];
		if (cur == NULL || cur->type != REDIS_REPLY_STRING || cur->str == NULL) {
			//printf("DEBUG: kredis_set_get: unknown element type: key: '"KREDIS_PREFIX"%s' type: %d\n", key, cur->type);
			kredis_set_free(res, reply->elements);
			freeReplyObject(reply);
			return NULL;
		}
		if ((res[i] = kredis_strdup(cur->str)) == NULL) {
			kredis_set_free(res, reply->elements);
			freeReplyObject(reply);
			return NULL;
		}
	}

	*nummembers = reply->elements;

	freeReplyObject(reply);
	return res;
}

/**
 * @brief Frees the returned array from kredis_set_get()
 *
 * @param[in,out] elements the array
 * @param[in]     nummembers the size of the array 
 */
void kredis_set_free(char ** elements, size_t nummembers) {
	if (elements == NULL)
		return;
	size_t i;
	for (i = 0; i < nummembers; i++) {
		free(elements[i]);
	}
	free(elements);
}


/**
 * @brief Native Redis HSET Command
 * 
 * Sets field in the hash stored at key to value.
 * If key does not exist, a new key holding a hash is created.
 * If field already exists in the hash, it is overwritten.
 *
 * @see https://redis.io/commands/hset
 *
 * @param[in,out] k the kredis context
 * @param[in]     key the key
 * @param[in]     field the field
 * @param[in]     value the value
 *
 * @returns whether the command was successful.
 * @retval false failed
 * @retval true success
 *
 * @note Check for errors using kredis_error() after calling this function.
 */
bool kredis_hash_set(kredis_t * k, const char * key, const char * field, const char * value) {
	if (key == NULL || field == NULL || value == NULL)
		return false;
	redisReply *reply = NULL;
	reply = redisCommand(k->rc, "HSET "KREDIS_PREFIX"%s %s %s", key, field, value);
	if (reply == NULL) {
		return false;
	}
	if (k->rc->err) {
		freeReplyObject(reply);
		return false;
	}
	if (reply->type != REDIS_REPLY_INTEGER) {
		//printf("DEBUG: kredis_hash_set: unknown type: key: '"KREDIS_PREFIX"%s' type: %d\n", key, reply->type);
		freeReplyObject(reply);
		return false;
	}
	freeReplyObject(reply);
	return true;
}

/**
 * @brief Native Redis HEXISTS Command
 * 
 * Returns if field is an existing field in the hash stored at key.
 *
 * @see https://redis.io/commands/hexists
 *
 * @param[in,out] k the kredis context
 * @param[in]     key the key
 * @param[in]     field the field
 *
 * @returns whether the field exists in the hash
 * @retval true the hash contains field
 * @retval false the hash does not contain field, key does not exist, or other failure
 *
 * @note Check for errors using kredis_error() after calling this function.
 */
bool kredis_hash_exists(kredis_t * k, const char * key, const char * field) {
	if (key == NULL || field == NULL)
		return false;
	redisReply *reply = NULL;
	reply = redisCommand(k->rc, "HEXISTS "KREDIS_PREFIX"%s %s", key, field);
	if (reply == NULL) {
		return false;
	}
	if (k->rc->err) {
		freeReplyObject(reply);
		return false;
	}
	if (reply->type != REDIS_REPLY_INTEGER) {
		//printf("DEBUG: kredis_hash_exists: unknown type: key: '"KREDIS_PREFIX"%s' type: %d\n", key, reply->type);
		freeReplyObject(reply);
		return false;
	}
	bool res = (bool)reply->integer;
	freeReplyObject(reply);
	return res;
}

/**
 * @brief Native Redis HGET Command
 * 
 * Returns the value associated with field in the hash stored at key.
 *
 * The returned string must be freed using free().
 *
 * @see https://redis.io/commands/hget
 *
 * @param[in,out] k the kredis context
 * @param[in]     key the key
 * @param[in]     field the field
 *
 * @returns the value
 * @retval NULL something failed, errno might be set
 *
 * @note Check for errors using kredis_error() after calling this function.
 */
char * kredis_hash_get(kredis_t * k, const char * key, const char * field) {
	if (key == NULL || field == NULL)
		return NULL;
	redisReply *reply = NULL;
	reply = redisCommand(k->rc, "HGET "KREDIS_PREFIX"%s %s", key, field);
	if (reply == NULL) {
		return NULL;
	}
	if (k->rc->err) {
		freeReplyObject(reply);
		return NULL;
	}
	if (reply->type != REDIS_REPLY_STRING || reply->str == NULL) {
		//printf("DEBUG: kredis_hash_get: unknown type: key: '"KREDIS_PREFIX"%s' type: %d\n", key, reply->type); 
		freeReplyObject(reply);
		return NULL;
	}
	char * res = kredis_strdup(reply->str); // no need for failure check here as the return value NULL might also indicate an error
	freeReplyObject(reply);
	return res;
}

