#ifndef CONFIG_H_
#define CONFIG_H_

#include "../cJSON/cJSON.h"

#include <sys/time.h>
#include <stdbool.h>

/**
 * @file
 * @brief Header file for config.c
 */


/**
 * @brief native representation of config.db
 */
struct config_db_s {
	char * type; ///< database type (currently only redis supported)
	char * ip; ///< server ip
	long port; ///< server port
	char * socket; ///< server unix socket 
	bool use_unix_socket; ///< whether to user the unix socket or ip+port
	struct timeval timeout; ///< redis timeout
	char * auth; ///< redis auth string, may be NULL if no authentication
};


/**
 * @brief native representation of config.auth
 */
struct config_auth_s {
	char * type; ///< authentcation type (currently only oidc (OpenID Connect) is supported)
	char * oauth_authorize_url; ///< the oauth authorize endpoint of the identity provider
	char * app_id; ///< oauth app id
	char * redirect_uri; ///< oauth redirect url
	char * scope; ///< oauth scope
	char * app_secret; ///< oauth scope
	char * oauth_token_url; ///< the oauth/oidc token endpoint of the identity provider
	char * oidc_expected_issuer; ///< the expected issuer (iss) claim in id_token
};

/**
 * @brief native representation of config.jwt
 */
struct config_jwt_s {
	char * secret; ///< JWT secret
	long ttl; ///< seconds until a JWT expires
	long leeway; ///< leeway to be added to ttl
	long ttl_refreshtoken; ///<  seconds until a refreshtoken expires
};


/**
 * @brief native representation of config["ssh-sign"]
 */
struct config_certify_s {
	char * client_ca_private_key; ///< path on the server to the private client-ca key
	char * validity; ///< how long a certified key should be valid for, see TIME FORMATS in sshd_config(5)
};


/**
 * @brief config struct
 */
struct config_s {
	cJSON * __raw; ///< the raw json data (internal use only!)
	struct config_db_s db; ///< config.db
	struct config_auth_s auth; ///< config.auth
	struct config_jwt_s jwt; ///< config.jwt
	struct config_certify_s certify; ///< config["ssh-sign"]
};


struct config_s * config_parse(char * errmsg, const char * filepath);

void config_free(struct config_s * config);

#endif
