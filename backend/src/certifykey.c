#include "common.h"
#include "cgi.h"
#include "db.h"
#include "jwt.h"
#include "oauth.h"
#include "config.h"

#include "../cJSON/cJSON.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

/**
 *
 * @file
 *
 * @brief key signing utility. handles endpoint at /cgi/certifykey
 *
 * Request:
 * Expects a POST Request with Content-Type set to `text/plain`.
 * The Request Header must contain a valid Authorization Bearer Token (JWT).
 * The User must be 'active'.
 * The Body must contain the PublicKey the User wants to get signed.
 *
 * Response (if successful):
 * Content-Type: `text/plain`
 * The Body contains the signed PublicKey of the User.
 */

/**
 * @brief Generates a comma-seperated list of principals from the user.
 *
 * TODO: user should be in user_t format instead of cJSON.
 *
 * @param[in] user the user
 *
 * @returns a comma-seperated list of principals 
 * @retval NULL an error occurred and errno might be set
 */
static char * getprincipalsasstring(cJSON * user) {
	bool success = false;
	char * res = NULL;
	size_t size = 16;
	cJSON * principals = NULL;
	cJSON * cur = NULL;
	bool first = true;

	if ((principals = cJSON_GetObjectItemCaseSensitive(user, "principals")) == NULL) {
		goto end;
	}
	if (!cJSON_IsArray(principals)) {
		goto end;
	}

	if ((res = malloc(sizeof(char) * size)) == NULL) {
		goto end;
	}

	res[0] = '\0';

	cJSON_ArrayForEach(cur, principals) {
	
		if (!cJSON_IsString(cur) || cur->valuestring == NULL) {
			goto end;
		}

		size += strlen(cur->valuestring) + 1;
		if ((res = realloc(res, sizeof(char) * size)) == NULL) {
			goto end;
		}
		if (first) {
			first = false;
		} else {
			strcat(res, ",");
		}
		strcat(res, cur->valuestring);
	};


	success = true;
end:
	if (!success) {
		free(res);
		return NULL;
	}
	return res;
}

/*
 * @brief Signs a key.
 *
 * TODO: further documentation of function
 *
 * TODO: user should be in user_t format instead of cJSON.
 */
static bool signkey(char * errmsg, const struct config_certify_s * signconfig, cJSON * user, const char * pubkeypath) {
	bool res = false;
	char * capath = NULL;
	char * validity = NULL;
	char * userid = NULL;
	char * principals = NULL;
	cJSON * tmp = NULL;

	char * cmd = NULL;

	if (signconfig == NULL || user == NULL || pubkeypath == NULL) {
		sprintf(errmsg, "%s: all arguments must be set", __func__);
		goto end;
	}


	capath = signconfig->client_ca_private_key;

	validity = signconfig->validity;

	// get userid
	if ((tmp = cJSON_GetObjectItemCaseSensitive(user, "id")) == NULL) {
		sprintf(errmsg, "%s: id is not defined in user", __func__);
		goto end;
	}
	if (!cJSON_IsString(tmp) || tmp->valuestring == NULL) {
		sprintf(errmsg, "%s: id is not a string in user", __func__);
		goto end;
	}
	userid = tmp->valuestring;

	if ((principals = getprincipalsasstring(user)) == NULL) {
		sprintf(errmsg, "%s: failed generating principals string", __func__);
		goto end;
	}

	if ((cmd = malloc(sizeof(char) * (128 + strlen(userid) + strlen(principals) + strlen(validity) + strlen(pubkeypath) + strlen(capath)))) == NULL) {
		sprintf(errmsg, "%s: failed allocating cmd", __func__);
		goto end;
	
	}

	// build command
	sprintf(cmd, "ssh-keygen -s \"%s\" -I \"%s\" -n \"%s\" -V \"%s\" \"%s\"", capath, userid, principals, validity, pubkeypath);
	//ssh-keygen -s ./ca -I testing-my-ca -n dmuth,splunk -V +1w -z 1 ./my-key.pub

	fprintf(stderr, "[DEBUG] %s: cmd: \"%s\"\n", __func__, cmd);

	// check if shell is available
	if (!system(NULL)) {
		sprintf(errmsg, "%s: no shell found", __func__);
		goto end;
	}

	if (system(cmd) != 0) {
		sprintf(errmsg, "%s: signing failed", __func__);
		goto end;
	}

	res = true;
end:
	free(principals);
	free(cmd);
	return res;

}

/**
 * @brief main function for certifykey
 *
 * @returns the exit code of this program.
 * @retval 0 success
 * @retval !0 failure
 */
int main(void) {
	char errmsg[ERRMSGMAXSIZE];
	int statuscode = 200;
	struct config_s * config = NULL;
	db_t * db = NULL;

	char * authstr = NULL;


	char * userid = NULL;
	cJSON * user = NULL;
	cJSON * tmp = NULL;


	char * jwtstr = NULL;

	char tmpfilepath[64 + UUIDLEN + 1];
	char * tmpfilepath2 = NULL;

	char * newpubkey = NULL;

	errmsg[0] = '\0';
	tmpfilepath[0] = '\0';
	
		
	if ((statuscode = cgi_testenv(errmsg, 'p', "text/plain")) != 200) {
		cgi_printerror(errmsg, statuscode);
		goto end;
	}


	// read config
	if ((config = config_parse(errmsg, CONFIGFILEPATH)) == NULL) {
		statuscode = 500;
		cgi_printerror(errmsg, statuscode);
		goto end;
	}

	// connect to database
	if ((db = db_connect(errmsg, &config->db)) == NULL) {
		statuscode = 500;
		cgi_printerror(errmsg, statuscode);
		goto end;
		
	}


	// read auth
	if ((authstr = getenv("HTTP_AUTHORIZATION")) == NULL) {
		statuscode = 400;
		sprintf(errmsg, "%s: Authorization missing", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}
	if (!isprefix("Bearer ", authstr)) {
		statuscode = 400;
		sprintf(errmsg, "%s: Authorization method must be Bearer", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}
	jwtstr = strchr(authstr, ' ') + 1; // "Bearer " contains a space

	// verify + decode jwtstr
	if ((userid = jwt_simple_decode(errmsg, jwtstr, config->jwt.secret)) == NULL) {
		statuscode = 403;
		cgi_printerror(errmsg, statuscode);
		goto end;
	}

	// get user
	if ((user = db_user_get(errmsg, db, userid)) == NULL) {
		statuscode = 500;
		cgi_printerror(errmsg, statuscode);
		goto end;
	}
	
	// verify user
	if ((tmp = cJSON_GetObjectItemCaseSensitive(user, "state")) == NULL) {
		statuscode = 500;
		sprintf(errmsg, "%s: state is not defined in user", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}
	if (!cJSON_IsString(tmp) || tmp->valuestring == NULL) {
		statuscode = 500;
		sprintf(errmsg, "%s: state in user is not a string", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}
	if (strcmp(tmp->valuestring, "active") != 0) {
		statuscode = 403;
		sprintf(errmsg, "%s: user isn't activated", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}
	// ==== user is allowed to get their certificate signed

	// generate tmpfilepath
	strcpy(tmpfilepath, "/tmp/sshca-");
	if (!uuid_v4_gen(strchr(tmpfilepath, '\0'))) {
		statuscode = 500;
		sprintf(errmsg, "%s: failed generating tmpfilepath", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	
	}
	strcat(tmpfilepath, ".pub");

	// write users public key to tmpfilepath
	if (!writestreamtofile(stdin, tmpfilepath)) {
		statuscode = 500;
		sprintf(errmsg, "%s: failed writing to tmpfilepath", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}


	// sign the key. WOHOO!!!
	if (!signkey(errmsg, &config->certify, user, tmpfilepath)) {
		statuscode = 500;
		cgi_printerror(errmsg, statuscode);
		goto end;
	}

	// remove old file
	if (remove(tmpfilepath) != 0) {
		statuscode = 500;
		sprintf(errmsg, "%s: failed deleting the old file", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}

	
	if ((tmpfilepath2 = strstr(tmpfilepath, ".pub")) == NULL) {
		statuscode = 500;
		sprintf(errmsg, "%s: tmpfilepath doesn't contain .pub", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}
	strcpy(tmpfilepath2, "-cert.pub");
	
	if ((newpubkey = filetostr(tmpfilepath)) == NULL) {
		statuscode = 500;
		sprintf(errmsg, "%s: failed reading certified pubkey back", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}

	// remove new file
	if (remove(tmpfilepath) != 0) {
		statuscode = 500;
		sprintf(errmsg, "%s: failed deleting the old file", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}

	printf("Content-Type: text/plain\r\n");
	printf("Status: %d\r\n", statuscode);

	/* --------------------------------------------------------------- */
	printf("\r\n");
	/* --------------------------------------------------------------- */
	printf("%s", newpubkey);


	/*
printf("<h2>env:</h2>\r\n");
printf("<pre>\r\n");
int i;
for (i = 0; envp[i] != NULL; i++)
	printf("\n%s", envp[i]);
printf("</pre>\r\n");
printf("<h2>stdin:</h2>\r\n");
printf("<pre>\r\n");
while ((i = getchar()) != EOF) {
	printf("%c",i);
}

printf("</pre>\r\n");
*/


end:
	free(newpubkey);
	free(userid);
	cJSON_Delete(user);
	fflush(stdout);
	db_close(db);
	config_free(config);
	return 0;
}
