#ifndef DB_H_
#define DB_H_

typedef struct db_s db_t;

#include "kredis.h"
#include "config.h"
#include "user.h"

#include "../cJSON/cJSON.h"

#include <stdbool.h>

/**
 * @file
 * @brief Header file for db.c
 */

/**
 * @brief database prefix for user
 */
#define DB_PREFIX_USER "user:"
/**
 * @brief database prefix for users
 *
 * TODO remove ':' (requires updating the database accordingly)
 */
#define DB_PREFIX_USERS "users:"
/**
 * @brief database prefix for jwtrefreshtoken
 */
#define DB_PREFIX_JWTREFRESHTOKEN "jwtrefreshtoken:"

/**
 * @brief the required buffer size for a jwtrefreshtoken
 */
#define DB_JWTREFRESHTOKEN_BUFSIZE (UUIDLEN + 1)

/**
 * @brief database struct
 */
struct db_s {
	bool isconnected; ///< whether the kredis context is connected to a database
	kredis_t *c; ///< the kredis context
};

db_t * db_connect(char * errmsg, const struct config_db_s * dbconfig);
void db_close(db_t * db);

bool db_user_store(char * errmsg, db_t * db, cJSON * user);
cJSON * db_user_get(char * errmsg, db_t * db, const char * userid);

cJSON * db_users_get_all(char * errmsg, db_t * db);

bool     db_jwtrefreshtoken_create(char * errmsg, db_t * db, char * token, const user_t * user, long ttl);
user_t * db_jwtrefreshtoken_refresh(char * errmsg, db_t * db, char * oldtoken, char * newtoken, long ttl);

#endif
