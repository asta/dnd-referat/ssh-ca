#include "common.h"
#include "cgi.h"
#include "db.h"
#include "jwt.h"
#include "oauth.h"
#include "config.h"
#include "user.h"
#include "template_engine.h"

#include "../cJSON/cJSON.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>


/**
 *
 * @file
 *
 * @brief utility for refreshing the jwtrefreshtoken and getting a JWT. handles endpoint at /cgi/refreshlogin
 *
 * Request:
 * Expects a POST Request with Content-Type set to `application/x-www-form-urlencoded`.
 * The Request Header must contain a valid jwtrefreshtoken cookie.
 * The User must be 'active'.
 * The Body should be empty.
 * The QUERY_STRING may contain a `as` field specifying the Content-Type of the response. (Supported are `json`,`html` and `plain`. Default: `json`)
 *
 *
 * Response as json (if successful):
 * Content-Type: `application/json`
 * Set-Cookie: `jwtrefreshtoken`
 * The Body should contain a JSON Object with a `jwt` string field and an `expires_in` number field.
 *
 * Response as html (if successful):
 * Content-Type: `text/html`
 * Set-Cookie: `jwtrefreshtoken`
 * The Body should contain the rendered version of refreshlogin.html.
 *
 * Response as plain (if successful):
 * Content-Type: `text/plain`
 * Set-Cookie: `jwtrefreshtoken`
 * The Body should contain the JWT.
 *
 */

/**
 * @brief main function for refreshlogin
 *
 * @returns the exit code of this program.
 * @retval 0 success
 * @retval !0 failure
 */
int main(void) {
	char errmsg[ERRMSGMAXSIZE];
	int statuscode = 200;
	struct config_s * config = NULL;
	db_t * db = NULL;
	char * qrystr = NULL;
	struct cgi_kvp_s * qry = NULL;
	char * cokstr = NULL;
	struct cgi_kvp_s * cok = NULL;

	const char * qry_as = NULL;

	user_t * user = NULL;

	char * jwtrefreshtoken = NULL;
	char newjwtrefreshtoken[DB_JWTREFRESHTOKEN_BUFSIZE];

	char * jwtstr = NULL;

	char * body = NULL;


	errmsg[0] = '\0';
	newjwtrefreshtoken[0] = '\0';
		
	if ((statuscode = cgi_testenv(errmsg, 'p', "application/x-www-form-urlencoded")) != 200) {
		cgi_printerror(errmsg, statuscode);
		goto end;
	}

	// read HTTP_COOKIE string
	if ((cokstr = getenv("HTTP_COOKIE")) == NULL || strlen(cokstr) <= 0 || (cok = cgi_parse_http_cookie(cokstr)) == NULL) {
		statuscode = 400;
		sprintf(errmsg, "%s: Failed parsing cookies.", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}

	// read cookie: 'jwtrefreshtoken' 
	if ((jwtrefreshtoken = cgi_kvp_get(cok, "jwtrefreshtoken")) == NULL) {
		statuscode = 400;
		sprintf(errmsg, "%s: Missing 'jwtrefreshtoken' cookie.", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}
	// verify jwtrefreshtoken encoding 
	if (!is_valid_uuid_v4(jwtrefreshtoken)) {
		statuscode = 400;
		sprintf(errmsg, "%s: jwtrefreshtoken is not encoded correctly.", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}
	// jwtrefreshtoken is harmless from here on out



	// read QUERY_STRING
	if ((qrystr = getenv("QUERY_STRING")) != NULL && strlen(qrystr) > 0 && (qry = cgi_parse_query_string(qrystr)) == NULL) {
		statuscode = 400;
		sprintf(errmsg, "%s: Failed parsing QUERY_STRING.", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}

	// read config
	if ((config = config_parse(errmsg,CONFIGFILEPATH)) == NULL) {
		statuscode = 500;
		cgi_printerror(errmsg, statuscode);
		goto end;
	}

	// connect to database
	if ((db = db_connect(errmsg, &config->db)) == NULL) {
		statuscode = 500;
		cgi_printerror(errmsg, statuscode);
		goto end;
		
	}


	// get user
	if ((user = db_jwtrefreshtoken_refresh(errmsg, db, jwtrefreshtoken, newjwtrefreshtoken,config->jwt.ttl_refreshtoken )) == NULL) {
		// token is invalid
		statuscode = 403;
		cgi_printerror(errmsg, statuscode);
		goto end;
	}
	
	// verify user
	if (strcmp(user->state, "active") != 0) {
		statuscode = 403;
		sprintf(errmsg, "%s: user isn't activated", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	}

	// generate jwt
	if ((jwtstr = jwt_simple_encode(errmsg, user, (unsigned long)config->jwt.ttl, (unsigned long)config->jwt.leeway, config->jwt.secret)) == NULL) {
		statuscode = 500;
		cgi_printerror(errmsg, statuscode);
		goto end;

	}



	// read query: 'as' 
	if ((qry_as = cgi_kvp_get(qry, "as")) == NULL)
		qry_as = "json"; // default


	if (strcmp(qry_as, "json") == 0) {
		printf("Content-Type: application/json\r\n");
		printf("Status: %d\r\n", statuscode);
		printf("Set-Cookie: jwtrefreshtoken=%s; Secure; HttpOnly; Max-Age=%ld; SameSite=Strict\r\n", newjwtrefreshtoken, config->jwt.ttl_refreshtoken);

		/* --------------------------------------------------------------- */
		printf("\r\n");
		/* --------------------------------------------------------------- */

		printf("{\r\n");
		printf("\"jwt\":\"%s\"\r\n", jwtstr);
		printf(",\"expires_in\":\"%ld\"\r\n", (unsigned long)config->jwt.ttl);
		printf("}\r\n");
	} else if (strcmp(qry_as, "html") == 0) {
		const struct tmplt_kvp_s data[] = {
			{"JWT",jwtstr,true},
			{NULL,NULL,false} // TERMINATE ARRAY
		};
		if ((body = tmplt_render_from_file(errmsg,TEMPLATES_DIR"/refreshlogin.html", data)) == NULL) {
			statuscode = 500;
			cgi_printerror(errmsg, statuscode);
			goto end;
		}
		printf("Content-Type: text/html\r\n");
		printf("Status: %d\r\n", statuscode);
		printf("Set-Cookie: jwtrefreshtoken=%s; Secure; HttpOnly; Max-Age=%ld; SameSite=Strict\r\n", newjwtrefreshtoken, config->jwt.ttl_refreshtoken);

		/* --------------------------------------------------------------- */
		printf("\r\n");
		/* --------------------------------------------------------------- */
		fputs(body, stdout);

	} else if (strcmp(qry_as, "plain") == 0) {
		printf("Content-Type: text/plain\r\n");
		printf("Status: %d\r\n", statuscode);
		printf("Set-Cookie: jwtrefreshtoken=%s; Secure; HttpOnly; Max-Age=%ld; SameSite=Strict\r\n", newjwtrefreshtoken, config->jwt.ttl_refreshtoken);

		/* --------------------------------------------------------------- */
		printf("\r\n");
		/* --------------------------------------------------------------- */
		printf("%s", jwtstr);
	} else {
		statuscode = 400;
		sprintf(errmsg, "%s: Unsupported 'as' query value", __func__);
		cgi_printerror(errmsg, statuscode);
		goto end;
	
	}

end:
	free(jwtstr);
	user_free(user);
	fflush(stdout);
	cgi_kvp_free(qry);
	cgi_kvp_free(cok);
	db_close(db);
	config_free(config);
	return 0;
}
