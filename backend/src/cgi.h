#ifndef CGI_H_
#define CGI_H_

#include <stdlib.h>
#include <stdbool.h>

/**
 * @file
 * @brief Header file for cgi.c
 */

/**
 * @brief the maximum value for CONTENT_LENGTH
 */
#define CGI_MAXINPUTSIZE 65536

/**
 * @brief key-value-pairs struct
 *
 * Basically a linked list of key-value-pairs.
 */
struct cgi_kvp_s {
	char * key; ///< the key
	char * value; ///< the value associated with the key
	struct cgi_kvp_s * next; ///< the next pair
};

struct cgi_kvp_s * cgi_parse_query_string(const char * qrystr);
struct cgi_kvp_s * cgi_parse_http_cookie(const char * cokstr);

struct cgi_kvp_s * cgi_kvp_create(void);
void cgi_kvp_free(struct cgi_kvp_s * kvp);
bool cgi_kvp_add(struct cgi_kvp_s * kvp, const char * key, const char * value);
bool cgi_kvp_add_delimbased(struct cgi_kvp_s * kvp, char * keyvalue, const char delim);
char * cgi_kvp_get(struct cgi_kvp_s * kvp, const char * key);

int cgi_testenv(char * errmsg, char method, const char * post_content_type);

void cgi_printstatuscodeasstr(int statuscode);

void cgi_printerror(char * errmsg, int statuscode);

/*bool cgi_readboundary(char * rawinput, size_t rawinputlen, char * boundarybuf, size_t maxboundarybufsize);

bool cgi_readformdata(char * rawinput, char * boundary);*/
#endif
