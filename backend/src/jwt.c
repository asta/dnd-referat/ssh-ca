#include "jwt.h"
#include "common.h"

#include "../cJSON/cJSON.h"

#include <openssl/sha.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

/**
 * 
 * @file
 *
 * @brief JSON Web Token
 *
 * This implementation of JWTs is NOT 100% conforming to RFC7519! See https://datatracker.ietf.org/doc/html/rfc7519#section-8 .
 * For example the algorithm type "none" is NOT implemented!
 * Also the validation is NOT 100% conforming to https://datatracker.ietf.org/doc/html/rfc7519#section-7.2 , as this implementation assumes the algorithm and thus can and does check the signature first.
 * 
 * @TODO Make this implementation 100% conforming to [RFC7519](https://datatracker.ietf.org/doc/html/rfc7519).
 *
 */

/**
 * @brief Wrapper function for creating, encoding and signing a JWT.
 *
 * This function calls:
 * 1. jwt_gen_payload()
 * 2. jwt_encode_and_sign()
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in]  user   the user
 * @param[in]  expin  number of seconds for which the JWT is valid
 * @param[in]  leeway leeway in seconds to account for clock skew (see https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.4)
 * @param[in]  key    the key/secret
 *
 * @returns a signed JWT as a string
 * @retval NULL an error occurred and errno might be set. An error message has been written to errmsg.
 *
 * The returned string must be freed using free().
 */
char * jwt_simple_encode(char * errmsg, const user_t * user, unsigned long expin, unsigned long leeway, const char * key) {
	bool success = false;
	char * jwtstr = NULL;
	cJSON * payload = NULL;

	payload = jwt_gen_payload(errmsg, user, expin, leeway);
	if (payload == NULL) {
		// errmsg already set
		goto end;
	}

	jwtstr = jwt_encode_and_sign(payload, key);
	if (jwtstr == NULL) {
		sprintf(errmsg, "%s: failed encoding and/or signing the JWT", __func__);
		goto end;
	}

	success = true;
end:
	cJSON_Delete(payload);
	if (!success) {
		free(jwtstr);
		return NULL;
	}
	return jwtstr;
}

/**
 * @brief Wrapper function for verifying and decoding a JWT and extracting the userid from the sub claim.
 *
 * This function calls:
 * 1. jwt_verify_encoding()
 * 2. jwt_verify_signature()
 * 3. jwt_decode()
 * 4. jwt_has_expired()
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in]  jwtstr the JWT as a string
 * @param[in]  key    the key/secret
 *
 * @returns the sub claim of the payload of the JWT. This should be a userid.
 * @retval NULL an error occurred and errno might be set, or jwtstr failed verification. An error message has been written to errmsg.
 *
 * The returned string must be freed using free().
 */
char * jwt_simple_decode(char * errmsg, const char * jwtstr, const char * key) {
	bool success = false;
	char * userid = NULL;
	cJSON * payload = NULL;
	cJSON * tmp = NULL;
	
	if (!jwt_verify_encoding(jwtstr)) {
		sprintf(errmsg, "%s: failed verifying encoding of JWT", __func__);
		goto end;
	}

	if (!jwt_verify_signature(jwtstr, key)) {
		sprintf(errmsg, "%s: failed verifying signature of JWT", __func__);
		goto end;
	}

	if ((payload = jwt_decode(jwtstr)) == NULL) {
		sprintf(errmsg, "%s: failed decoding JWT", __func__);
		goto end;
	}

	// extract sub and store it in userid
	if ((tmp = cJSON_GetObjectItemCaseSensitive(payload, "sub")) == NULL) {
		sprintf(errmsg, "%s: sub is not defined in payload", __func__);
		goto end;
	}
	if (!cJSON_IsString(tmp) || tmp->valuestring == NULL) {
		sprintf(errmsg, "%s: sub in payload is not a string", __func__);
		goto end;
	}
	if ((userid = strdup(tmp->valuestring)) == NULL) {
		sprintf(errmsg, "%s: failed duplicating userid", __func__);
		goto end;
	}
	
	// check if expired
	if (jwt_has_expired(payload)) {
		sprintf(errmsg, "%s: the JWT has expired", __func__);
		goto end;
	}


	success = true;
end:
	cJSON_Delete(payload);
	if (!success) {
		free(userid);
		return NULL;
	}
	return userid;
}

/**
 * @brief Generates a JWT payload.
 *
 * The payload contains the following claims:
 * iss: JWT_CLAIM_ISS
 * sub: userid
 * iat: current time as 
 * nbf: iat - leeway
 * exp: iat + expin + leeway
 * jti: UUIDv4
 * name: username
 *
 * @see https://datatracker.ietf.org/doc/html/rfc7519#section-4.1
 *
 * @param[out] errmsg buffer for error messages. size: ERRMSGMAXSIZE
 * @param[in]  user   the user
 * @param[in]  expin  number of seconds for which the JWT is valid
 * @param[in]  leeway leeway in seconds to account for clock skew (see https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.4)
 *
 *
 * @returns the payload as JSON
 * @retval NULL an error occurred and errno might be set. An error message has been written to errmsg.
 *
 * The returned cJSON object must be freed using cJSON_Delete().
 *
 */
cJSON * jwt_gen_payload(char * errmsg, const user_t * user, unsigned long expin, unsigned long leeway) {
	/*
	 * iss: JWT_CLAIM_ISS
	 * sub: userid
	 * iat: current time
	 * nbf: iat - leeway
	 * exp: iat + expin + leeway
	 * jti: UUIDv4
	 * 
	 * name: username
	 *
	 * //aud: maybe used in future to determine who can use the webapi
	 */
	bool success = false;
	cJSON * res = NULL;
	const char * iss = JWT_CLAIM_ISS;
	time_t iat = 0;
	time_t nbf = 0;
	time_t exp = 0;
	char jti[UUIDLEN + 1];
	char * sub = NULL;
	char * name = NULL;

	if (user == NULL) {
		sprintf(errmsg, "%s: user isn't set", __func__);
		goto end;
	}

	// get current time
	if ((iat = time(NULL)) == ((time_t) -1)) {
		sprintf(errmsg, "%s: couldn't get current time", __func__);
		goto end;
	}

	// calculate nbf and exp
	nbf = iat - leeway;
	exp = iat + expin + leeway;

	// generate jti
	if (!uuid_v4_gen(jti)) {
		sprintf(errmsg, "%s: failed generating jti", __func__);
		goto end;
	}

	sub = user->id;

	name = user->name;


	// === generate payload
	
	if ((res = cJSON_CreateObject()) == NULL) {
		sprintf(errmsg, "%s: failed generating payload object", __func__);
		goto end;
	}

	// add claims to payload
	if (cJSON_AddStringToObject(res, "iss", iss) == NULL) {
		sprintf(errmsg, "%s: failed adding iss to payload", __func__);
		goto end;
	}
	if (cJSON_AddStringToObject(res, "sub", sub) == NULL) {
		sprintf(errmsg, "%s: failed adding sub to payload", __func__);
		goto end;
	}
	if (cJSON_AddNumberToObject(res, "iat", iat) == NULL) {
		sprintf(errmsg, "%s: failed adding iat to payload", __func__);
		goto end;
	}
	if (cJSON_AddNumberToObject(res, "nbf", nbf) == NULL) {
		sprintf(errmsg, "%s: failed adding nbf to payload", __func__);
		goto end;
	}
	if (cJSON_AddNumberToObject(res, "exp", exp) == NULL) {
		sprintf(errmsg, "%s: failed adding exp to payload", __func__);
		goto end;
	}
	if (cJSON_AddStringToObject(res, "jti", jti) == NULL) {
		sprintf(errmsg, "%s: failed adding jti to payload", __func__);
		goto end;
	}
	if (cJSON_AddStringToObject(res, "name", name) == NULL) {
		sprintf(errmsg, "%s: failed adding name to payload", __func__);
		goto end;
	}



	success = true;
end:
	if (!success) {
		cJSON_Delete(res);
		return NULL;
	}
	return res;
}

/**
 * @brief Encodes and signs a JWT.
 *
 * Uses HS256 as algorithm.
 *
 * The payload can be generated using jwt_gen_payload().
 *
 * @note The payload is taken as is and NOT further modified by this function.
 *
 * @param[in] payload the JWT payload
 * @param[in] key the key/secret
 *
 * @returns a signed JWT as a string
 * @retval NULL an error occurred and errno might be set
 *
 * The returned string must be freed using free().
 *
 */
char * jwt_encode_and_sign(const cJSON * payload, const char * key) {
	bool success = false;
	char * res = NULL;
	char * tmp = NULL;
	char * payloadstr = NULL;
	size_t payloadstrlen = 0;
	const char * headerraw = "{\"alg\":\""JWT_HEADER_ALG"\",\"typ\":\""JWT_HEADER_TYP"\"}";
	char * headerstr = NULL;
	size_t headerstrlen = 0;
	unsigned char sigraw[SHA256_DIGEST_LENGTH];
	char * sigstr;

	if (payload == NULL || !cJSON_IsObject(payload) || key == NULL) {
		goto end;
	}

	// generate base64url encoded payloadstr
	if ((tmp = cJSON_PrintUnformatted(payload)) == NULL) {
		goto end;
	}
	if ((payloadstr = base64url_encode(tmp, strlen(tmp), true)) == NULL) {
		goto end;
	}
	payloadstrlen = strlen(payloadstr);
	free(tmp); tmp = NULL;

	// generate base64url encoded headerstr
	if ((headerstr = base64url_encode(headerraw, strlen(headerraw), true)) == NULL) {
		goto end;
	}
	headerstrlen = strlen(headerstr);

	// generate beginning of res
	if ((res = malloc(sizeof(char) * (headerstrlen + 1 + payloadstrlen + 1 + (SHA256_DIGEST_LENGTH * 4) + 1))) == NULL) {
		goto end;
	}
	res[0] = '\0';
	strcat(res, headerstr);
	strcat(res, ".");
	strcat(res, payloadstr);
	
	// sign
	if (!hmac_sha256(sigraw, res, key)) {
		goto end;
	}
	if ((sigstr = base64url_encode((char *)sigraw, SHA256_DIGEST_LENGTH, true)) == NULL) {
		goto end;
	}
	strcat(res, ".");
	strcat(res, sigstr);


	success = true;
end:
	free(sigstr);
	free(headerstr);
	free(tmp);
	free(payloadstr);
	if (!success) {
		free(res);
		return NULL;
	}
	return res;
}


/**
 * @brief Verifies the encoding of a JWT string.
 *
 * This function is a really basic encoding verification.
 * It only checks whether all characters are valid base64url characters, the number of dots and the minimal distance between the dots.
 * That's it!
 *
 * @param[in] jwtstr the JWT as a string
 *
 * @returns whether jwtstr is encoded correctly
 * @retval true jwtstr might be a valid JWT string
 * @retval false jwtstr is not a valid JWT string
 *
 */
bool jwt_verify_encoding(const char * jwtstr) {
	int dots = 0;
	int sincelastdot = 0;
	char c;
	size_t i = 0;

	if (jwtstr == NULL)
		return false;

	while ((c = jwtstr[i]) != '\0') {
		if (((c) >= 'A' && (c) <= 'Z') || ((c) >= 'a' && (c) <= 'z') || ((c) >= '0' && (c) <= '9') || (c) == '-' || (c) == '_') {
			sincelastdot++;
		} else if (c == '.') {
			if (sincelastdot < 2)
				return false;
			dots++;
			sincelastdot = 0;
		} else {
			return false;
		}
		i++;
	}

	if (sincelastdot < 2)
		return false;

	if (dots != 2)
		return false;

	return true;
}

/**
 * @brief Verifies the signature of a JWT string.
 *
 * @param[in] jwtstr the JWT as a string
 * @param[in] key the key to be used for verification
 *
 * @returns whether jwtstr is signed by key
 * @retval true jwtstr was signed with key
 * @retval false jwtstr was NOT signed with key
 *
 * @note You should call jwt_verify_encoding() before calling this function.
 *
 */
bool jwt_verify_signature(const char * jwtstr, const char * key) {
	bool res = false;
	char * jwtstrdup = NULL;
	char * tmp = NULL;
	char * sigstr = NULL;
	unsigned char expecsigraw[SHA256_DIGEST_LENGTH];
	char * expecsigstr = NULL;
	
	if (jwtstr == NULL || key == NULL)
		goto end;

	if ((jwtstrdup = strdup(jwtstr)) == NULL)
		goto end;

	// find signature
	if ((tmp = strchr(jwtstrdup, '.')) == NULL)
		goto end;
	if ((tmp = strchr(tmp + 1, '.')) == NULL)
		goto end;
	sigstr = tmp + 1;
	*tmp = '\0'; // remove signature from jwtstrdup

	// generate expected signature
	if (!hmac_sha256(expecsigraw, jwtstrdup, key)) {
		goto end;
	}
	if ((expecsigstr = base64url_encode((char *)expecsigraw, SHA256_DIGEST_LENGTH, true)) == NULL) {
		goto end;
	}

	// verify signature
	if (strcmp(sigstr, expecsigstr) != 0) {
		goto end;
	}

	res = true;
end:
	free(expecsigstr);
	free(jwtstrdup);
	return res;
}

/**
 *
 * @brief Extracts the payload of a JWT string.
 *
 * @param[in] jwtstr the JWT as a string
 *
 * @returns the payload as JSON
 * @retval NULL an error occurred and errno might be set
 *
 * @note You should call jwt_verify_signature() before calling this function.
 * @note You should call jwt_has_expired() after calling this function.
 *
 * The returned cJSON object must be freed using cJSON_Delete().
 *
 */
cJSON * jwt_decode(const char * jwtstr) {
	bool success = false;
	char * jwtstrdup = NULL;
	char * headerstr = NULL;
	char * headerend = NULL;
	char * payloadstr = NULL;
	char * payloadend = NULL;
	cJSON * header = NULL;
	cJSON * payload = NULL;
	cJSON * tmp = NULL;

	if (jwtstr == NULL) {
		goto end;
	}

	// duplicate jwtstr
	if ((jwtstrdup = strdup(jwtstr)) == NULL) {
		goto end;
	}

	// find headerend
	if ((headerend = strchr(jwtstrdup, '.')) == NULL) {
		goto end;
	}
	*headerend = '\0';
	// decode header as string
	if ((headerstr = base64url_decode(jwtstrdup)) == NULL) {
		goto end;
	}

	// find payloadend
	if ((payloadend = strchr(headerend + 1, '.')) == NULL) {
		goto end;
	}
	*payloadend = '\0';
	// decode payload as string
	if ((payloadstr = base64url_decode(headerend + 1)) == NULL) {
		goto end;
	}

	// parse header
	if ((header = cJSON_Parse(headerstr)) == NULL) {
		goto end;
	}

	// verify header typ
	if ((tmp = cJSON_GetObjectItemCaseSensitive(header, "typ")) == NULL) {
		goto end;
	}
	if (!cJSON_IsString(tmp) || tmp->valuestring == NULL) {
		goto end;
	}
	if (strcmp(tmp->valuestring, JWT_HEADER_TYP) != 0) {
		goto end;
	}

	// verify header alg
	// TODO verify header alg somewhere else otherwise there are problems when decoding id_token in oidc.c
	/*if ((tmp = cJSON_GetObjectItemCaseSensitive(header, "alg")) == NULL) {
		goto end;
	}
	if (!cJSON_IsString(tmp) || tmp->valuestring == NULL) {
		goto end;
	}
	if (strcmp(tmp->valuestring, JWT_HEADER_ALG) != 0) {
		goto end;
	}*/

	// parse payload
	if ((payload = cJSON_Parse(payloadstr)) == NULL) {
		goto end;
	}


	success = true;
end:
	cJSON_Delete(header);
	free(payloadstr);
	free(headerstr);
	free(jwtstrdup);
	if (!success) {
		cJSON_Delete(payload);
		return NULL;
	}
	return payload;
}

/**
 * @brief Tests whether a JWT payload has expired.
 *
 * This function compares the nbf and exp claims in the payload to the current time.
 *
 * nbf is ignored, if nbf doesn't exist in the payload.
 *
 * @param[in] payload the JWT payload
 *
 * @returns whether the payload has expired
 * @retval false the payload has NOT expired
 * @retval true the payload has expired and is thus invalid
 *
 */
bool jwt_has_expired(const cJSON * payload) {
	time_t curtime = 0;
	time_t nbf = 0;
	time_t exp = 0;
	cJSON * tmp = NULL;

	if (payload == NULL || !cJSON_IsObject(payload)) {
		return true;
	}

	// get current time
	if ((curtime = time(NULL)) == ((time_t) -1)) {
		return true;
	}
	//fprintf(stderr, "%s: [DEBUG] curtime: %ld\n", __func__, curtime);
	
	// get nbf
	if ((tmp = cJSON_GetObjectItemCaseSensitive(payload, "nbf")) != NULL) {
		if (!cJSON_IsNumber(tmp)) {
			return true;
		}
		nbf = (time_t)tmp->valuedouble; // using valuedouble because valueint is too small
		//fprintf(stderr, "%s: [DEBUG] nbf: %ld\n", __func__, nbf);
	}

	// get exp
	if ((tmp = cJSON_GetObjectItemCaseSensitive(payload, "exp")) == NULL) {
		return true;
	}
	if (!cJSON_IsNumber(tmp)) {
		return true;
	}
	exp = (time_t)tmp->valuedouble; // using valuedouble because valueint is too small


	// compare times
	if (curtime < nbf) {
		return true;
	} else if (curtime >= exp) {
		return true;
	}

	return false;
}

