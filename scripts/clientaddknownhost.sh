#!/bin/bash

HOSTCAPUB="$(cat ../host-ca.pub | tr -d '\r\n')"

grep -qxF "${HOSTCAPUB}" ~/.ssh/known_hosts || echo "@cert-authority * ${HOSTCAPUB}" | tee -a ~/.ssh/known_hosts
