#!/bin/bash

if [ "x$#" != "x1" ]; then
	echo "Usage: $0 <SERVER IP>"
	exit 1
fi

ASTASSHSERVERSCSVURL="${ASTASSHSERVERSCSVURL:=https://asta.pages.gwdg.de/dnd-referat/asta-ansible/ssh_ca_servers.csv}"

TMPKEYSDIR="/tmp/sshca-tmp-keys"
SSHCADIR="/opt/ssh-ca"
HOSTCAFILE="${SSHCADIR}/host-ca"

TARGET_ADDRESS="${1}"
TARGET_USER=cloud

VALIDTIME="-5m:+52w"

get_principals() {
	serverscsv="$(curl "${ASTASSHSERVERSCSVURL}" | tr -d ' ' | grep '^t')"
	serverline="$(echo "$serverscsv" | grep -F "|${TARGET_ADDRESS}|")"
	echo "$serverline"
	if [ "x$serverline" = "x" ]; then
		echo "ERROR: server not found in ${ASTASSHSERVERSCSVURL}"
		exit 1
	elif [ "x$(echo "${serverline}" | wc -l)" != "x1" ]; then
		echo "ERROR: server has duplicates in ${ASTASSHSERVERSCSVURL}"
		exit 1
	fi
	SERVERNAME="$(echo "${serverline}" | cut -d '|' -f 2)"
	SERVERIP="$(echo "${serverline}" | cut -d '|' -f 3)"
	#SERVERDOMAINS="$(echo "${serverline}" | cut -d '|' -f 4)"
	SERVERPRINCIPALS="admin\n${SERVERNAME}\n$(echo "${serverline}" | cut -d '|' -f 5 | tr ',' '\n')"
}
set -e

# Test if on CA Server
if [ "$(hostname)" != "sshca" ]; then
	echo "This script only works on the CA Server!"
	exit 1
fi

pushd "${SSHCADIR}"

echo "=> Setting Server up for login via SSHCA: IP: ${TARGET_ADDRESS}"

# Remove Target from known_hosts file
echo "=> Remove Target from known_hosts file"
ssh-keygen -f "/home/cloud/.ssh/known_hosts" -R "${TARGET_ADDRESS}"


# Test if Target is reachable and add to known_hosts file
echo "=> Test if Target is reachable"
ssh -o "ConnectTimeout=3" -o "StrictHostKeyChecking=no" -n ${TARGET_USER}@${TARGET_ADDRESS} "whoami"

# Get Hostname of Target
echo "=> Get Hostname of Target"
TARGET_HOSTNAME="$(ssh -n ${TARGET_USER}@${TARGET_ADDRESS} "hostname")"
echo "TARGET_HOSTNAME: \"${TARGET_HOSTNAME}\""


get_principals
echo "Using Principals:"
echo "$SERVERPRINCIPALS"
echo ""
#exit 0

## Update Target
#ssh -n ${TARGET_USER}@${TARGET_ADDRESS} "sudo apt-get -y update"
#ssh -n ${TARGET_USER}@${TARGET_ADDRESS} "sudo apt-get -y upgrade"
#ssh -n ${TARGET_USER}@${TARGET_ADDRESS} "sudo apt-get -y autoremove"

# (Re-)create SSH Host Key on Target and add to known_hosts file again
echo "=> (Re-)create SSH Host Key on Target"
ssh -n ${TARGET_USER}@${TARGET_ADDRESS} "sudo rm -vf /etc/ssh/ssh_host_ed25519_key /etc/ssh/ssh_host_ed25519_key.pub"
ssh -n ${TARGET_USER}@${TARGET_ADDRESS} -o "StrictHostKeyChecking=no" "sudo ssh-keygen -N '' -C \"HOST_KEY_${TARGET_HOST}\" -t ed25519 -h -f /etc/ssh/ssh_host_ed25519_key"

# Sign Target Host Key
echo "=> Sign Target Host Key"
mkdir -p "${TMPKEYSDIR}"
pushd "${TMPKEYSDIR}"
rm -fv *
scp ${TARGET_USER}@${TARGET_ADDRESS}:/etc/ssh/ssh_host_ed25519_key.pub .
ssh-keygen -s "${HOSTCAFILE}" -I "SERVER_${TARGET_HOSTNAME}" -h -V ${VALIDTIME} ssh_host_ed25519_key.pub
scp ssh_host_ed25519_key-cert.pub ${TARGET_USER}@${TARGET_ADDRESS}:/tmp/ssh_host_ed25519_key-cert.pub
## NOTE: Potential cace condition!
ssh -n ${TARGET_USER}@${TARGET_ADDRESS} "sudo mv -v /tmp/ssh_host_ed25519_key-cert.pub /etc/ssh/ssh_host_ed25519_key-cert.pub"
rm -fv *
popd

# Configure ssh to use new host certificate
echo "=> Configure ssh to use new host certificate"
ssh -n ${TARGET_USER}@${TARGET_ADDRESS} "grep -qxF 'HostCertificate /etc/ssh/ssh_host_ed25519_key-cert.pub' /etc/ssh/sshd_config || echo 'HostCertificate /etc/ssh/ssh_host_ed25519_key-cert.pub' | sudo tee -a /etc/ssh/sshd_config"


# Copy public client CA signing key to target host
echo "=> Copy public client CA signing key to target host"
scp client-ca.pub ${TARGET_USER}@${TARGET_ADDRESS}:/tmp/client-ca.pub
## NOTE: Potential cace condition!
ssh -n ${TARGET_USER}@${TARGET_ADDRESS} "sudo mv -v /tmp/client-ca.pub /etc/ssh/client-ca.pub"

# Configure to use new public client CA key
echo "=> Configure to use new public client CA key"
ssh -n ${TARGET_USER}@${TARGET_ADDRESS} "sudo chmod 644 /etc/ssh/client-ca.pub"
ssh -n ${TARGET_USER}@${TARGET_ADDRESS} "grep -qxF 'TrustedUserCAKeys /etc/ssh/client-ca.pub' /etc/ssh/sshd_config || echo 'TrustedUserCAKeys /etc/ssh/client-ca.pub' | sudo tee -a /etc/ssh/sshd_config"

# Configure principals
echo "=> Configure principals"
ssh -n ${TARGET_USER}@${TARGET_ADDRESS} "sudo mkdir -p /etc/ssh/auth_principals"
ssh -n ${TARGET_USER}@${TARGET_ADDRESS} "echo -e '${SERVERPRINCIPALS}' | sudo tee /etc/ssh/auth_principals/${TARGET_USER}"
ssh -n ${TARGET_USER}@${TARGET_ADDRESS} "grep -qxF 'AuthorizedPrincipalsFile /etc/ssh/auth_principals/%u' /etc/ssh/sshd_config || echo 'AuthorizedPrincipalsFile /etc/ssh/auth_principals/%u' | sudo tee -a /etc/ssh/sshd_config"

## TODO: disable Password authentication!

# Restart SSH Server on Target
echo "=> Restart SSH Server on Target"
ssh -n ${TARGET_USER}@${TARGET_ADDRESS} "sudo systemctl restart ssh"
ssh -n ${TARGET_USER}@${TARGET_ADDRESS} "sudo systemctl status ssh"


echo "=> Finished! Server: ${TARGET_ADDRESS} \"${TARGET_HOSTNAME}\""
popd
