#!/bin/bash

if [ "x$#" != "x0" ]; then
	echo "Usage: $0"
	exit 1
fi

ASTASSHSERVERSCSVURL="${ASTASSHSERVERSCSVURL:=https://asta.pages.gwdg.de/dnd-referat/asta-ansible/ssh_ca_servers.csv}"

ASTASETUPSERVERFORSSHCASCRIPT="/opt/ssh-ca/scripts/setup_server_for_sshca.sh"


#set -e

# Test if on CA Server
if [ "$(hostname)" != "sshca" ]; then
	echo "This script only works on the CA Server!"
	exit 1
fi

SERVERSCSV="$(curl "${ASTASSHSERVERSCSVURL}" | tr -d ' ' | grep '^t')"
echo "${SERVERSCSV}"

echo "${SERVERSCSV}" | while read serverline ; do
	echo ""
	echo ""
	echo ""

	SERVERNAME="$(echo "${serverline}" | cut -d '|' -f 2)"
	SERVERIP="$(echo "${serverline}" | cut -d '|' -f 3)"
	echo "serverline: \"${serverline}\""
	echo ""
	/opt/ssh-ca/scripts/setup_server_for_sshca.sh "${SERVERIP}"

	echo ""
	echo ""
	echo ""
done
echo "Done."
