
function updateJWT(jwt) {
	var jwtshowsection = document.getElementById("jwt-show-section");
	var jwtfield = document.getElementById("jwt-field");
	jwtfield.value = jwt;

	document.getElementById("jwt-copy-button").onclick = function() {
		jwtfield.type = "text";
		jwtfield.select();
		jwtfield.setSelectionRange(0, 99999)
		document.execCommand("copy");
		jwtfield.type = "password";
	};

	jwtshowsection.classList.remove("hidden");
}

function refreshJWT() {
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "/cgi/refreshlogin?as=json", true);
	xhr.responseType = 'json';
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.onload = function() {
		var status = xhr.status;
		if (status === 200) {
			updateJWT(xhr.response.jwt);
		} else {
			//callback(status, xhr.response.jwt);
		}
	};
	xhr.send();
};

window.onload = function () {
	refreshJWT();
	setInterval(function () {
		refreshJWT();
	}, 60000 * 5);
}

