https://allthingscloud.eu/2020/01/05/ssh-certificate-based-authentication-a-quick-guide/
https://smallstep.com/blog/use-ssh-certificates/


host-ca was created with:
```
ssh-keygen -t ed25519 -N '' -C "AStA Host SSH CA" -f host-ca
```

client-ca was created with:
```
ssh-keygen -t ed25519 -N '' -C "AStA Client SSH CA" -f client-ca
```
